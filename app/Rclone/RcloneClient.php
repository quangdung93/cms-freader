<?php

namespace App\Rclone;

use GuzzleHttp\Client;

class RcloneClient
{

    protected $client;
    protected $host;
    protected $auth;

    public function __construct(Client $client, string $host = "http://localhost:5572", $auth = ['dat', 'dat123'])
    {
        $this->client = $client;
        $this->host = $host;
        $this->auth = $auth;
    }
    public function checkConfig($configObj)
    {
        $conType = $configObj['type'];
        unset($configObj['type']);
        $name = md5(rand());
        $mainConfig = [
            'name' => $name,
            'type' => $conType,
            'parameters' => $configObj,
        ];
        // create connection
        if (!$this->createRcloneConfig($mainConfig)) {
            return ['status' => 'fail'];
        }
        if (!$this->testUpload($name)) {
            //
            return ['status' => 'fail'];
        }
        $this->deleteConfig($name);
        return [
            'status' => 'success',
        ];
        //close connection
    }
    protected function createRcloneConfig($config)
    {
        $res = $this->client->post($this->host . '/config/create', [
            'json' => $config,
            'auth' => $this->auth,
        ]);
        if ($res->getStatusCode() == 200) {
            return true;
        } else {
            return false;
        }
    }
    protected function testUpload($name)
    {
        try {

            $res = $this->client->post($this->host . '/operations/size', [
                'json' => [
                    'fs' => $name . ":/test",
                    // "remote" => ""
                ],
                'auth' => $this->auth,
            ]);
            if ($res->getStatusCode() == 200) {
                return true;
            } else {
                return false;
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $err = (json_decode($e->getResponse()->getBody(), 1));
            if ($err['error'] == 'directory not found'){
                return true;
            }
            return false;
        }

    }
    protected function fsInfo($name)
    {
        $res = $this->client->post($this->host . '/operations/fsinfo', [
            'json' => [
                "fs" => $name . ":",
            ],
            'auth' => $this->auth,
        ]);
        if ($res->getStatusCode() == 200) {
            $data = json_decode($res->getBody()->getContents(), 1);

            if (isset($data['Features'])) {
                return $data['Features'];
            }
        } else {
            return null;
        }
    }
    protected function deleteConfig($name)
    {
        $res = $this->client->post($this->host . '/config/delete', [
            'json' => [
                'name' => $name,
            ],
            'auth' => $this->auth,
        ]);
        $body = $res->getStatusCode();
        if ($res->getStatusCode() == 200) {
            return true;
        } else {
            return false;
        }
    }

}
