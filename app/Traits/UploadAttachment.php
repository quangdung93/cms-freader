<?php
 
namespace App\Traits;

use Exception;
use Illuminate\Http\UploadedFile;
use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
 
trait UploadAttachment
{

    public function uploadFile(UploadedFile $file, $path)
    {
        try {
            $ext = $file->extension();
            $fileName = md5($file->get()). '.'. $ext;
            $loc = $path. '/' .$fileName;
            Storage::disk("s3")->putFileAs($path, $file, $fileName);
            return $loc;
        } catch (Exception $e) {
            Log::error($e);
            return null;
        }
    }
    public function deleteFiles(array $files){
        foreach($files as $deleteMe){
            try{
                Storage::disk("s3")->delete($deleteMe);
            }catch(Exception $e){
                Log::error($e);
                return null;
            }
        }
    }
}