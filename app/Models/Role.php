<?php

namespace App\Models;

use App\Scopes\UndevelopRoleScope;
use Spatie\Permission\Models\Role as ModelsRole;

class Role extends ModelsRole
{
    protected static function booted(){
        static::addGlobalScope(new UndevelopRoleScope);
    }

    public function permissionGroups(){
        return $this->belongsToMany(PermissionGroup::class, 'role_has_permission_groups', 'role_id', 'permission_group_id');
    }
}