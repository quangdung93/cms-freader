<?php

namespace App\Models;

use App\Traits\LogActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Store extends Model
{
    use SoftDeletes;

    protected $table = 'stores';
    protected $primaryKey = "id";

    protected $fillable = [
        'name',
        'company_name',
        'logo',
        'phone',
        'address',
        'bank_account',
        'facebook',
        'zalo',
        'website',
        'email',
        'note',
        'status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $guarded = [];
    protected $dates = ['deleted_at'];
    const DEFAULT = 2;

    public function scopeActive($query){
        return $query->where('status', 1);
    }

    public static function getDefaultStore(){
        $stores = Auth::user()->stores;
        $userStores = [];
        if($stores){
            $userStores = explode(',', $stores);
        }

        if(count($userStores) > 0){
            return $userStores[0];
        }

        return null;
    }

    public function getDataTableQuery(array $input)
    {
        $query = $this::query();
        if(isset($input['name'])){
            $keyword = trim($input['name']);
            $query->where('name', 'like', '%'.$keyword.'%')
                ->orWhere('company_name', 'like', '%'.$keyword.'%')
                ->orWhere('phone', 'like', '%'.$keyword.'%');
        }
        return $query->whereNull("deleted_at")->select('*');
    }

    public function getItem($id)
    {
        $select = $this->where("id", $id)
                       ->whereNull("deleted_at");

        return $select->first();
    }

    /**
     * Thêm chi nhánh
     * @param $data
     * @return mixed
     */
    public function createItem($data)
    {
        return $this::create($data);
    }

    /**
     * Thêm chi nhánh
     * @param $data
     * @return mixed
     */
    public function updateItem($data, $id)
    {
        return $this::where("id", $id)->update($data);
    }

    public function getItemByName($name, $id=null)
    {
        $select = $this->where("name", $name)
                       ->whereNull("deleted_at");
        if(isset($id)){
            $select->where("id", "<>", $id);
        }
        return $select->first();
    }
}
