<?php

namespace App\Models;

use App\Models\Ward;
use App\Models\District;
use App\Models\Province;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Customer extends Model
{
    protected $table = 'users';
    protected $primaryKey = "id";
    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = [
        'name',
        'first_name',
        'dob',
        'gender',
        'password',
        'email',
        'phone_number',
        'is_active',
        'is_deleted',
        'updated_at',
        'deleted_at'
    ];

    public function devices(){
        return $this->hasMany(AccessToken::class, "user_id", "id")
                    ->where("is_active", 1);
    }
    public function searchCustomerWithoutIds($key, $listExistedIds = [], $limit = 10){
        return self::where(DB::raw("CONCAT(first_name,name, phone_number,email)"), 'LIKE', "%".$key."%")
            ->whereNotIn('id', $listExistedIds)
            ->limit($limit)
            ->get();
    }

}
