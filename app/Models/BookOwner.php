<?php

namespace App\Models;

use App\Scopes\StoreScope;
use App\Traits\HasUUID;
use App\Traits\LogActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BookOwner extends Model
{

    protected $table = 'book_owner';

    protected $primaryKey = "id";
    // protected $keyType = "string";
    protected $guarded = [];

    // protected static function boot(){
    //     parent::boot();
    //     static::addGlobalScope(new StoreScope);
    // }
    public $timestamps = true;
    public function getBookOwners($customerId, $productIds){
        return $this->whereIn('book_id', $productIds)->where('user_id', $customerId)->get();

    }
    public function order_detail(){
        return $this->hasOne("order_detail", "order_detail_id");
    }
}
