<?php

namespace App\Models;

use App\Traits\HasUUID;
use App\Traits\LogActivity;
use App\Helpers\ShortcodeHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class StorageObject extends Model
{
    use HasUUID;
    protected $table = 'storage_object';
    protected $primaryKey = "id";
    protected $keyType = "string";
    public $timestamps = true;
    public $incrementing = false;
    protected $guarded = [];
    public function updateStorageLink($preCondition, $updatedData){
        return $this->updateOrInsert($preCondition, $updatedData);
    }
    public function getStorageValue($bookId){
        return $this->select('data')->where('object_id', $bookId)->where('type', 's3')->where('object', 'book')->first();
    }
}
