<?php

namespace App\Models;

use Spatie\Permission\Models\Permission as ModelPermission;

class Permission extends ModelPermission
{
    public function feature(){
        return $this->belongsTo(Feature::class, 'feature_id');
    }
}
