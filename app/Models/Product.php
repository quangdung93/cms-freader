<?php

namespace App\Models;

use App\Helpers\ShortcodeHelper;
use App\Traits\LogActivity;
use DB;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use LogActivity;
    protected $table = 'book';
    protected $primaryKey = "id";
    protected $keyType = "string";

    protected $guarded = [];

    public function category()
    {
        return $this->categories()->where('is_primary', 1);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_category');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable')->orderBy('sequence');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->where('status', 1);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function link()
    {
        return config('stableweb.prefix.product.slug') . '/' . $this->slug ?: '/';
    }

    public function handleContent()
    {
        $this->content = ShortcodeHelper::renderFromContent($this->content);
        //Lazyload image in body content
        $this->content = str_replace('src', 'data-src', $this->content);
    }

    public function searchProductWithoutIds($key, $listExistedIds = [], $limit = 10, $listIdInclude = [])
    {
        if(empty($key) && !empty($listIdInclude)){
            return self::whereIn('id', $listIdInclude)
                ->limit($limit)
                ->get();
        }
        return self::where(function ($query) use ($key, $listExistedIds) {
            return $query->where(DB::raw("CONCAT(name, sku, id)"), 'LIKE', "%" . $key . "%")
                ->whereNotIn('id', $listExistedIds);})
            ->orWhereIn('id', $listIdInclude)
            ->limit($limit)
            ->get();
    }

    public function getProducts($data)
    {
        $select = $this->select();
        if (isset($data['book_ids'])) {
            $select->whereIn("{$this->table}.id", $data['book_ids']);
        }

        return $select->get();
    }

    public function getItem($productId)
    {
        $select = $this->select()
            ->where("{$this->table}.id", $productId);

        return $select->first();
    }

    public function getProductPaging($data)
    {
        $select = $this->select(
            "{$this->table}.id",
            "{$this->table}.name",
            "{$this->table}.price",
            "{$this->table}.sku",
            "{$this->table}.thumbnail"
        );

        if (isset($data['search'])) {
            $select->where("{$this->table}.name", "LIKE", "%{$data['search']}%");
        }

        $page = (int) ($data['page'] ?? 1);
        $display = (int) ($data['perpage'] ?? 10);
        return $select->paginate($display, $columns = ['*'], $pageName = 'page', $page);
    }
}
