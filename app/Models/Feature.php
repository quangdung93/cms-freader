<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $table = 'features';
    protected $primaryKey = 'feature_id';

    protected $guarded = [];

    public function scopeActive($query){
        return $query->where('status', 1);
    }
}
