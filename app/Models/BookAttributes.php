<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BookAttributes extends Model
{
    // use SoftDeletes;
    protected $table = 'book_attributes';

    protected $guarded = [];
    public $timestamps = true;

}
