<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionGroup extends Model
{
    protected $table = 'permission_groups';

    protected $guarded = [];

    public function permissions(){
        return $this->belongsToMany(Permission::class, 'permission_group_details', 'permission_group_id', 'permission_id');
    }
}
