<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookAttributeDetail extends Model
{
    // use SoftDeletes;
    protected $table = 'book_attributes_detail';

    protected $guarded = [];
    public $timestamps = true;
    public function attribute()
    {
        return $this->belongsTo(BookAttributes::class, 'attribute_id', 'id');
    }
    public function upsertDetail($attrId, $value, $book_id)
    {
        return $this->updateOrInsert(
            [
                "book_id" => $book_id,
                "attribute_id" => $attrId,
            ], ["value" => $value]);
    }
}
