<?php

namespace App\Models;

use App\Traits\LogActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
// use Illuminate\Database\Eloquent\Factories\HasFactory;

class Supplier extends Model
{
    use SoftDeletes;
    use LogActivity;
    
    protected $table = 'suppliers';

    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function scopeActive($query){
        return $query->where('status', 1);
    }
}
