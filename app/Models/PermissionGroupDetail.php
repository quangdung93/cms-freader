<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionGroupDetail extends Model
{
    protected $table = 'permission_group_details';

    protected $guarded = [];
}
