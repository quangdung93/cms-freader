<?php

namespace App\Models;

use App\Models\Ward;
use App\Models\District;
use App\Models\Province;
use Illuminate\Database\Eloquent\Model;

class BlockItem extends Model
{
    protected $table = 'blocks_item';
    protected $primaryKey = "id";

    protected $fillable = [
        'blocks_id', 'data', 'type', 'priority', 'is_active', 'created_at', 'updated_at'
    ];

    /**
     *
     * Xóa danh sách item của block theo block id
     * @param $id
     * @return mixed
     */
    public function removeBlockItems($id)
    {
        return $this->where("blocks_id", $id)->delete();
    }

    /**
     * Thêm danh sách items
     * @param array $items
     */
    public function insertBlockBulk($items)
    {
        if(sizeof($items) > 0){
            return $this->insert($items);
        }
        return 1;
    }
}
