<?php

namespace App\Models;

use App\Traits\HasUUID;
use App\Traits\LogActivity;
use App\Helpers\ShortcodeHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Book extends Model
{
    use HasUUID;
    protected $table = 'book';
    protected $primaryKey = "id";
    protected $keyType = "string";
    public $timestamps = true;

    protected $guarded = [];

    public function attributes(){
        return $this->hasMany(BookAttributeDetail::class);
    }
    public function storage_links(){
        return $this->hasMany(StorageObject::class, 'object_id');
    }
}
