<?php

namespace App\Models;

use App\Models\Ward;
use App\Models\District;
use App\Models\Province;
use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    protected $table = 'blocks';
    protected $primaryKey = "id";

    protected $fillable = [
        'name',
        'type',
        'values',
        'block_type',
        'priority',
        'created_at',
        'updated_at',
        'is_active'
    ];


    public function block_items(){
        return $this->hasMany(BlockItem::class, 'blocks_id', 'id');
    }

    public function getDataTableQuery($input)
    {
        $query = $this::query();
        if(isset($input['name'])){
            $keyword = trim($input['name']);
            $query->where('name', 'like', '%'.$keyword.'%');
        }
        return $query->select('*');
    }

    public function getItem($id)
    {
        $select = $this->select(
                            "{$this->table}.id",
                            "{$this->table}.name",
                            "{$this->table}.type",
                            "{$this->table}.values",
                            "{$this->table}.block_type",
                            "{$this->table}.priority",
                            "{$this->table}.is_active",
                        )
                        ->with(['block_items' => function ($q) {
                            $q->orderBy('priority', 'desc');
                        }])
                        ->where("{$this->table}.id", $id);

        return $select->first();
    }

    public function getBlocks($data)
    {
        $select = $this->select(
            "{$this->table}.id",
            "{$this->table}.name",
            "{$this->table}.type",
            "{$this->table}.values",
            "{$this->table}.block_type",
            "{$this->table}.priority",
            "{$this->table}.is_active",
        );

        if(isset($data['search'])){
            $select->where("{$this->table}.name", "LIKE", "%{$data['search']}%");
        }
        if(isset($data['type'])){
            $select->where("{$this->table}.type", $data['type']);
        }

        $page    = (int) ($data['page'] ?? 1);
        $display = (int) ($data['perpage'] ?? 10);
        return $select->paginate($display, $columns = ['*'], $pageName = 'page', $page);
    }

    /**
     * Thêm nôi dung
     * @param $data
     * @return mixed
     */
    public function createItem($data){
        return $this->create($data);
    }

    public function updateItem($array, $id)
    {
        $select = $this->where("{$this->table}.id", $id);

        return $select->update($array);
    }
}
