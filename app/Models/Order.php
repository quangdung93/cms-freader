<?php

namespace App\Models;

use App\Scopes\StoreScope;
use App\Traits\HasUUID;
use App\Traits\LogActivity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes, HasUUID;

    protected $table = 'orders';

    protected $primaryKey = "id";
    protected $keyType = "string";
    protected $guarded = [];

    const NEW = 'new';
    const PAID = 'paid';
    const FINISH = 'finish';

    // protected static function boot(){
    //     parent::boot();
    //     static::addGlobalScope(new StoreScope);
    // }
    public $timestamps = true;

    public function detail(){
        return $this->belongsToMany(Book::class, 'order_detail','order_id', 'book_id')
        ->withPivot('id', 'quantity', 'price', 'subtotal', 'discount');
    }
    public function orderDetail(){
        return $this->hasMany(OrderDetail::class, 'order_id')
        ->leftJoin("book_owner", "book_owner.order_detail_id", "order_detail.id")
        ->select("order_detail.*", "book_owner.status as dis_status");
    }
    public function province(){
        return $this->belongsTo(Province::class);
    }

    public function district(){
        return $this->belongsTo(District::class);
    }

    public function ward(){
        return $this->belongsTo(Ward::class);
    }

    public function saler(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function customer(){
        return $this->belongsTo(Customer::class, 'user_id');
    }

    public function scopeType($query, $type){
        return $query->where('order_type', $type);
    }

    public function shipping(){
        return $this->hasOne(Shipping::class, 'order_id');
    }

    public function scopeWithHas($query, $relation, $callback){
        return $query->whereHas($relation, $callback)->with([$relation => $callback]);
    }

    public function buildQueryFilter($query, $filters){
        if($filters['date_from']){
            $query->where('created_at', '>=', format_date($filters['date_from'], 'Y-m-d 00:00:00'));
        }

        if($filters['date_to']){
            $query->where('created_at', '<=', format_date($filters['date_to'], 'Y-m-d 23:59:59'));
        }

        return $query;
    }

    public function getOrderSource($filters){
        $query = Order::selectRaw("status,
                SUM(price) as total_money, 
                count(id) as count");

        $query = $this->buildQueryFilter($query, $filters);

        $data = $query->groupBy('status')
                ->pluck('total_money', 'status')
                ->toArray();

        return $data;
    }

    public function getOrderRevenue($filters, $type){
        $query = self::query();

        if($type == 'day'){
            $query->selectRaw("DAY(created_at) as date, 
                MONTH(created_at) as month,
                YEAR(created_at) as year,
                SUM(price) as total_money");
        }
        elseif($type == 'month'){
            $query->selectRaw("MONTH(created_at) as date, 
                MONTH(created_at) as month,
                YEAR(created_at) as year,
                SUM(price) as total_money");
        }
        elseif($type == 'year'){
            $query->selectRaw("YEAR(created_at) as date, 
                MONTH(created_at) as month,
                YEAR(created_at) as year,
                SUM(price) as total_money");
        }

        $query->whereIn('status', [self::NEW, self::PAID, self::FINISH]);

        $query = $this->buildQueryFilter($query, $filters);

        $data = $query->groupBy('date', 'month', 'year')
                ->orderBy('date', 'ASC')
                ->orderBy('month', 'ASC')
                ->orderBy('year', 'ASC')
                ->get()->toArray();

        return $data;
    }
}
