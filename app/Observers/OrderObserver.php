<?php

namespace App\Observers;

use App\Models\Order;
use App\Models\Customer;
use App\Models\CustomerPoint;
use App\Services\OrderService;
use Illuminate\Support\Facades\Log;

class OrderObserver
{
    /**
     * Handle the Order "created" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function created(Order $order)
    {
        //
    }

    /**
     * Handle the Order "updated" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function updated(Order $order)
    {   
        if($order->isDirty('status')){ //Thay đổi trạng thái đơn hàng

            $orderDetail = $order->load('detail');

            // Tính số tiền được phép tích điểm
            $totalPriceAllowPoints = (new OrderService)->calculatorAddPoints($orderDetail);

            if($order->status == Order::FINISH){ // Cộng điểm nếu trạng thái là hoàn thành
                $customer = Customer::where('id', $order->customer_id)->first();

                if($totalPriceAllowPoints['totalPrice'] > 0){
                    if($order->exchange_points == 0){ //Chưa tích điểm
                        //Tích điểm
                        if($customer){
                            $newPoints = calCustomerPoints($totalPriceAllowPoints['totalPrice']);
                            $points = $customer->points;
                            $customer->points = $points + $newPoints;
                            $customer->save();

                            Log::info($totalPriceAllowPoints);

                            $description = '';
                            foreach ($totalPriceAllowPoints['productAllowPoints'] as $product) {
                                $description .= sprintf('%s (+%s điểm)', $product['product_name'], $product['points']);
                            }
    
                            //History
                            $dataCustomerPoints = [
                                'customer_id' => $order->customer_id,
                                'order_id' => $order->id,
                                'action' => 'add',
                                'points' => $newPoints,
                                'note' => "Cộng ($newPoints) điểm đơn hàng ($order->code) hoàn thành! ($description)"
                            ];
    
                            CustomerPoint::create($dataCustomerPoints);
                        }   
                    }
                    else{ //Trừ điểm nếu đã sửa dụng tích điểm
                        if($customer){
                            $points = $customer->points;
                            $customer->points = $points - $order->exchange_points;
                            $customer->save();
    
                            //History
                            $dataCustomerPoints = [
                                'customer_id' => $order->customer_id,
                                'order_id' => $order->id,
                                'action' => 'remove',
                                'points' => $order->exchange_points,
                                'note' => "Trừ ($order->exchange_points) điểm đơn hàng ($order->code) đã quy đổi thành công!"
                            ];
    
                            CustomerPoint::create($dataCustomerPoints);
                        }  
                    }
                }
            }
            else if($order->getOriginal('status') == 1 && $order->exchange_points == 0){ // trừ điểm nếu đổi trạng thái
                $customer = Customer::where('id', $order->customer_id)->first();
                if($customer){
                    // Trừ điểm
                    $newPoints = calCustomerPoints($totalPriceAllowPoints['totalPrice']);
                    $points = $customer->points;
                    $customer->points = $points - $newPoints;
                    $customer->save();

                    //History
                    $oldStatus = renderOrderStatus($order->getOriginal('status'));
                    $newStatus = renderOrderStatus($order->status);
                    $dataCustomerPoints = [
                        'customer_id' => $order->customer_id,
                        'order_id' => $order->id,
                        'action' => 'remove',
                        'points' => $newPoints,
                        'note' => "Trừ ($newPoints) điểm đơn hàng ($order->code) thay đổi trạng thái từ $oldStatus đến $newStatus"
                    ];

                    CustomerPoint::create($dataCustomerPoints);
                }
            }
        }
    }

    /**
     * Handle the Order "deleted" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function deleted(Order $order)
    {
        //
    }

    /**
     * Handle the Order "restored" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function restored(Order $order)
    {
        //
    }

    /**
     * Handle the Order "force deleted" event.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function forceDeleted(Order $order)
    {
        //
    }
}
