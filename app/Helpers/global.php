<?php

use Carbon\Carbon;
use App\Models\Task;
use App\Models\Order;
use App\Models\Receipt;
use App\Models\Customer;
use App\Models\Debt;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

const CODE_SUCCESS = 0;
const CODE_ERROR = 1;
const BLOCKED = 'prevent-event';
const CACHE_STORE = 'store_user';

if (!function_exists('getCurrentSlug')) {
    function getCurrentSlug(){
        return request()->path();
    }
}

if (!function_exists('menu')) {
    function menu($menuName, $type = null){
        return (new App\Models\Menu)->display($menuName, $type);
    }
}

if (!function_exists('setting')) {
    function setting($key, $default = null){
        return (new App\Models\Setting)->setting($key, $default);
    }
}

if (!function_exists('format_date')) {
    function format_date($data, $format = 'd/m/Y'){
		if(is_int($data)){
			$data = int_to_date($data);
		}

		$data = str_replace('/', '-', $data);
        return Carbon::parse($data)->format($format);
    }
}

if (!function_exists('format_datetime')) {
    function format_datetime($data){
		$data = str_replace('/', '-', $data);
        return Carbon::parse($data)->format('d/m/Y - H:i');
    }
}

if (!function_exists('format_price')) {
    function format_price($price){
        return number_format($price);
    }
}

if (!function_exists('asset_image')) {
    function asset_image($path){
		if(Storage::disk('local')->exists('/public/'.$path)){
			return asset('/storage/'.$path);
		}
		else{
			return asset('admin/images/default.png');
		}
    }
}

if (!function_exists('asset_image_public')) {
    function asset_image_public($path){
		if(file_exists(public_path($path))){
			return asset($path);
		}
		else{
			return asset('admin/images/default.png');
		}
    }
}

if (!function_exists('shortcode')) {
	function shortcode($key){
		return App\Helpers\ShortcodeHelper::renderByKey($key);
	}
}

if (!function_exists('renderOrderStatus')) {
	function renderOrderStatus($orderStatus){
		switch ($orderStatus) {
			case 0:
				return 'Chờ xác nhận';
			case 5:
				return 'Đã xuất hàng';
			case 6:
				return 'Đang vận chuyển';
			case 1:
				return 'Hoàn thành';
			case 2:
				return 'Thất bại';
			case 3:
				return 'Hủy';
			default:
				break;
		}
	}
}

if (!function_exists('renderOrderStatusLabel')) {
	function renderOrderStatusLabel($status){
		switch ($status) {
			case 'new':
				return ['Chờ xác nhận', 'info'];
			case 'paid':
				return ['Đã thanh toán', 'primary'];
			case 'finish':
				return ['Đã hoàn thành', 'success'];
			case 'cancel':
				return ['Huỷ', 'danger'];
			default:
				return ['', 'info'];
				break;
		}
	}
}

if (!function_exists('renderOrderInventoryStatusLabel')) {
	function renderOrderInventoryStatusLabel($inventoryStatus){
		switch ($inventoryStatus) {
			case Order::INVENTORY_NOT_EXPORT:
				return ['Chưa xuất kho', 'danger'];
			case Order::INVENTORY_EXPORT:
				return ['Đã xuất kho', 'success'];
			default:
				return ['', 'info'];
				break;
		}
	}
}

if (!function_exists('renderOrderAdminStatusLabel')) {
	function renderOrderAdminStatusLabel($adminStatus){
		switch ($adminStatus) {
			case Order::ADMIN_WAITING:
				return ['Chưa xác nhận', 'danger', 'x'];
			case Order::ADMIN_APPROVE:
				return ['Đã xác nhận', 'success', 'check'];
			default:
				return ['', 'info'];
				break;
		}
	}
}

if (!function_exists('renderTaskStatus')) {
	function renderTaskStatus($status){
		switch ($status) {
			case Task::STATUS_NEW:
				return ['Mới', 'info'];
			case Task::STATUS_PROGRESS:
				return ['Đang tiến hành', 'warning'];
			case Task::STATUS_PENDING:
				return ['Tạm ngưng', 'warning'];
			case Task::STATUS_FINISH:
				return ['Hoàn thành', 'success'];
			case Task::STATUS_CANCEL:
				return ['Hủy bỏ', 'danger'];
			default:
				return ['', 'info'];
				break;
		}
	}
}

if (!function_exists('renderReceiptStatus')) {
	function renderReceiptStatus($status){
		switch ($status) {
			case Receipt::FINISH:
				return ['Hoàn thành', 'success'];
			case Receipt::CANCEL:
				return ['Đã hủy', 'danger'];
			default:
				return ['', 'info'];
				break;
		}
	}
}

if (!function_exists('renderDebtStatus')) {
	function renderDebtStatus($status){
		switch ($status) {
			case Debt::UNPAID:
				return ['Chưa thanh toán', 'warning'];
			case Debt::PAID:
				return ['Đã thanh toán', 'success'];
			case Debt::CANCEL:
				return ['Đã hủy', 'danger'];
			default:
				return ['', 'info'];
				break;
		}
	}
}

if (!function_exists('renderTaskType')) {
	function renderTaskType($type){
		switch ($type) {
			case Task::TYPE_CALL:
				return ['Gọi điện', 'info'];
			case Task::TYPE_EMAIL:
				return ['Gửi mail', 'success'];
			case Task::TYPE_MESSAGE:
				return ['Nhắn tin', 'warning'];
			case Task::TYPE_METTING:
				return ['Họp', 'primary'];
			case Task::TYPE_ADVISE:
				return ['Tư vấn', 'info'];
			case Task::TYPE_CUSTOMER_CARE:
				return ['Chăm sóc khách hàng', 'danger'];
			case Task::TYPE_OTHER:
				return ['Khác', 'success'];
			default:
				return ['', 'info'];
				break;
		}
	}
}

if (!function_exists('renderTaskPriority')) {
	function renderTaskPriority($priority){
		switch ($priority) {
			case Task::PRIORITY_LOW:
				return ['Thấp', 'success'];
			case Task::PRIORITY_MEDIUM:
				return ['Trung bình', 'warning'];
			case Task::PRIORITY_HIGH:
				return ['Cao', 'danger'];
			default:
				return ['', 'info'];
				break;
		}
	}
}

if (!function_exists('exchangePoints')) {
	function exchangePoints($point){
		$array = [
			100 => 300000,
			200 => 600000,
			300 => 1000000,
		];

		return isset($array[$point]) ? $array[$point] : 0;
	}
}

if (!function_exists('renderOrderSource')) {
	function renderOrderSource($source){
		switch ($source) {
			case 'new':
				return 'Mới';
			case 'paid':
				return 'Đã thanh toán';
			case 'finish':
				return 'Hoàn thành';
			case 'cancel':
				return 'Hủy';
			default:
				return 'Không xác định';
		}
	}
}

if (!function_exists('renderPaymentMethod')) {
	function renderPaymentMethod($paymentMethod){
		switch ($paymentMethod) {
			case 1:
				return 'Tiền mặt';
			case 2:
				return 'COD';
			case 3:
				return 'Chuyển khoản';
			case 5:
				return 'Cà thẻ';
			case 6:
				return 'Website';
			case 7:
				return 'Công nợ';
			default:
				break;
		}
	}
}

if (!function_exists('getCustomerGroup')) {
	function getCustomerGroup($customerGroup){
		switch ($customerGroup) {
			case 0:
				return 'Khách lẻ';
			case 1:
				return 'Đại lý';
			case 2:
				return 'Nhân viên sale';
			default:
				break;
		}
	}
}

if (!function_exists('handle_show_attribute')) {
    function handle_show_attribute($key, $value, $model){
		if($key == 'price' || $key == 'price_old'){
			return format_price($value);
		}
		elseif($key == 'brand_id' && $model == 'App\Models\Product'){
			return App\Models\Brand::find($value)->name;
		}
		elseif($key == 'category_id'){
			if($model == 'App\Models\Product'){
				return App\Models\Category::find($value)->name;
			}
			elseif($model == 'App\Models\Post'){
				return App\Models\PostCategory::find($value)->name;
			}
		}
		elseif(in_array($key, ['content', 'body'])){
			return 'Chỉnh sửa nội dung';
		}
		
		return $value;
    }
}

if (!function_exists('theme')) {
    function theme($key, $default = null){
		return (new App\Models\ThemeOption())->getThemeOption($key, $default);
    }
}

if (!function_exists('category')) {
	function category($category_id){
		return (new App\Models\Category())->getCategory($category_id);
    }
}

if (!function_exists('isDeveloper')) {
	function isDeveloper(){
		$user = Auth::user();
		return $user->isDeveloper();
    }
}

if (!function_exists('isAdmin')) {
	function isAdmin(){
		$user = Auth::user();
		return $user->isDeveloper() || $user->isAdmin();
    }
}

if (!function_exists('calCustomerLevel')) {
	function calCustomerLevel($totalMoney){
		$levelClass = '';

		if($totalMoney < Customer::TYPE_NON_MEMBER){
            $typeCustomer = 'Chưa đạt hạng';
            $key = 'non-member';
			$levelClass = 'label-default';
        }
        else if($totalMoney >= Customer::TYPE_NON_MEMBER && $totalMoney < Customer::TYPE_MEMBER){ // < 30tr
            $typeCustomer = 'Thành viên';
            $key = 'member';
			$levelClass = 'label-info';
        }
        else if($totalMoney >= Customer::TYPE_MEMBER && $totalMoney < Customer::TYPE_SILVER){
            $typeCustomer = 'Bạc';
            $key = 'silver';
			$levelClass = 'label-success';
        }
        else if($totalMoney >= Customer::TYPE_SILVER && $totalMoney < Customer::TYPE_GOLDEN){
            $typeCustomer = 'Vàng';
            $key = 'golden';
			$levelClass = 'label-warning';
        }
		else if($totalMoney >= Customer::TYPE_GOLDEN && $totalMoney < Customer::TYPE_PLATINUM){
            $typeCustomer = 'Bạch kim';
            $key = 'platinum';
			$levelClass = 'label-primary';
        }
        else if($totalMoney >= Customer::TYPE_PLATINUM){
            $typeCustomer = 'Kim cương';
            $key = 'diamond';
			$levelClass = 'label-danger';
        }

		return [
			'level' => $typeCustomer,
			'key' => $key,
			'class' => $levelClass,
		];
    }
}

if (!function_exists('calTotalBuyCustomer')) {
	function calTotalBuyCustomer($customerId){
        $result = Order::selectRaw("count(store_id) as quantity, 
                SUM(total_money) as total_money, 
                SUM(lack) as total_debt, 
                SUM(coupon) as total_discount, 
                SUM(total_quantity) as total_quantity")
                ->where('status', 1)
                // ->where('admin_status', 1)
                ->where('customer_id', $customerId)->first()->toArray();

        return isset($result['total_money']) ? $result['total_money'] : 0;
    }
}

if (!function_exists('calCustomerPoints')) {
	function calCustomerPoints($totalMoney){
        return round($totalMoney / 100000, 0);
    }
}


if (!function_exists('isLeader')) {
	function isLeader($less = false){ //less = true => lấy quyền thấp hơn
		$user = Auth::user();

		if($less){
			return $user->isLeader() || $user->isSales();
		}

		return $user->isDeveloper() || $user->isAdmin() || $user->isLeader();
    }
}

if (!function_exists('isSales')) {
	function isSales(){
		$user = Auth::user();
		return $user->isSales();
    }
}

if (!function_exists('isRoleAgency')) {
	function isRoleAgency(){
		$user = Auth::user();
		if($user->isAdmin() || $user->can('read_agents')){
			return true;
		}

		return false;
    }
}

if (!function_exists('isRoleCustomer')) {
	function isRoleCustomer(){
		$user = Auth::user();
		if($user->isAdmin() || $user->can('read_customers')){
			return true;
		}

		return false;
    }
}

if (!function_exists('isRoleCustomerSale')) {
	function isRoleCustomerSale(){
		$user = Auth::user();
		if($user->isAdmin() || $user->can('read_customer_salers')){
			return true;
		}

		return false;
    }
}

if (!function_exists('get_embed_youtube')) {
	function get_embed_youtube($url){
		try{
			parse_str( parse_url( $url, PHP_URL_QUERY ),$embedUrl);
			return $embedUrl['v'];
		}
		catch(\Exception $e){
			return null;
		}
	}
}

if (!function_exists('split_textarea')){
	function split_textarea($text){
		return array_filter(preg_split('/\r\n|[\r\n]/', $text));
	}
}

if (!function_exists('render_split_textarea')){
	function render_split_textarea($text, $tag = 'p'){
		$list = split_textarea($text);
		$html = '';
		if(count($list) > 0){
			foreach($list as $item){
				$html .= '<'.$tag.'>' .$item. '</'.$tag.'>';
			}
		}

		return $html;
	}
}


if (!function_exists('post_excerpt')){
	function post_excerpt($content, $limit = 30){
		return strip_tags(Str::words($content, $limit));
	}
}

if (!function_exists('product_template')){
	function product_template($products){
		if(!$products){
			return null;
		}

		$html = '';

		foreach ($products as $item) {
			$discount = $priceOld = '';
			if($item->discount > 0){
				$discount = '<div class="discount">-'.round($item->discount, 1).'%</div>';
				$priceOld = '<span class="price-old">'.number_format($item->price_old).'đ</span>';
			}
			
			$html .= '<div class="product-item">
				<div class="product-img">
					<a href="'.url($item->link()).'">
						<img class="lazy" data-src="'.asset($item->image).'" alt="'.$item->name.'"/>
					</a>
					'.$discount.'
				</div>
				<div class="product-info">
					<h3 class="product-title">
						<a href="'.url($item->link()).'">'.$item->name.'</a>
					</h3>
					<div class="product-price">
						'.$priceOld.'
						<span class="price-new">'.number_format($item->price).'đ</span>
					</div>
					<div class="note-price">(Giá chưa bao gồm VAT)</div>
					<div class="btn add-cart-product-temp" data-id="'.$item->id.'">Thêm vào giỏ hàng</div>
				</div>
			</div>';
			
		}

		return $html;
	}
}

function detectDevice()
{
    if(!isset($_SERVER['HTTP_USER_AGENT']) || !isset($_SERVER['HTTP_ACCEPT'])){
        return 'desktop';
    }

    $mobile_browser = 0;
    if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
        $mobile_browser++;
    }

    if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
        $mobile_browser++;
    }

    if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0)
        or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))
    ) {
        $mobile_browser++;
    }

    $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
    $mobile_agents = array(
        'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
        'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
        'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
        'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
        'newt','noki','palm','pana','pant','phil','play','port','prox',
        'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
        'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
        'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
        'wapr','webc','winw','winw','xda ','xda-');

    if (in_array($mobile_ua,$mobile_agents)) {
        $mobile_browser++;
    }

    if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
        $mobile_browser++;
        //Check for tablets on opera mini alternative headers
        $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])
            ? $_SERVER['HTTP_X_OPERAMINI_PHONE_UA']
            : (isset($_SERVER['HTTP_DEVICE_STOCK_UA']) ? $_SERVER['HTTP_DEVICE_STOCK_UA'] : ''));
        if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
            $mobile_browser++;
        }
    }

    if ($mobile_browser > 0) {
        return 'mobile';
    } else {
        return 'desktop';
    }
}

if(!function_exists('int_to_date')){
	function int_to_date($int){
		$time  = gmdate("Y/m/d h:m ", $int);
		return $time;
	}
}

if(!function_exists('qrCodeGenerate')){
	function qrCodeGenerate($string, $pathStore = null, $size = 100, $margin = 3){
		return QrCode::size($size)->margin($margin)->errorCorrection('H')->generate($string, $pathStore);
	}
}



if(!function_exists('getAgoTime')){
	function getAgoTime($date){
		Carbon::setLocale('vi');
	    $dt = Carbon::createFromFormat('Y-m-d H:i:s', $date);
	    $now = Carbon::now();
	    return $dt->diffForHumans($now); 
	}
}

if(!function_exists('getRangeDay')){
	function getRangeDay($dateFrom, $dateTo){
		return CarbonPeriod::create(date('Y-m-d', strtotime($dateFrom)), date('Y-m-d', strtotime($dateTo)));
	}
}

if(!function_exists('getKeyCache')){
	function getKeyCache($prefix = CACHE_STORE){
		return sprintf($prefix.':%s', Auth::id());
	}
}

if(!function_exists('logger')){
    function logger($title = 'Zalo API', $context = [], $level = 'info'){

        if(!is_array($context)){
            $context = (array)$context;
        }

        switch ($level) {
            case 'info':
                Log::info($title, $context);
                break;
            case 'warning':
                Log::warning($title, $context);
                break;
            case 'error':
                Log::error($title, $context);
                break;
            default:
                break;
        }
    }
}