<?php

namespace App\Repositories\Books;

use App\Models\Book;
use App\Models\BookAttributeDetail;
use App\Models\BookAttributes;
use App\Models\StorageObject;
use App\Traits\UploadAttachment;
use App\Traits\UploadImage;
use App\Asynq\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BookRepository implements BookInterface
{
    use UploadImage, UploadAttachment;
    protected $bookTable;
    protected $bookAttribute;
    protected $bookAttributeDetail;
    protected $storageObject;

    protected $asynqClient;
    public function __construct(Book $bookTable, BookAttributes $bookAttribute, BookAttributeDetail $bookAttributeDetail, StorageObject $storageObject, Client $client)
    {
        $this->bookTable = $bookTable;
        $this->bookAttribute = $bookAttribute;
        $this->bookAttributeDetail = $bookAttributeDetail;
        $this->storageObject = $storageObject;
        $this->asynqClient = $client;
    }

    /** Danh sách sách được phân trang
     * @param $input
     * @return mixed
     */
    public function getListPaging($input)
    {
        return $this->bookTable->getBookPaging($input);
    }
    public function approvePublisher($bookId ){
        $book = Book::findOrFail($bookId);
        if($book->status == 'reviewing'){
            DB::beginTransaction();
            $book->status = 'approved';
            $book->save();
            // enqueuing generate book in backend
            $url = $this->storageObject->getStorageValue($bookId);
            if(isset($url)){
                $this->asynqClient->Enqueue([
                    'typename' => 'book:encrypt',
                    'payload' => [
                        'file_path' => $url->data,
                        'book_id' => $book->id
                    ]
                ]);
            }
            DB::commit();

            return [
                "message" => "Duyệt thành công",
                "status" => 200
            ];
        }else{
            return [
                "message" => "Trạng thái sách không đang chờ duyệt",
                "status" => 400
            ];
        }
    }
    public function update($request, $id)
    {
        $attrAll = BookAttributes::all()->where("is_active", 1);
        $validateInput = [
            'name' => 'required',
        ];
        $validateResp = [
            'name.required' => 'Bạn chưa nhập tên sản phẩm',
        ];
        foreach ($attrAll as $attrValue) {
            if ($attrValue->is_required == 1) {
                $attrValidFormat = "attr." . $attrValue->name;
                $validateInput[$attrValidFormat] = "required";
                $validateResp[$attrValidFormat . ".required"] = sprintf("Bạn chưa nhập '%s'", $attrValue->describe);
            }
        }
        $request->validate($validateInput, $validateResp);
        $book = Book::findOrFail($id);

        $price = (int) Str::replace(',', '', $request->price);

        $original_price = 0;
        if ($request->original_price) {
            $original_price = (int) Str::replace(',', '', $request->original_price);
        }

        if ($price == 0) {
            return back()->withErrors(['price' => 'Giá bán phải lớn hơn 0']);
        } elseif ($original_price > 0 && $price > $original_price) {
            return back()->withErrors(['price' => 'Giá bán không được lớn hơn giá gốc']);
        }

        // Avatar image
        if ($request->hasFile('thumbnail')) {
            $imagePath = $this->uploadImage('books', $request->file('thumbnail'));
            // if($imagePath){
            //     // $this->deleteImage($book->thumbnail);
            // }
        } else {
            $imagePath = $book->thumbnail;
        }

        $data = $request->except('thumbnail', 'attr', 'status', 'file_link');
        $data['price'] = $price;
        $data['original_price'] = $original_price;
        $data['thumbnail'] = $imagePath ?? null;
        if($book->status == 'new' || $book->status == 'reviewing' ){
            $data['status'] = is_null($request->status) ? 'new' : 'reviewing';
        }
        DB::beginTransaction();
        if ($request->hasFile('file_link')) {
            $fileLinkData = $this->uploadFile($request->file("file_link"), "temp_docs");
        $this->storageObject->updateStorageLink([
                "object_id" => $book->id,
                "type" => "s3",
                "object" => "book"
            ], [
                "data" => env('STORAGE_PREFIX','s3biz'). ':/' . $fileLinkData
            ]);
        }
        $update = $book->update($data);
        $bookAttrs = $book->attributes()->get();
        $mappingAttr = [];
        foreach ($attrAll as $attrValue) {
            $mappingAttr[$attrValue->name] = $attrValue->id;
            $mappingAttr['type'][$attrValue->name] = $attrValue->type;
        }
        $fileShouldDeleted = [];
        foreach ($request->attr as $attrName => $attrValue) {
            switch ($mappingAttr['type'][$attrName]) {
                case "attachment":
                case "image":
                    if ($request->hasFile("attr." . $attrName)) {
                        $attrValue = $this->uploadFile($request->file("attr." . $attrName), $mappingAttr['type'][$attrName]);
                        foreach ($bookAttrs as $bookAttr) {
                            if ($bookAttr->attribute_id === $mappingAttr[$attrName] && !empty($bookAttr->value)) {
                                // mean exist
                                $fileShouldDeleted[] = $bookAttr->value;
                            }
                        }
                    }
                    break;
            }

            $this->bookAttributeDetail->upsertDetail($mappingAttr[$attrName], $attrValue, $book->id);
        }
        // $book->attributes()->delete();
        if ($update) {
            DB::commit();
            $this->deleteFiles($fileShouldDeleted);
            return redirect('admin/book/edit/' . $id)->with('success', 'Sửa thành công!');
        } else {
            DB::rollBack();
            return redirect('admin/book/edit/' . $id)->with('danger', 'Sửa thất bại!');
        }

    }
    public function store($request)
    {
        $attrAll = BookAttributes::all()->where("is_active", 1);
        $validateInput = [
            'name' => 'required',
            'file_link' => 'required',
        ];
        $validateResp = [
            'name.required' => 'Bạn chưa nhập tên sản phẩm',
            'file_link.required' => 'Bạn chưa chọn tệp sách epub'
        ];
        foreach ($attrAll as $attrValue) {
            if ($attrValue->is_required == 1) {
                $attrValidFormat = "attr." . $attrValue->name;
                $validateInput[$attrValidFormat] = "required";
                $validateResp[$attrValidFormat . ".required"] = sprintf("Bạn chưa nhập '%s'", $attrValue->describe);
            }
        }
        $request->validate($validateInput, $validateResp);

        $price = (int) Str::replace(',', '', $request->price);

        $original_price = 0;
        if ($request->original_price) {
            $original_price = (int) Str::replace(',', '', $request->original_price);
        }

        if ($price == 0) {
            return back()->withErrors(['price' => 'Giá bán phải lớn hơn 0']);
        } elseif ($original_price > 0 && $price > $original_price) {
            return back()->withErrors(['price' => 'Giá bán không được lớn hơn giá gốc']);
        }
        //Avatar image
        if ($request->hasFile('thumbnail')) {
            $imagePath = $this->uploadImage('books', $request->file('thumbnail'));
        }

        $data = $request->except('input_file', 'attr', 'file_link');
        $data['price'] = $price;
        $data['original_price'] = $original_price;
        $data['thumbnail'] = $imagePath ?? null;
        $data['status'] = is_null($request->status) ? 'new' : 'reviewing';
        $data['created_by'] = Auth::id();
        // $data['book_date'] = strtotime(format_date(str_replace('/', '-', $request->book_date), 'Y-m-d'));
        DB::beginTransaction();
        $book = Book::create($data);

        if ($book) {
            if ($request->hasFile('file_link')) {
                $fileLinkData = $this->uploadFile($request->file("file_link"), "temp_docs");
                $book->storage_links()->create([
                    "object_id" => $book->id,
                    "type" => "s3",
                    "object" => "book",
                    "data" => env('STORAGE_PREFIX','s3biz'). ':/' . $fileLinkData
                ]);
            }
            $attr_list = [];
            $mappingAttr = [];
            foreach ($attrAll as $attrValue) {
                $mappingAttr[$attrValue->name] = $attrValue->id;
                $mappingAttr['type'][$attrValue->name] = $attrValue->type;
            }
            foreach ($request->attr as $attr_name => $attr_value) {
                // custom specific case here ('number','text','date','datetime','json','attachment','list_attachment','image','list_image')
                switch ($mappingAttr['type'][$attr_name]) {
                    case "attachment":
                    case "image":
                        if ($request->hasFile("attr." . $attr_name)) {
                            // handle upload file
                            // $attr_value = ""; // override value -> to link to
                            $attr_value = $this->uploadFile($request->file("attr." . $attr_name), $mappingAttr['type'][$attr_name]);
                        }
                        break;
                }
                $attr_list[] = new BookAttributeDetail([
                    "attribute_id" => $mappingAttr[$attr_name],
                    "book_id" => $book->id,
                    "value" => $attr_value,
                ]);
            }
            $book->attributes()->saveMany($attr_list);
            if ($request->hasFile('thumbnail')) {
                $imageDisk = Storage::disk('s3')->path('/public/images');
                $imageUrl = Str::uuid() . '.jpg';
                $imageDisk->put($imageUrl, imagePath);
                $book->thumbnail = $imageUrl;
                // $book->code = $code;
                $book->save();
            }
            DB::commit();
            // if($request->categories){
            //     $book->categories()->attach($request->categories);
            // }

            return redirect('admin/book')->with('success', 'Tạo thành công!');
        } else {
            DB::rollBack();
            return redirect('admin/book')->with('danger', 'Tạo thất bại!');
        }

    }
}
