<?php


namespace App\Repositories\Books;

interface BookInterface{
    /** Danh sách sách được phân trang
     * @param $input
     * @return mixed
     */
    public function getListPaging($input);
    public function store($request);
    public function approvePublisher($bookId);
    public function update($request, $id);
}

