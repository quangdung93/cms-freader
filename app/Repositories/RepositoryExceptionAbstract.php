<?php
namespace App\Repositories;

use Throwable;


abstract class RepositoryExceptionAbstract extends \Exception
{
    protected $errorData = null;


    public function __construct(string $message = "", int $code = 0, $errorData = null, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->errorData = $errorData;
    }


    public function getErrorData()
    {
        return $this->errorData;
    }
}
