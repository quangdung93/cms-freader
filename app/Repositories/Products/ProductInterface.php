<?php


namespace App\Repositories\Products;

interface ProductInterface{
    /** Danh sách sách được phân trang
     * @param $input
     * @return mixed
     */
    public function getListPaging($input);
}

