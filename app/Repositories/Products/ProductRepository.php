<?php

namespace App\Repositories\Products;

use App\Models\Product;

class ProductRepository implements ProductInterface
{
    protected $productTable;

    public function __construct(Product $productTable)
    {
        $this->productTable = $productTable;
    }


    /** Danh sách sách được phân trang
     * @param $input
     * @return mixed
     */
    public function getListPaging($input)
    {
        return $this->productTable->getProductPaging($input);
    }
}

