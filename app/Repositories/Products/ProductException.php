<?php


namespace App\Repositories\Products;

use App\Repositories\RepositoryExceptionAbstract;

class  ProductException extends RepositoryExceptionAbstract
{

    const DEFAULT_CODE = 0;

    public function __construct(int $code = 0, string $message = "")
    {
        parent::__construct($message ?: $this->transMessage($code), 9);
    }

    protected function transMessage($code)
    {
        switch ($code) {
            default:
                return null;
        }
    }

}

