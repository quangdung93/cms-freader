<?php

namespace App\Repositories\Blocks;


use App\Models\Block;
use App\Models\BlockItem;
use App\Models\Product;
use App\Traits\UploadImage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

class BlockRepository implements BlockInterface
{
    use UploadImage;

    protected $blockTable;

    public function __construct(Block $blockTable)
    {
        $this->blockTable = $blockTable;
    }

    /**
     * Trả về datatable của danh sách block
     * @param $input
     * @return mixed
     */
    public function getDatatable($input)
    {
        $table = $this->blockTable->getDataTableQuery($input);
        $data = Datatables::of($table)
//            ->editColumn('id', function ($row) {
//                $path = route("blocks.detail", ['id' => $row->id]);
//                $id = '<div>';
//                $id .= '<a class="text-primary" href="'.$path.'">'. "$row->id" .'</a>';
//                $id .= '</div>';
//                return $id;
//
//            })
            ->editColumn('name', function ($row) {
                $path = route("blocks.detail", ['id' => $row->id]);
                $name = '<div>';
                $name .= '<a class="text-primary" href="'.$path.'">'. $row->name .'</a>';
                $name .= '</div>';
                return $name;

            })
            ->editColumn('type', function ($row) {
                return "<div>$row->type</div>";

            })
            ->editColumn('block_type', function ($row) {
                return "<div>$row->block_type</div>";
            })
            ->editColumn('priority', function ($row) {
                return "<div>$row->priority</div>";
            })
            ->editColumn('is_active', function ($row) {
                return $row->is_active === 1 ? '<label class="label label-success">Hoạt động</label>' : '<label class="label label-warning">Ngưng hoạt động</label>';
            })
            ->addColumn('action', function ($row) {
                $user = Auth::user();
                $action = "";
                if($user->can('edit_homepage')){
                    $editPath = route("blocks.edit", ['id' => $row->id]);;
                    $action .= '<a class="btn btn-primary" href="'. $editPath .'" title="Chỉnh sửa">
                                <i class="feather icon-edit-1"></i></a>';
                }
                $detailPath = route("blocks.detail", ['id' => $row->id]);;
                $action .= '<a class="btn btn-success" href="'. $detailPath .'" target="_blank"><i class="feather icon-eye" title="Xem"></i></a>';

                return $action;
            })
            ->rawColumns(['id', 'name', 'type', 'block_type', 'priority', 'is_active','action'])
            ->make(true);
        return $data;
    }

    /**
     * Chi tiết block
     * @param $id
     * @return mixed
     */
    public function getBlockDetail($id)
    {
        $productTable = app(Product::class);
        $blockTable = app(Block::class);
        $block = $this->blockTable->getItem($id);
        $blockItems = $block->block_items;
        if($block->type == 'slider'){
            $bookIds = [];
            $books = [];
            if(sizeof($blockItems) > 0){
                $block['block_item_priority'] = $blockItems[0]['priority'];
                $bookIds = $blockItems->pluck("data");
                $books = $productTable->getProducts(["book_ids" => $bookIds]);
            }
            foreach ($block->block_items as $item){
                $book = $this->getBookInList($books, $item['data']);
                $item['block_item_id'] = $item['id'] ?? null;
                $item['object_id'] = $book['id'] ?? null;
                $item['object_name'] = $book['name'] ?? null;
                $item['object_thumbnail'] = $book['thumbnail'] ?? null;
                $item['object_price'] = $book['price'] ?? null;
            }
        }else{
            if(sizeof($block->block_items) > 0){
                $block['block_item_priority'] = $blockItems[0]['priority'];
                foreach ($block->block_items as $item){
                    $itemData = json_decode($item['data']);
                    $item['thumbnail'] = $itemData->thumbnail;
                    if(isset($itemData->blocks_id)){
                        $blockBanner = $blockTable->getItem($itemData->blocks_id);
                        $item['block_item_id'] = $item['id'] ?? null;
                        $item['object_type'] = "block";
                        $item['object_id'] = $blockBanner['id'] ?? null;
                        $item['object_name'] = $blockBanner['name'] ?? null;
                        $item['object_thumbnail'] = null;
                        $item['object_price'] = null;
                    }elseif(isset($itemData->book_id)){
                        $book = $productTable->getItem($itemData->book_id);
                        $item['block_item_id'] = $item['id'] ?? null;
                        $item['object_type'] = "book";
                        $item['object_id'] = $book['id'] ?? null;
                        $item['object_name'] = $book['name'] ?? null;
                        $item['object_thumbnail'] = $book['thumbnail'] ?? null;
                        $item['object_price'] = $book['price'] ?? null;
                    }
                }
            }
        }
        return $block;
    }


    private function getBookInList($books, $bookId)
    {
        foreach ($books as $book){
            if($book['id'] == $bookId){
                return $book;
            }
        }
        return null;
    }

    /**
     * Cập nhật block
     * @param $data
     * @return mixed
     */
    public function updateBlock($data)
    {
        try {
            DB::beginTransaction();
            $block = $this->blockTable->getItem($data['id']);
            if($block){
                $this->blockTable->updateItem([
                    'name' => $data['name'],
                    'type' => $data['type'],
                    'block_type' => $data['block_type'],
                    'priority' => $data['priority'] ?? 1,
                    'updated_at' => Carbon::now(),
                    'is_active' => isset($data['is_active']) ? ($data['is_active'] == 'on' ? 1 : 0) : 0,
                ], $data['id']);
                if($data['type'] == $block['type']){
                    $this->removeBlockItem($data['id']);
                    if($data['type'] == 'slider'){
                        $this->insertBlockItems($data);
                    }else{
                        $this->insertBlockBannerItems($data);
                    }
                }
            }
            DB::commit();
            return 1;
        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception("Cập nhật nội dung thất bại: " . $e->getMessage());
        }
    }

    /**
     * Xóa danh sách item của block theo block id
     * @param $id
     * @return mixed
     */
    private function removeBlockItem($id)
    {
        $blockItemTable = app(BlockItem::class);
        return $blockItemTable->removeBlockItems($id);
    }

    private function insertBlockItems($data)
    {
        $blockItemTable = app(BlockItem::class);
        $items = [];
        foreach ($data['data'] as $index => $item){
            $items[] = [
                'blocks_id' => $data['id'],
                'data' => $item,
                'type' => 'text',
                'priority' => $data['priorities'][$index] ?? $index,
                'is_active' => $data['status'][$index] ?? 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
        }
        return $blockItemTable->insertBlockBulk($items);
    }

    private function insertBlockBannerItems($data)
    {
        $blockItemTable = app(BlockItem::class);
        $items = [];
        foreach ($data['data'] as $index => $value){
            $items[] = [
                'blocks_id' => $data['id'],
                'data' => $value,
                'type' => 'json',
                'priority' => $data['priorities'][$index] ?? $index,
                'is_active' => $data['status'][$index] ?? 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];
        }
        return $blockItemTable->insertBlockBulk($items);
    }

    /**
     * Danh sách slider
     * @param array $data
     * @return mixed
     */
    public function getBlocks($data)
    {
        return $this->blockTable->getBlocks($data);
    }

    /**
     * Upload hình của banner
     * @param input
     * @return mixed
     */
    public function addBannerAttribute($input)
    {
        //Avatar image
        if(isset($input['input_file'])){
            $inputFile = $this->uploadImage('blocks', $input['input_file']);
        }
        $objectId = null;
        $objectName = null;
        $data = [
            'thumbnail'     => asset($inputFile)
        ];
        if($input['banner_type'] == 'slider'){
            $objectId = $input['slider_id'];
            $block = $this->blockTable->getItem($input['slider_id']);
            $objectName = $block['name'] ?? null;
            $data['blocks_id'] = $objectId;
        }else{
            $objectId = $input['book_id'];
            $productTable = app(Product::class);
            $book = $productTable->getItem($input['book_id']);
            $objectName = $book['name'] ?? null;
            $data['book_id'] = $objectId;
        }

        return [
            "input_file" => $inputFile ?? null,
            "banner_type" => $input['banner_type'],
            "book_id" => $input['book_id'] ?? null,
            "slider_id" => $input['slider_id'] ?? null,
            "object_id" => $objectId,
            "view" => view('admin.blocks.banner-attribute-item', [
                'item' => [
                    'id' => $input['row_count'] ?? 1,
                    'object_type'   => $input['banner_type'],
                    'object_id'     => $objectId,
                    'thumbnail'     => asset($inputFile),
                    'object_name'   =>  $objectName,
                    'type'   =>  'json',
                    'data' => json_encode($data),
                    'priority' => $input['row_count'],
                    'is_active'   =>  1
                ],
                'isEdit' => true
            ])->render()
        ];
    }

    /**
     * Thêm nôi dung
     * @param array $data
     * @return mixed
     */
    public function storeBlock(array $data)
    {
        try {
            DB::beginTransaction();
            $block = $this->blockTable->createItem([
                'name' => $data['name'],
                'type' => $data['type'],
                'block_type' => $data['block_type'],
                'priority' => $data['priority'] ?? 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'is_active' => isset($data['is_active']) ? ($data['is_active'] == 'on' ? 1 : 0) : 1,
            ]);
            $data['id'] = $block['id'];

            if($data['type'] == 'slider'){
                $this->insertBlockItems($data);
            }else{
                $this->insertBlockBannerItems($data);
            }
            DB::commit();
            return $block;
        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception("Cập nhật nội dung thất bại" . $e->getMessage());
        }
    }

}

