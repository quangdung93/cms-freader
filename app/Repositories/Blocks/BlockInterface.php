<?php


namespace App\Repositories\Blocks;

interface BlockInterface{

    /**
     * Trả về datatable của danh sách block
     * @param array $input
     * @return mixed
     */
    public function getDatatable(array $input);

    /**
     * Chi tiết block
     * @param $id
     * @return mixed
     */
    public function getBlockDetail($id);

    /**
     * Cập nhật block
     * @param $data
     * @return mixed
     */
    public function updateBlock($data);

    /**
     * Danh sách slider
     * @param array $data
     * @return mixed
     */
    public function getBlocks(array $data);

    /**
     * Upload hình của banner
     * @param array $input
     * @return mixed
     */
    public function addBannerAttribute(array $input);

    /**
     * Thêm nôi dung
     * @param array $data
     * @return mixed
     */
    public function storeBlock(array $data);
}

