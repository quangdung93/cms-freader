<?php


namespace App\Repositories\Stores;

interface StoreInterface{

    /**
     * Trả về datatable của danh sách block
     * @param array $input
     * @return mixed
     */
    public function getDatatable(array $input);

    /**
     * Lấy chi tiết cua hang
     * @param array $input
     * @return mixed
     */
    public function getStoreDetail($id);

    /**
     * Thêm chi nhánh
     * @param array $input
     * @return mixed
     */
    public function store($input);

    /**
     * Chỉnh sửa chi nhánh
     * @param $input
     * @return mixed
     */
    public function update($input);

    /**
     * Xóa chi nhánh
     * @param $id
     * @return mixed
     */
    public function destroy($id);
}

