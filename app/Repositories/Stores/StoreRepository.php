<?php

namespace App\Repositories\Stores;


use App\Models\Block;
use App\Models\Store;
use App\Traits\UploadImage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class StoreRepository implements StoreInterface
{
    use UploadImage;
    protected $storeTable;

    public function __construct(Store $storeTable)
    {
        $this->storeTable = $storeTable;
    }

    /**
     * Trả về datatable của danh sách block
     * @param $input
     * @return mixed
     */
    public function getDatatable($input)
    {
        $table = $this->storeTable->getDataTableQuery($input);
        $data = Datatables::of($table)
            ->editColumn('id', function ($row) {
                $path = route("stores.detail", ["id" => $row->id]);
                $id = '<div>';
                $id .= '<a class="text-primary" href="'.$path.'">'. "$row->id" .'</a>';
                $id .= '</div>';
                return $id;

            })
            ->editColumn('name', function ($row) {
                $path = route("stores.detail", ["id" => $row->id]);
                $name = '<div>';
                $name .= '<a class="text-primary" href="'.$path.'">'. $row->name .'</a>';
                $name .= '</div>';
                return $name;

            })
            ->editColumn('phone', function ($row) {
                $phone = $row->phone ?? 'N/A';
                return "<div>$phone</div>";

            })
            ->editColumn('company_name', function ($row) {
                $companyName = $row->company_name ?? 'N/A';
                return "<div>$companyName</div>";
            })
            ->editColumn('status', function ($row) {
                return $row->status === 1 ? '<label class="label label-success">Hoạt động</label>' : '<label class="label label-warning">Ngưng hoạt động</label>';
            })
            ->addColumn('action', function ($row) {
                $user = Auth::user();
                $action = "";
                if($user->can('edit_stores')){
                    $editPath = route("stores.edit", ["id" => $row->id]);
                    $action .= '<a class="btn btn-primary" href="'. $editPath .'" title="Chỉnh sửa">
                                <i class="feather icon-edit-1"></i></a>';
                }
                $detailPath = route("stores.detail", ["id" => $row->id]);
                $action .= '<a class="btn btn-success" href="'. $detailPath .'" target="_blank"><i class="feather icon-eye" title="Xem"></i></a>';

                if($user->can('delete_stores')){
                    $deletePath = route("stores.destroy", ['id' => $row->id]);
                    $action .= '<a href="'. $deletePath . '" class="btn btn-danger notify-confirm" title="Xóa">
                        <i class="feather icon-trash-2"></i>
                    </a>';
                }
                return $action;
            })
            ->rawColumns(['id', 'name', 'phone', 'company_name', 'status','action'])
            ->make(true);
        return $data;
    }

    /**
     * Lấy chi tiết cua hang
     * @param array $input
     * @return mixed
     */
    public function getStoreDetail($id)
    {
        return $this->storeTable->getItem($id);
    }

    /**
     * Thêm chi nhánh
     * @param $input
     * @return mixed
     */
    public function store($input)
    {
        $now = Carbon::now();

        $store = $this->storeTable->getItemByName($input['name']);

        throw_if(!empty($store), new StoreException(StoreException::STORE_NAME_IS_EXITS));

        $data = [
            "name" => $input['name'],
            "company_name" => $input['company_name'],
            "phone" => $input['phone'],
            "address" => $input['address'],
            "bank_account" => $input['bank_account'],
            "facebook" => $input['facebook'],
            "zalo" => $input['zalo'],
            "email" => $input['email'],
            "note" => $input['note'],
            "status" => ($input['status'] ?? "off") == "on" ? 1 : 0,
            "created_at" => $now,
            "updated_at" => $now,
        ];

        if(isset($input['logo'])){
            $logo = $this->uploadImage('stores', $input['logo']);
            $data['logo'] = $logo;
        }


        $store = $this->storeTable->createItem($data);

        return $store;
    }

    /**
     * Chỉnh sửa chi nhánh
     * @param $input
     * @return mixed
     */
    public function update($input)
    {
        $now = Carbon::now();

        $store = $this->storeTable->getItem($input['id']);
        throw_if(empty($store), new StoreException(StoreException::STORE_NOT_FOUND));

        $storeByName = $this->storeTable->getItemByName($input['name'], $input['id']);

        throw_if(!empty($storeByName), new StoreException(StoreException::STORE_NAME_IS_EXITS));

        $data = [
            "name" => $input['name'],
            "company_name" => $input['company_name'],
            "phone" => $input['phone'],
            "address" => $input['address'],
            "bank_account" => $input['bank_account'],
            "facebook" => $input['facebook'],
            "zalo" => $input['zalo'],
            "email" => $input['email'],
            "note" => $input['note'],
            "status" => ($input['status'] ?? "off") == "on" ? 1 : 0,
            "created_at" => $now,
            "updated_at" => $now,
        ];

        if(isset($input['logo'])){
            $logo = $this->uploadImage('stores', $input['logo']);
            $data['logo'] = $logo;
        }
        $this->storeTable->updateItem($data, $input['id']);

        return $this->storeTable->getItem($input['id']);
    }

    /**
     * Xóa chi nhánh
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $store = $this->storeTable->getItem($id);
        throw_if(empty($store), new StoreException(StoreException::STORE_NOT_FOUND));
        $now = Carbon::now();
        $data = [
            "deleted_at" => $now,
            "updated_at" => $now,
        ];
        return $this->storeTable->updateItem($data, $id);
    }
}

