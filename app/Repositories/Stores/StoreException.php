<?php


namespace App\Repositories\Stores;

use App\Repositories\RepositoryExceptionAbstract;

class  StoreException extends RepositoryExceptionAbstract
{

    const DEFAULT_CODE = 0;
    const STORE_NOT_FOUND = 1;
    const STORE_NAME_IS_EXITS = 2;

    public function __construct(int $code = 0, string $message = "")
    {
        parent::__construct($message ?: $this->transMessage($code), 9);
    }

    protected function transMessage($code)
    {
        switch ($code) {
            case self::STORE_NOT_FOUND:
                return "Không tìm thấy chi nhánh";
            case self::STORE_NAME_IS_EXITS:
                return "Tên chi nhánh trùng với chi nhánh khác, vui lòng nhập vào tên khác";
            default:
                return null;
        }
    }

}

