<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckStoreMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userLogin = Auth::user();

        if(!$userLogin){
            return $next($request);
        }

        $stores = $userLogin->stores;
        $userStores = [];
        if($stores){
            $userStores = explode(',', $stores);
        }

        if(count($userStores) == 0){
            return abort('403');
        }

        return $next($request);
    }
}
