<?php
namespace App\Http\Requests\Stores;
use Illuminate\Foundation\Http\FormRequest as Request;

class UpdateStoreRequest  extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $arrRule = [
            'name'              => 'required',
        ];
        $input = request()->all();
        if(isset($input['email'])){
            $arrRule['email'] = 'email';
        }

        return $arrRule;
    }
    /*
     * function custom messages
     */
    public function messages()
    {
        return [
            'name.required'                => 'Vui lòng nhập vào tên chi nhánh',
            'email.email'                => 'Địa chỉ email không đúng định dạng',
        ];
    }

}
