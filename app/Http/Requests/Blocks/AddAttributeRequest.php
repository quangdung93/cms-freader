<?php
namespace App\Http\Requests\Blocks;
use Illuminate\Foundation\Http\FormRequest as Request;

class AddAttributeRequest  extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $arrRule = [
            'banner_type'         => 'required',
            'image'              => 'required',
        ];

        $param = request()->all();

        if($param['banner_type'] == 'slider'){
            $arrRule['slider_id'] = 'required';
        }else{
            $arrRule['book_id'] = 'required';
        }

        return $arrRule;
    }
    /*
     * function custom messages
     */
    public function messages()
    {
        return [
            'banner_type.required'        => 'Vui lòng chọn loại thuộc tính',
            'image.required'              => 'Vui lòng chọn hình banner',
            'slider_id.required'          => 'Vui lòng chọn slider',
            'book_id.required'            => 'Vui lòng chọn sách',
        ];
    }

}
