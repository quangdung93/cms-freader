<?php
namespace App\Http\Requests\Blocks;
use Illuminate\Foundation\Http\FormRequest as Request;

class CreateBlockRequest  extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $arrRule = [
            'name'              => 'required',
            'type'              => 'required',
            'block_type'              => 'required',
            'data'              => 'required',
        ];

        return $arrRule;
    }
    /*
     * function custom messages
     */
    public function messages()
    {
        return [
            'name.required'                => 'Vui lòng nhập vào tên nội dung',
            'type.required'          => 'Vui lòng chọn loại nội dung',
            'block_type.required'                => 'Vui lòng chọn vị trí hiển thị',
            'object_id.required'            => 'Vui lòng chọn sách',
        ];
    }

}
