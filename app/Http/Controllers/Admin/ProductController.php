<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Category;
use App\Models\Supplier;
use App\Repositories\Products\ProductInterface;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    protected $productService;
    protected $product;

    public function __construct(ProductService $productService, ProductInterface $product)
    {
        $this->productService = $productService;
        $this->product = $product;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::where('type', 0)->active()->get();
        $origins = Brand::where('type', 1)->active()->get();
        return view('admin.products.index', compact('brands', 'origins'));
    }

    public function getDatatable(){
        $products = Product::with('categories','brand')->orderByDesc('id');
        return $this->productService->getDatatable($products);
    }

    public function getLists(Request $request){
        $query = Product::query();

        if($request->name){
            $keyword = trim($request->name);
            $query->where('name', 'like', '%'.$keyword.'%');
            $query->orWhere('code', 'like', '%'.$keyword.'%');
        }

        if($request->brand_id){
            $query->where('brand_id', (int)$request->brand_id);
        }

        if($request->origin_id){
            $query->where('origin_id', (int)$request->origin_id);
        }

        $products = $query->with('categories', 'brand')->select('*');

        return $this->productService->getDatatable($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::active()->get();
        $brands = Brand::where('type', 0)->active()->get();
        $origins = Brand::where('type', 1)->active()->get();
        $suppliers = Supplier::active()->get();

        return view('admin.products.add-edit')->with([
            'categories' => $categories,
            'brands' => $brands,
            'origins' => $origins,
            'suppliers' => $suppliers,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'brand_id' => 'required',
        ],[
            'name.required' => 'Bạn chưa nhập tên sản phẩm',
            'brand_id.required' => 'Bạn chưa chọn thương hiệu',
        ]);

        $price = (int)Str::replace(',','', $request->price);

        $origin_price = 0;
        if($request->origin_price){
            $origin_price = (int)Str::replace(',','', $request->origin_price);
        }

        if($price == 0){
            return back()->withErrors(['price' => 'Giá bán phải lớn hơn 0']);
        }
        elseif($origin_price > 0 && $price > $origin_price){
            return back()->withErrors(['price' => 'Giá bán không được lớn hơn giá gốc']);
        }

        $discount = 0;
        if($origin_price > 0){
            $discount = ($origin_price - $price) / $origin_price * 100;
        }

        //Avatar image
        if($request->hasFile('input_file')){
            $avatarPath = $this->uploadImage('products', $request->file('input_file'));
        }

        $data = $request->except('input_file', 'categories');
        $data['slug'] = Str::slug($request->name);
        $data['price'] = $price;
        $data['origin_price'] = $origin_price;
        $data['discount'] = round($discount, 2);
        $data['image'] = $avatarPath ?? null;
        $data['status'] = isset($request->status) ? 1 : 0;
        $data['created_by'] = Auth::id();
        $data['product_date'] = strtotime(format_date(str_replace('/', '-', $request->product_date), 'Y-m-d'));
        $product = Product::create($data);

        if($product){
            $productId = $product->id;

            //Product Code
            $code = 'DC-'.$product->brand_id.'-'.$productId;

            //QR Code
            $codesDir = Storage::disk('local')->path('public/product_code/');
            $codeFileName = $productId.'-'.date('d-m-Y-h-i-s').'.svg';
            $codePath = $codesDir.$codeFileName;
            qrCodeGenerate($code, $codePath);

            $product->image_code = $codeFileName;
            $product->code = $code;
            $product->save();

            if($request->categories){
                $product->categories()->attach($request->categories);
            }

            return redirect('admin/products')->with('success', 'Tạo thành công!');
        }
        else{
            return redirect('admin/products')->with('danger', 'Tạo thất bại!');
        }
    }

    public function fix(){
        $prefix = 'SP';
        $products = Product::all();
        $sku = '';
        $maxCode = 1;

        foreach ($products as $key => $product) {
            if($key == 0){
                $sku = $prefix.'0000001';
            }
            else{
                $maxCode = $maxCode + 1;

                if ($maxCode < 10)
                    $sku = $prefix.'000000' . ($maxCode);
                else if ($maxCode < 100)
                    $sku = $prefix.'00000' . ($maxCode);
                else if ($maxCode < 1000)
                    $sku = $prefix.'0000' . ($maxCode);
                else if ($maxCode < 10000)
                    $sku = $prefix.'000' . ($maxCode);
                else if ($maxCode < 100000)
                    $sku = $prefix.'00' . ($maxCode);
                else if ($maxCode < 1000000)
                    $sku = $prefix.'0' . ($maxCode);
                else if ($maxCode < 10000000)
                    $sku = $prefix.'' . ($maxCode);
            }

            $product->sku = $sku;
            $product->save();
        }

        echo 'Done';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id)->load('images','categories');
        $categories = Category::active()->get();
        $brands = Brand::active()->get();
        $origins = Brand::where('type', 1)->active()->get();
        $suppliers = Supplier::active()->get();

        return view('admin.products.add-edit')->with([
            'product' => $product,
            'categories' => $categories,
            'brands' => $brands,
            'origins' => $origins,
            'suppliers' => $suppliers,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'brand_id' => 'required',
        ],[
            'name.required' => 'Bạn chưa nhập tên sản phẩm',
            'brand_id.required' => 'Bạn chưa chọn thương hiệu',
        ]);

        $product = Product::findOrFail($id);

        $price = (int)Str::replace(',','', $request->price);

        $origin_price = 0;
        if($request->origin_price){
            $origin_price = (int)Str::replace(',','', $request->origin_price);
        }

        if($price == 0){
            return back()->withErrors(['price' => 'Giá bán phải lớn hơn 0']);
        }
        elseif($origin_price > 0 && $price > $origin_price){
            return back()->withErrors(['price' => 'Giá bán không được lớn hơn giá gốc']);
        }

        $discount = 0;
        if($origin_price > 0){
            $discount = ($origin_price - $price) / $origin_price * 100;
        }

        //Avatar image
        if($request->hasFile('input_file')){
            $avatarPath = $this->uploadImage('products', $request->file('input_file'));
            if($avatarPath){
                $this->deleteImage($product->image);
            }
        }else{
            $avatarPath = $product->image;
        }

        $data = $request->except('input_file','categories');
        $data['price'] = $price;
        $data['origin_price'] = $origin_price;
        $data['discount'] = round($discount, 2);
        $data['image'] = $avatarPath ?? null;
        $data['status'] = isset($request->status) ? 1 : 0;
        $data['created_by'] = Auth::id();
        $data['product_date'] = strtotime(format_date(str_replace('/', '-', $request->product_date), 'Y-m-d'));

        $update = $product->update($data);

        if($update){
            //Update category
            $product->categories()->sync($request->categories);

            return redirect('admin/products/edit/' . $id)->with('success', 'Sửa thành công!');
        }
        else{
            return redirect('admin/products/edit/'. $id)->with('danger', 'Sửa thất bại!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $delete = $product->delete();

        if($delete){
            return redirect('admin/products')->with('success', 'Xóa thành công!');
        }
    }

    public function search(\Illuminate\Http\Request $request)
    {
        $key = $request->input('key');
        if (!empty($key) || !empty($request->get('ids_include'))) {
            $listExistedIds = [];

            $ids = $request->get('ids');
            if (!empty($ids)) {
                $listExistedIds = explode(',', $ids);
            }
            $listIdInclude = [];
            $ids_include = $request->get('ids_include');
            if (!empty($ids_include)) {
                $listIdInclude = explode(',', $ids_include);
            }
            $product = new Product();

            $result = $product->searchProductWithoutIds($key, $listExistedIds, 10, $listIdInclude);
            if ($result) {
                return $this->responseJson(CODE_SUCCESS, $result);
            }
        }

        return $this->responseJson(CODE_ERROR, null, 'Không tìm thấy sản phẩm');
    }

    public function searchPaging(Request $request)
    {
        try{
            $input = $request->all();

            $data = $this->product->getListPaging($input);

            return [
                'error' => 0,
                'data' => $data,
                'message' => 'Lấy sách thành công'
            ];
        }catch (\Exception $exception){
            return [
                'error' => 1,
                'message' => $exception->getMessage()
            ];
        }
    }

}
