<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\Permission;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\PermissionGroup;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\PermissionGroupDetail;

class RoleController extends Controller
{
    protected $listPermission = ['users','roles','posts','products','pages','menus','settings'];
    public function index(){
        $roles = Role::all();
        return view('admin.roles.index')->with(compact('roles'));
    }

    public function create(){
        return view('admin.roles.add');
    }

    public function store(Request $request){
        $data = [
            'name' => Str::slug($request->name),
            'display_name' => $request->display_name
        ];
        Role::create($data);
        
        return redirect('admin/roles');
    }

    public function edit(Request $request, $id){
        $role = Role::findOrFail($id)->load('permissions', 'permissionGroups');

        $permissionGroups = PermissionGroup::all();

        $permissionIdsByGroup = $role->permissionGroups ? $role->permissionGroups->pluck('id')->toArray() : [];

        $permissionGroupDetail = PermissionGroupDetail::whereIn('permission_group_id', $permissionIdsByGroup)->get();

        $permissionIdsByChooseGroup = $permissionGroupDetail ? $permissionGroupDetail->pluck('permission_id')->toArray() : [];

        //Check permission edit
        if($role->name == config('permission.role_dev') 
        && !Auth::user()->hasRole(config('permission.role_dev'))){
            return abort(403);
        }

        $permissions = Permission::with('feature')->get();

        return view('admin.roles.edit')
            ->with([
                'role' => $role, 
                'permissions' => $permissions,
                'permissionGroups' => $permissionGroups,
                'permissionIdsByChooseGroup' => $permissionIdsByChooseGroup,
            ]);
    }

    public function update(Request $request, $id){
        $role = Role::findOrFail($id);

        $role->name = Str::slug($request->name);
        $role->display_name = $request->display_name;
        $role->save();

        $permissionIdByGroup = [];
        if($request->permission_groups){
            $role->permissionGroups()->sync($request->permission_groups);

            $permissionGroupDetail = PermissionGroupDetail::whereIn('permission_group_id', $request->permission_groups)->get();
            $permissionIdByGroup = $permissionGroupDetail ? $permissionGroupDetail->pluck('permission_id')->toArray() : [];
        }

        if($request->permission){
            $permissionIds = array_keys($request->permission);
            $totalPermissionIds = array_merge($permissionIds, $permissionIdByGroup);
            $totalPermissionIds = array_values(array_unique($totalPermissionIds));
            $role->syncPermissions($totalPermissionIds);
        }

        return redirect('admin/roles/edit/'.$id)->with('success','Cập nhật thành công!');
    }

    public function createPermission(Request $request){
        if(Auth::user()->roles->first()->name != config('permission.role_dev')){
            return abort(403);
        }

        $permission = $request->permission;
        $name = $request->name ?? "";

        Permission::create(['name' => 'read_'.$permission, 'display_name' => $name, 'groups' => $permission]);
        Permission::create(['name' => 'add_'.$permission, 'display_name' => 'Thêm '.$name,'groups' => $permission]);
        Permission::create(['name' => 'edit_'.$permission, 'display_name' => 'Sửa '.$name,'groups' => $permission]);
        Permission::create(['name' => 'delete_'.$permission, 'display_name' => 'Xóa '.$name,'groups' => $permission]);

        return 'Create Permission Success!';
    }
}
