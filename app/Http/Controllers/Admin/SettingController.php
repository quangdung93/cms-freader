<?php

namespace App\Http\Controllers\Admin;

use App\Models\Brand;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Category;
use App\Rclone\RcloneClient;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class SettingController extends Controller
{
    protected $rcloneClient;
    public function __construct(
        RcloneClient $client
        )
    {
        $this->rcloneClient = $client;
    }

    public function index(){
        $settings = Setting::all();
        return view('admin.settings.index')->with([
            'settings' => $settings
        ]);
    }

    public function store(Request $request){
        $request->validate([
            'key' => 'required|unique:settings',
        ],[
            'key.required' => 'Key không được trống',
            'key.unique' => 'Key đã tồn tại',
        ]);

        $data = $request->all();
        $data['order'] = (int)Setting::where('group', $data['group'])->max('order') + 1;
        $setting = Setting::create($data);

        if($setting){
            return redirect('admin/settings')->withSuccess('Tạo thành công!');
        }
        else{
            return redirect('admin/settings')->with('danger', 'Tạo thất bại!');
        }
    }

    public function update(Request $request)
    {
        $data = $request->except('_token');
        foreach ($data as $key => $value) {

            //Image
            if($request->hasFile($key)){
                $imagePath = $this->uploadImage('settings', $request->file($key));
                if($imagePath){
                    $setting = Setting::where('key', $key)->first();
                    $this->deleteImage($setting->value);
                    $value = $imagePath;
                }
            }

            $setting = Setting::where('key', $key)->first();
            $update = $setting->update(['value' => $value]);
        }

        return redirect('admin/settings')->with('success','Cập nhật thành công!');
    }

    public function destroy($id)
    {
        $setting = Setting::findOrFail($id);

        $delete = $setting->delete();

        if($delete){
            return redirect('admin/settings')->withSuccess('Xóa thành công!');
        }
        else{
            return redirect('admin/settings')->with('danger', 'Xóa thất bại!');
        }
    }

    public function order(Request $request){
        $order = $request->order;
        
        foreach ($order as $item) {
            Setting::where('id', $item['id'])->update(['order' => $item['order']]);
        }

        return response()->json(['status' => true]);
    }

    public function getConfigProduct(){
        $categories = Category::active()->get();
        $brands = Brand::active()->get();
        $settingProducts = explode(',', setting('config_not_points_products'));
        if($settingProducts){
            $products = Product::whereIn('id', $settingProducts)->get();
        }

        return view('admin.products.config')->with([
            'categories' => $categories, 
            'brands' => $brands,
            'settingCategories' => explode(',', setting('config_not_points_categories')),
            'settingBrands' => explode(',', setting('config_not_points_brand')),
            'settingProducts' => $products ? $products->reverse() : null,
        ]);
    }

    public function postConfigProduct(Request $request){
        $settingCategories = Setting::where('key', 'config_not_points_categories')->first();

        $categories = $request->categories ? implode(',', $request->categories) : null;
        if($settingCategories){
            $settingCategories->update(['value' => $categories]);
        }

        $settingBrands = Setting::where('key', 'config_not_points_brand')->first();

        $brands = $request->brands ? implode(',', $request->brands) : null;
        if($settingCategories){
            $settingBrands->update(['value' => $brands]);
        }

        $productIds = [];
        if($request->products){
            foreach ($request->products as $value) {
                $productIds[] = $value['id'];
            }
        }

        $settingProducts = Setting::where('key', 'config_not_points_products')->first();
        $products = $productIds ? implode(',', $productIds) : null;
        if($settingProducts){
            $settingProducts->update(['value' => $products]);
        }

        Cache::forget('settings');

        return redirect(route('admin.config.points'));
    }
    public function rcloneHealthCheck(Request $request){
            return response()->json($this->rcloneClient->checkConfig(
                $request->config
            ));
    }
}
