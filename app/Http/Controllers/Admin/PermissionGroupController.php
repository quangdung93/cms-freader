<?php

namespace App\Http\Controllers\Admin;

use App\Models\Permission;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use App\Models\PermissionGroup;
use Illuminate\Support\Facades\Auth;

class PermissionGroupController extends Controller
{
    public function index(){
        $permissionGroups = PermissionGroup::all();
        return view('admin.permission_group.index')->with(compact('permissionGroups'));
    }

    public function create(){
        return view('admin.permission_group.add');
    }

    public function store(Request $request){
        $request->validate([
            'group_name' => 'required',
            'group_code' => 'required|unique:permission_groups',
        ],[
            'group_name.required' => 'Bạn chưa nhập tên nhóm quyền',
            'group_code.required' => 'Bạn chưa nhập mã nhóm quyền',
            'group_code.unique' => 'Mã nhóm quyền đã tồn tại',
        ]);

        $data = [
            'group_name' => $request->group_name,
            'group_code' => $request->group_code
        ];

        PermissionGroup::create($data);
        
        return redirect('admin/permissions/group');
    }

    public function edit(Request $request, $id){
        $permissionGroup = PermissionGroup::findOrFail($id)->load('permissions');
        $permissions = Permission::with('feature')->get();

        return view('admin.permission_group.edit')
            ->with([
                'permissionGroup' => $permissionGroup, 
                'permissions' => $permissions
            ]);
    }

    public function update(Request $request, $id){
        $request->validate([
            'group_name' => 'required',
            'group_code' => 'required|unique:permission_groups,group_code,'.$id,
        ],[
            'group_name.required' => 'Bạn chưa nhập tên nhóm quyền',
            'group_code.required' => 'Bạn chưa nhập mã nhóm quyền',
            'group_code.unique' => 'Mã nhóm quyền đã tồn tại',
        ]);

        $permissionGroup = PermissionGroup::findOrFail($id)->load('permissions');

        $permissionGroup->group_name = $request->group_name;
        $permissionGroup->group_code = $request->group_code;
        $permissionGroup->save();

        if($request->permission){
            $permissionIds = array_keys($request->permission);

            //Lưu vào detail
            $permissionGroup->permissions()->sync($permissionIds);
        }

        return redirect('admin/permissions/group/edit/'.$id)->with('success','Cập nhật thành công!');
    }
    
}
