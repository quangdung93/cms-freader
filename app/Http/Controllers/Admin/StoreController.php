<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Stores\CreateStoreRequest;
use App\Http\Requests\Stores\UpdateStoreRequest;
use App\Repositories\Stores\StoreInterface;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    protected $storeRepo;

    public function __construct(StoreInterface $storeRepo){
        $this->storeRepo = $storeRepo;
    }
    //

    /**
     * Màn hình danh sách các block
     * @return Application|Factory|View
     */
    public function indexAction(){
        return view('admin.stores.index');
    }

    /**
     * Màn hình danh sách các block
     * @return Application|Factory|View
     */
    public function create(){
        return view('admin.stores.add');
    }

    /**
     * Màn hình danh sách các block
     * @return Application|Factory|View
     */
    public function edit($id){
        $data = $this->storeRepo->getStoreDetail($id);
        return view('admin.stores.edit', ['data' => $data]);
    }

    /**
     * Màn hình danh sách các block
     * @return Application|Factory|View
     */
    public function storeAction(CreateStoreRequest $request){
        try{
            $input = $request->all();

            $this->storeRepo->store($input);

            session()->flash('success', "Tạo chi chi nhánh thành công");
            return redirect(route('stores.create'));
        }catch (\Exception $exception){
            session()->flash('danger', $exception->getMessage());
            return view('admin.stores.add', ['data' => $input]);
        }
    }

    /**
     * Màn hình danh sách các block
     * @return Application|Factory|View
     */
    public function updateAction(UpdateStoreRequest $request){
        $input = $request->all();
        try{
            $data = $this->storeRepo->update($input);

            session()->flash('success', "Cập nhật cửa han thành công");
            return view('admin.stores.edit', ['data' => $data]);
        }catch (\Exception $exception){
            session()->flash('danger', $exception->getMessage());
            return view('admin.stores.edit', ['data' => $input]);
        }
    }

    /**
     * Xóa chi nhánh
     * @return Application|Factory|View
     */
    public function destroyAction($id){
        try{
            $delete = $this->storeRepo->destroy($id);
            if($delete){
                return redirect(route("stores.index"))->with('success', 'Xóa thành công!');
            }else{
                return redirect(route("stores.index"));
            }
        }catch (\Exception $exception){
            return redirect(route("stores.index"))->with('danger', $exception->getMessage());
        }
    }

    /**
     * Trả về datatable của danh sách block
     * @return Application|Factory|View
     */
    public function getDatatableAction(Request $request){
        $input = $request->all();
        return $this->storeRepo->getDatatable($input);
    }

    /**
     * Chi tiết chi nhánh
     * @return Application|Factory|View
     */
    public function detail($id){
        $data = $this->storeRepo->getStoreDetail($id);

        return view('admin.stores.detail', ['data' => $data]);
    }

}
