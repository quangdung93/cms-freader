<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Blocks\AddAttributeRequest;
use App\Http\Requests\Blocks\CreateBlockRequest;
use App\Repositories\Blocks\BlockInterface;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class BlockController extends Controller
{
    protected $blockRepo;

    public function __construct(BlockInterface $blockRepo){
        $this->blockRepo = $blockRepo;
    }
    //

    /**
     * Màn hình danh sách các block
     * @return Application|Factory|View
     */
    public function indexAction(){
        return view('admin.blocks.index');
    }

    /**
     * Trả về datatable của danh sách block
     * @return Application|Factory|View
     */
    public function getDatatableAction(Request $request){
        $input = $request->all();
        return $this->blockRepo->getDatatable($input);
    }

    /**
     * Màn hình chi tieets  block
     * @return Application|Factory|View
     */
    public function detailAction($id){

        $block = $this->blockRepo->getBlockDetail($id);

        return view('admin.blocks.detail', ['block' => $block]);
    }

    /**
     * Màn hình chi tieets  block
     * @return Application|Factory|View
     */
    public function editAction($id){
        $block = $this->blockRepo->getBlockDetail($id);
        return view('admin.blocks.edit', ['block' => $block]);
    }

    /**
     * Màn hình chi tieets  block
     * @return Application|Factory|View
     */
    public function createAction(){
        return view('admin.blocks.create');
    }

    /**
     * Màn hình chi tieets  block
     */
    public function updateAction(Request $request){
        try{
            $data = $request->all();
            $this->blockRepo->updateBlock($data);
            return [
                'error' => 0,
                'message' => 'Cập nhật nội dung thành công'
            ];
        }catch (\Exception $exception){
            return [
                'error' => 1,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * Danh sách slider
     */
    public function getBlocks(Request $request){
        try{
            $input = $request->all();
            $data = $this->blockRepo->getBlocks($input);
            return [
                'error' => 0,
                'data' => $data,
                'message' => 'Lấy danh sách nội dung thành công'
            ];
        }catch (\Exception $exception){
            return [
                'error' => 1,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * Danh sách slider
     */
    public function addAttributeAction(AddAttributeRequest $request){
            $input = $request->all();
            if($request->hasFile('image')){
                $input['input_file'] = $request->file('image');
            }
            $data = $this->blockRepo->addBannerAttribute($input);

            return [
                'error' => 0,
                'data' => $data,
                'message' => 'Thêm thuộc tính thành công'
            ];
    }


    /**
     * Màn hình danh sách các block
     * @return Application|Factory|View
     */
    public function getBlockContent(Request $request){
        $type = $request->type;
        if($type == 'slider'){
            return view('admin.blocks.slider-attribute', ['isEdit' => 1]);
        }else{
            return view('admin.blocks.banner-attribute', ['isEdit' => 1]);
        }
    }

    /**
     * Màn hình chi tieets  block
     */
    public function storeAction(CreateBlockRequest $request){
        try{
            $data = $request->all();
            $this->blockRepo->storeBlock($data);
            return [
                'error' => 0,
                'message' => 'Thêm nội dung thành công'
            ];
        }catch (\Exception $exception){
            return [
                'error' => 1,
                'message' => $exception->getMessage()
            ];
        }
    }
}
