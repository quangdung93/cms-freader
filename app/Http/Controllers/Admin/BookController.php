<?php

namespace App\Http\Controllers\Admin;

use App\Models\BookAttributeDetail;
use App\Models\BookAttributes;
use App\Models\Brand;
use App\Models\Book;
use App\Models\Setting;
use App\Models\Category;
use App\Models\Supplier;
use App\Repositories\Books\BookInterface;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Services\BookService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
class BookController extends Controller
{
    protected $bookService;
    protected $book;

    public function __construct(BookService $bookService, BookInterface $book)
    {
        $this->bookService = $bookService;
        $this->book = $book;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.book.index');
    }

    public function getDatatable(){
        $books = Book::orderByDesc('created_at');
        return $this->bookService->getDatatable($books);
    }

    public function getLists(Request $request){
        $query = Book::query();
        if($request->name){
            $keyword = trim($request->name);
            $query->where('name', 'like', '%'.$keyword.'%');
            $query->orWhere('sku', 'like', '%'.$keyword.'%');
        }

        $books = $query->select('*');

        return $this->bookService->getDatatable($books);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $categories = Category::active()->get();
        // $brands = Brand::where('type', 0)->active()->get();
        // $origins = Brand::where('type', 1)->active()->get();
        // $suppliers = Supplier::active()->get();
        $attr= BookAttributes::all()->where("is_active", 1)->sortBy("priority");
        return view('admin.book.add-edit')->with([
            // 'categories' => $categories,
            // 'brands' => $brands,
            // 'origins' => $origins,
            // 'suppliers' => $suppliers,
            'book_attrs' => $attr
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->book->store($request);
    }

    public function fix(){
        $prefix = 'SP';
        $books = Book::all();
        $sku = '';
        $maxCode = 1;

        foreach ($books as $key => $book) {
            if($key == 0){
                $sku = $prefix.'0000001';
            }
            else{
                $maxCode = $maxCode + 1;

                if ($maxCode < 10)
                    $sku = $prefix.'000000' . ($maxCode);
                else if ($maxCode < 100)
                    $sku = $prefix.'00000' . ($maxCode);
                else if ($maxCode < 1000)
                    $sku = $prefix.'0000' . ($maxCode);
                else if ($maxCode < 10000)
                    $sku = $prefix.'000' . ($maxCode);
                else if ($maxCode < 100000)
                    $sku = $prefix.'00' . ($maxCode);
                else if ($maxCode < 1000000)
                    $sku = $prefix.'0' . ($maxCode);
                else if ($maxCode < 10000000)
                    $sku = $prefix.'' . ($maxCode);
            }

            $book->sku = $sku;
            $book->save();
        }

        echo 'Done';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::findOrFail($id);
        $book->storage_links()->get();
        $attrAll= BookAttributes::all()->where("is_active", 1)->sortBy("priority");
        $proAttr = $book->attributes()->get();
        $fileLink = $book->storage_links()->first();
        $mappingAttr = [];
        foreach ($proAttr as $attr) {
            # code...
            $mappingAttr[$attr->attribute()->first()->name] = $attr->value;
        }
        return view('admin.book.add-edit')->with([
            'book' => $book,
            'book_attrs' => $attrAll,
            'attr_values' => $mappingAttr,
            'file_link' => empty($fileLink) ? ''  : $fileLink->data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->book->update($request, $id);
    }
    public function approvePublisher(Request $request){
        return $this->book->approvePublisher($request->book_id);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::findOrFail($id);
        $delete = $book->delete();

        if($delete){
            return redirect('admin/book')->with('success', 'Xóa thành công!');
        }
    }

    public function search(\Illuminate\Http\Request $request)
    {
        $key = $request->input('key');
        if (!empty($key)) {
            $listExistedIds = [];

            $ids = $request->get('ids');
            if (!empty($ids)) {
                $listExistedIds = explode(',', $ids);
            }
            $ids_include = $request->get('ids_include');
            $listIdInclude = [];

            if (!empty($ids_include)) {
                $listIdInclude = explode(',', $ids_include);
            }
            $listIdInclude = [];
            $book = new Book();

            $result = $book->searchBookWithoutIds($key, $listExistedIds, 10, $listIdInclude);
            if ($result) {
                return $this->responseJson(CODE_SUCCESS, $result);
            }
        }

        return $this->responseJson(CODE_ERROR, null, 'Không tìm thấy sản phẩm');
    }

    public function searchPaging(Request $request)
    {
        try{
            $input = $request->all();

            $data = $this->book->getListPaging($input);

            return [
                'error' => 0,
                'data' => $data,
                'message' => 'Lấy sách thành công'
            ];
        }catch (\Exception $exception){
            return [
                'error' => 1,
                'message' => $exception->getMessage()
            ];
        }
    }

}
