<?php

namespace App\Http\Controllers\Admin;

use App\Models\AccessToken;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Role;
use App\Models\User;
use App\Services\CustomerService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Models\Store;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    protected $customerService;

    public function __construct(CustomerService $customerService){
        $this->customerService = $customerService;
    }
    //
    public function index(){
        return view('admin.customers.index');
    }
    public function getDatatable(){
        $customers = Customer::query()->where("is_deleted", 0);
        return $this->customerService->getDatatable($customers);
    }

    public function getLists(Request $request){
        $query = Customer::query();
        if($request->name){
            $keyword = trim($request->name);
            $query->where(function($queryFilter) use ($keyword){
                $queryFilter->where('name', 'like', '%'.$keyword.'%');
                $queryFilter->orWhere('first_name', 'like', '%'.$keyword.'%');
                $queryFilter->orWhere(\DB::raw('CONCAT(first_name," ",name)'), 'LIKE', '%'.$keyword.'%');
                $queryFilter->orWhere('phone_number', 'like', '%'.$keyword.'%');
                $queryFilter->orWhere('email', 'like', '%'.$keyword.'%');
            });
        }
        $customers = $query->where("is_deleted", 0)
                           ->select('*');
        return $this->customerService->getDatatable($customers);
    }

    public function getDeviceDatatable(Request $request, $id){
        Log::error($id);
        $query = AccessToken::query();
        $customerDevices = $query->select('*')
                                 ->where("user_id", $id)
                                 ->where("is_active", 1);
        return $this->customerService->getCustomerDeviceDatatable($customerDevices);
    }

    public function getCustomerDeviceList(Request $request){

        try{
            $input = $request->all();
            $customerId = $input['customer_id'];
            $query = AccessToken::query();
            $customerDevices = $query->select('*')
                ->where("user_id", $customerId)
                ->where("is_active", 1)
                ->get()
                ->toArray();
            return [
                "error" => 0,
                "message" => "",
                "data" => view("admin.customers.devices.index", [
                    'devices' => $customerDevices
                ])->render()
            ];
        }catch (\Exception $exception){
            return [
                "error" => 0,
                "message" => $exception->getMessage(),
                "data" => null
            ];
        }
    }

    public function destroyCustomerDevice($id){
        $device = $this->customerService->findCustomerDevice($id);
        if(!$device){
            return abort('404');
        }
        $now = Carbon::now();
        $delete = $this->customerService->updateCustomerDevice([
            "is_active" => 0,
            "updated_at" => $now,
        ], $id);

        if($delete){
            $path = route("customers.detail-view", ['id' => $device['user_id']]);
            return redirect($path)->with('success', 'Xóa thành công!');
        }
    }

    public function create(){
        $roles = Role::all();
        $stores = Store::active()->get();
        $managers = $this->userModel->getAllUser();

        return view('admin.users.add-edit', [
            'roles' => $roles,
            'managers' => $managers,
            'stores' => $stores,
        ]);
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'username' => 'required|min:3|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'stores' => 'required',
            'role' => 'required'
        ],[
            'name.required' => 'Bạn chưa nhập tên người dùng',
            'username.required' => 'Bạn chưa nhập username',
            'username.min' => 'Username phải nhiều hơn 3 ký tự',
            'username.unique' => 'Username đã tồn tại',
            'email.required' => 'Bạn chưa nhập email',
            'email.email' => 'Định dạng email không đúng',
            'email.unique' => 'Email đã tồn tại',
            'password.required' => 'Bạn chưa nhập mật khẩu',
            'stores.required' => 'Bạn chưa chọn chi nhánh',
            'role.required' => 'Bạn chưa chọn quyền'
        ]);

        //Avatar image
        if($request->hasFile('input_file')){
            $avatarPath = $this->uploadImage('users', $request->file('input_file'));
        }

        $requestStores = $request->stores;

        $data = [
            'name' => $request->name,
            'username' => Str::slug($request->username),
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'status' => isset($request->status) ? 1 : 0,
            'phone' => $request->phone,
            'address' => $request->address,
            'gender' => (int)$request->gender,
            'stores' => $requestStores ? implode(',', $requestStores) : null,
            'birthday' => format_date(str_replace('/', '-', $request->birthday), 'Y-m-d'),
            'avatar' => $avatarPath ?? '/admin/images/user-default.png'
        ];

        //Set store mặc định
        if($requestStores){
            $data['store_active'] = $requestStores[0];
        }

        $user = User::create($data);

        //Assign Role
        if($user && $request->role){
            $user->assignRole($request->role);

            return redirect('admin/users');
        }
        else{
            return redirect('admin/users')->with('danger', 'Tạo thất bại!');
        }
    }

    public function detail($id){
        $customer = $this->customerService->findCustomer($id);

        return view('admin.customers.detail')->with([
            'roles' => null,
            'managers' => null,
            'stores' => null,
            'customer' => $customer
        ]);
    }
    public function getDetail($id){
        $customer = $this->customerService->findCustomerInfo($id);

        return response()->json($customer);
    }

    public function edit($id){
        $customer = $this->customerService->findCustomer($id);
//
//        if(!$user){
//            return abort(404);
//        }
//
//        if(!isAdmin() && Auth::id() != $id){
//            return abort(403);
//        }
//
//        $roles = Role::all();
//        $stores = Store::active()->get();
//        $managers = $this->userModel->getAllUser();

        return view('admin.customers.add-edit')->with([
            'roles' => null,
            'managers' => null,
            'stores' => null,
            'customer' => $customer
        ]);
    }

    public function update(Request $request, $id){
        $customer = $this->customerService->findCustomer($id);
        if(!$customer){
            return abort('404');
        }
        $request->validate([
            'name' => 'required',
            'phone' => 'required|unique:users,phone_number,'.$customer->id,
            'email' => 'regex:/^([a-z0-9+-]+)(.[a-z0-9+-]+)*@([a-z0-9-]+.)+[a-z]{2,6}$/ix|unique:users,email,'.$customer->id,
            'birthday' => 'nullable',
            'gender' => 'nullable',
            'is_active' => 'nullable',
        ],[
            'name.required' => 'Bạn chưa nhập tên khách hàng',
            'username.unique' => 'Số điền thoại đã tồn tại',
            'email.regex' => 'Định dạng email không đúng',
            'email.unique' => 'Email đã tồn tại'
        ]);
        $input = $request->all();
        $data = [
            'first_name' => $input['first_name'] ?? null,
            'name' => $input['name'],
            'phone_number' => $input['phone'],
            'email' => $input['email'],
            'is_active' => isset($input['is_active']) ? $input['is_active'] == 'on' ? 1 : 0 : 0,
            'gender' => $input['gender'],
            'dob' => format_date(str_replace('/', '-', $input['birthday']), 'Y-m-d')
        ];

        $this->customerService->updateCustomer($data, $id);

        $path = route("customers.edit", ['customer' => $id]);
        return redirect($path)->with('success','Cập nhật thành công!');
    }
    public function search(\Illuminate\Http\Request $request)
    {
        $key = $request->input('key');
        if (!empty($key)) {
            $listExistedIds = [];

            $ids = $request->get('ids');
            if (!empty($ids)) {
                $listExistedIds = explode(',', $ids);
            }

            $customer = new Customer();

            $result = $customer->searchCustomerWithoutIds($key, $listExistedIds, 10);
            if ($result) {
                return $this->responseJson(CODE_SUCCESS, $result);
            }
        }

        return $this->responseJson(CODE_ERROR, null, 'Không tìm thấy sản phẩm');
    }

    public function destroy($id){
        $customer = $this->customerService->findCustomer($id);

        if(!$customer){
            return abort('404');
        }
        $now = Carbon::now();
        $delete = $this->customerService->updateCustomer([
            "is_deleted" => 1,
//            "deleted_at" => $now,
            'updated_at' => $now
        ], $id);

        if($delete){
            return redirect('admin/customers')->with('success', 'Xóa thành công!');
        }
    }
}
