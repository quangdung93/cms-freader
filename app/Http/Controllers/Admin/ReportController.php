<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ChartService;

class ReportController extends Controller
{
    protected $chartService;

    public function __construct(ChartService $chartService)
    {
        $this->chartService = $chartService;
    }

    public function chartOrderSourceAction(Request $request){
        try {
            $data = $this->chartService->getOrderSource($request->all());
            return $this->responseJson(CODE_SUCCESS, $data);
        } catch (\Throwable $th) {
            return $this->responseJson(CODE_ERROR, null, $th->getMessage());
        }
    }

    public function chartOrderRevenueAction(Request $request){
        try {
            $data = $this->chartService->getOrderRevenue($request->all());
            return $this->responseJson(CODE_SUCCESS, $data);
        } catch (\Throwable $th) {
            return $this->responseJson(CODE_ERROR, null, $th->getMessage());
        }
    }
    
}
