<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BookOwner;
use App\Models\Customer;
use App\Models\Order;
use App\Models\Store;
use App\Models\User;
use App\Services\CustomerService;
use App\Services\OrderService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use N2W;
use PDF;

class OrderController extends Controller
{
    protected $orderService;
    protected $userService;
    protected $customerService;

    public function __construct(
        OrderService $orderService,
        UserService $userService,
        CustomerService $customerService
    ) {
        $this->orderService = $orderService;
        $this->userService = $userService;
        $this->customerService = $customerService;
    }

    public function index()
    {
        return view('admin.orders.index');
    }

    public function getDatatable()
    {
        $query = Order::query();
        $orders = $query->orderBy('created_at', 'DESC')->select('*');
        return $this->orderService->renderDatatable($orders);
    }

    public function getLists(Request $request)
    {
        $query = Order::query();
        $exportOrderId = '';

        if ($request->code) {
            $keyword = trim($request->code);
            $query->where('code', 'like', '%'.$keyword.'%');
        }

        if ($request->status != '') {
            $query->where('status', $request->status);
        }

        // if((int)$request->payment > 0){
        //     $query->where('payment_method', (int)$request->payment);
        // }

        $orders = $query->with('saler', 'customer')->orderBy('id', 'DESC')->select('*');

        return $this->orderService->renderDatatable($orders, $exportOrderId);
    }

    public function detail($id)
    {
        $order = Order::findOrFail($id)->load('detail');
        return view('admin.orders.add')->with([
            'order' => $order,
        ]);
    }
    public function edit($id)
    {
        $staff = User::all();

        $order = Order::findOrFail($id)->load('detail');
        return view('admin.orders.add')->with([
            'order' => $order,
            'staff' => $staff,
        ]);
    }

    public function create(Request $request)
    {
        $customers = Customer::all();
        if ($request->customer_id) {
            $customer = Customer::where('id', $request->customer_id)->first();
        }
        $staff = User::all();

        return view('admin.orders.add')->with([
            'customers' => $customers,
            'customer' => $customer ?? null,
            'staff' => $staff,
        ]);
    }
    public function checkTask($orderDetailId)
    {
        $bo = BookOwner::where("order_detail_id", $orderDetailId)->first();
        if ($bo) {
            if ($bo->status == 'active') {
                return ["status" => "finish"];
            } elseif ($bo->status == 'new') {
                if (is_null($bo->task_id)) {
                    // not enqueued
                    return ["status" => "new"];
                } else {
                    // enqueued but still not finished yet
                    return ["status" => "processing", "task_id" => $bo->task_id, "url" => env("ASYNQ_SERVER") . "monitoring/queues/default/tasks/" . $bo->task_id];
                }
            }
        }
    }
    public function enqueueTask(Request $request)
    {
        $request->validate([
            'order_detail_id' => 'required',
        ], [
            'order_detail_id.required' => 'Lỗi, thiếu mã chi tiết đơn hàng',
        ]);
        return $this->orderService->enqueueTask($request->order_detail_id);
    }

    public function update(Request $request)
    {
        $request->validate([
            'products' => 'present|array',
            'order_id' => 'required',
        ], [
            'products.present' => '(*) Bạn chưa chọn sản phẩm',
            'order_id.required' => '',
        ]);
        // check if user own books
        $data = ($request->except(['_token', 'submit']));
        $order = Order::findOrFail($data['order_id'])->load('orderDetail');
        $listBookIds = (array_map(function ($item) {
            return $item['id'];
        }, $data['products']));
        $countSame = 0;
        // check book_owner any active
        foreach ($order->toArray()['order_detail'] as $orderDetail) {
            if ($orderDetail["dis_status"] != 'new' && !is_null($orderDetail["dis_status"])) {
                // return error -> all must be not error
                $errLogs = [];
                $errLogs["book_id." . $orderDetail['book_id']] = 'Sách đã được phát hành';
                return back()->withErrors(
                    $errLogs
                )->withInput();
            }
            foreach ($listBookIds as $bookIndex => $bookId) {
                if ($bookId == $orderDetail['book_id']) {
                    $listBookIds[$bookIndex] = '';
                    $countSame++;
                }
            }
        }
        // check if purchase new book
        $listBookIds = array_filter($listBookIds);
        // then process update
        if (count($listBookIds) > 0) {
            $bookOwners = $this->orderService->getBookOwner($order->user_id, $listBookIds);
            if (count($bookOwners) > 0) {
                // return errors
                $errLogs = [];
                foreach ($bookOwners as $bookOwner) {
                    $errLogs["book_id." . $bookOwner->book_id] = 'Người dùng đã sở hữu sách này';
                }
                return back()->withErrors(
                    $errLogs
                )->withInput();
            }
        }
        // create order -> order detail
        if ($order->status == 'new') {
            $orderStatus = $this->orderService->updateOrder($order->id, $data['products'], $order->user_id, (int) $data['saler_id']);
            if ($orderStatus) {
                return back()->withInput()->with('success', 'Cập nhật thành công!');
            } else {
                return back()->withInput()->with('danger', 'Cập nhật thất bại!');
            }
        } else {
            return back()->withInput()->withErrors('danger', 'Đơn hàng đã bàn giao');
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'products' => 'present|array',
            'customer_id' => 'required',
        ], [
            'products.present' => '(*) Bạn chưa chọn sản phẩm',
            'customer_id.required' => '(*) Bạn chưa chọn Khách hàng',
        ]);
        // check if user own books
        $data = ($request->except(['_token']));
        $listBookIds = (array_map(function ($item) {
            return $item['id'];
        }, $data['products']));
        $bookOwners = $this->orderService->getBookOwner($data['customer_id'], $listBookIds);
        if (count($bookOwners) > 0) {
            // return errors
            $errLogs = [];
            foreach ($bookOwners as $bookOwner) {
                $errLogs["book_id." . $bookOwner->book_id] = 'Người dùng đã sở hữu sách này';
            }
            return back()->withErrors(
                $errLogs
            )->withInput();
        }
        // create order -> order detail
        $orderStatus = $this->orderService->createOrder($data['products'], $data['customer_id'], (int) $data['saler_id']);
        if ($orderStatus) {
            DB::commit();

            return redirect('admin/orders')->with('success', 'Tạo thành công!');
        } else {
            DB::rollback();
            return redirect('admin/orders/create')->with('danger', 'Tạo thất bại!');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        $delete = $order->delete();

        if ($delete) {
            return redirect('admin/orders')->with('success', 'Xóa thành công!');
        }
    }

    public function detailOrder(Request $request, $id)
    {
        try {
            $order = Order::where('id', $id)->with('detail')->first();
            $result = $this->orderService->renderOrderDetail($order, $request->type);
            return $this->responseJson(CODE_SUCCESS, $result);
        } catch (\Throwable $th) {
            return $this->responseJson(CODE_ERROR, null, $th->getMessage());
        }
    }

    public function getOrdersByCustomer($customerId)
    {
        try {
            $orders = Order::where('customer_id', $customerId)->get();

            return $this->responseJson(CODE_SUCCESS, $orders);
        } catch (\Throwable $th) {
            return $this->responseJson(CODE_ERROR, null, $th->getMessage());
        }
    }

    public function printOrder($orderId)
    {
        $order = Order::where('id', $orderId)->with('customer', 'detail')->first();

        if (!$order) {
            return abort(404);
        }

        $moneyToVietnamese = N2W::toCurrency($order->total_money);

        $store = Store::where('id', $order->store_id)->first();

        $pdf = PDF::loadView('admin.orders.print', [
            'order' => $order,
            'store' => $store,
            'moneyToVietnamese' => $moneyToVietnamese,
        ]);

        $pdf->setOptions(['isRemoteEnabled' => true]);

        return $pdf->stream();
    }
}
