<?php

namespace App\View\Components;

use Illuminate\View\Component;

class UploadAttachment extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $type;
    public $title;
    public $accept;
    public $name;
    public $value;
    public $errorname;

    public function __construct($type, $title, $name, $accept, $value = null, $errorname = null)
    {
        $this->type = $type;
        $this->title = $title;
        $this->name = $name;
        $this->accept = $accept;
        $this->value = $value;
        $this->errorname = $errorname ?? $name;
    }
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.upload-attachment');
    }
}
