<?php

namespace App\Api;

use App\Api\ApiAbstract;

class GHTKApi extends ApiAbstract
{

    /**
     * Lấy danh sách thông tin sản phẩm
     *
     * @return void
     */
    public function getProducts($data){
        return $this->callApi('POST', 'services/kho-hang/thong-tin-san-pham', $data);
    }

    /**
     * Đăng đơn hàng
     *
     * @return void
     */
    public function sendOrder($data){
        return $this->callApi('POST', 'services/shipment/order?ver=1.6.3', $data);
    }
    
    /**
     * Trạng thái đơn hàng
     *
     * @return void
     */
    public function getOrderStatus($orderCode){
        return $this->callApi('GET', 'services/shipment/v2/'.$orderCode);
    }
    

    /**
     * Hủy đơn hàng
     *
     * @return void
     */
    public function cancelOrder($orderCode){
        return $this->callApi('POST', 'services/shipment/cancel/'.$orderCode);
    }

    /**
     * Lấy danh sách địa chỉ lấy hàng
     *
     * @return void
     */
    public function getListPicking(){
        return $this->callApi('GET', 'services/shipment/list_pick_add');
    }

    /**
     * Tính phí vận chuyển
     *
     * @return void
     */
    public function calShippingFee(){
        return $this->callApi('GET', 'services/shipment/fee');
    }
}
