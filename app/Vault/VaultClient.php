<?php

namespace App\Vault;

use GuzzleHttp\Client;

class VaultClient
{

    protected $client;
    protected $host;
    protected $auth;

    public function __construct(Client $client, string $host = "http://0.0.0.0:8200", $auth = "")
    {
        $this->client = $client;
        $this->host = $host;
        $this->auth = $auth;
    }
    public function getKey($secretPath, $id)
    {
        try {

            $res = $this->client->get($this->host . '/v1/' . $secretPath . "/data/" . $id, [
                "headers" => [
                    "X-Vault-Token" => $this->auth,
                ],
            ]);
            if ($res->getStatusCode() == 200) {
                $data = json_decode($res->getBody()->getContents(), 1);
                return $data['data']['data'];
            } else {
                return [];
            }
        } catch (\Exception $e) {
            // dd($e);
            return [];

        }
    }
}
