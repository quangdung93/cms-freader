<?php

namespace App\Providers;

use App\Rclone\Rclone;
use App\Rclone\RcloneClient;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class RcloneServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    //
    $this->app->bind(RcloneClient::class, function($app){
        return new RcloneClient(new Client(), env("RCLONE_HOST"), [env("RCLONE_USERNAME"),env("RCLONE_PASSWORD")]);
    });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
