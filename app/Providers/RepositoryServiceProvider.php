<?php

namespace App\Providers;
use App\Repositories\Blocks\BlockInterface;
use App\Repositories\Blocks\BlockRepository;
use App\Repositories\Books\BookRepository;
use App\Repositories\Books\BookInterface;
use App\Repositories\Products\ProductInterface;
use App\Repositories\Products\ProductRepository;
use App\Repositories\Stores\StoreInterface;
use App\Repositories\Stores\StoreRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(BlockInterface::class, BlockRepository::class);
        $this->app->singleton(StoreInterface::class, StoreRepository::class);
        $this->app->singleton(ProductInterface::class, ProductRepository::class);
        $this->app->singleton(BookInterface::class, BookRepository::class);
    }

}
