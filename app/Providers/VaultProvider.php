<?php

namespace App\Providers;

use App\Vault\VaultClient;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class VaultProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //

        $this->app->bind(VaultClient::class, function($app){
            return new VaultClient(new Client(), env("VAULT_HOST"), env("VAULT_TOKEN"));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
