<?php

namespace App\Providers;

use App\Asynq\Client;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Redis;

class AsynqServiceProvider extends ServiceProvider
{
    protected $defer = true;
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Client::class, function($app){
            $redis = Redis::connection("asynq");
            $client = new Client($redis->client());
            return $client;
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
