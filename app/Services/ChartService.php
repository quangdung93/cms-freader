<?php 

namespace App\Services;

use Carbon\Carbon;
use App\Models\Order;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ChartService
{
    protected $orderModel;

    public function __construct(Order $orderModel)
    {
        $this->orderModel = $orderModel;
    }

    public function getOrderSource($params){
        $data = $this->orderModel->getOrderSource($params);

        $dataChart = [];
        $newData = [];
        if($data){
            foreach ($data as $key => $item) {
                $source = renderOrderSource($key);
                $newData[$source] = (double)$item;
            }

            $dataChart = [
                'label' => array_keys($newData),
                'data' => array_values($newData),
            ];
        }

        return $dataChart;
    }

    public function getOrderRevenue($params){
        $period = getRangeDay(format_date($params['date_from'], 'Y-m-d'), format_date($params['date_to'], 'Y-m-d'));
        $countPeriod = $period->count();

        $days = [];
        foreach ($period as $date) {
            if($countPeriod <= 31){
                $days[] = (int)$date->format('d');
            }
            elseif($countPeriod > 31 && $countPeriod <= 365 * 2){
                $days[] = (int)$date->format('m') .'/'. (int)$date->format('Y');
            }
            elseif($countPeriod > 365 * 2){
                $days[] = (int)$date->format('Y');
            }
        }

        $type = 'day';
        if($countPeriod > 31 && $countPeriod <= 365 * 2){
            $days = array_values(array_unique($days));
            $type = 'month';
        }
        elseif($countPeriod > 365 * 2){
            $days = array_values(array_unique($days));
            $type = 'year';
        }

        $result = $this->orderModel->getOrderRevenue($params, $type);

        $data = [];
        foreach ($result as $item) {
            if($type == 'month'){
                $keyMonth = $item['date'] .'/'. $item['year'];
                $data[$keyMonth] = $item['total_money'];
            }else{
                $data[$item['date']] = $item['total_money'];
            }
        }

        $dataChart = [];
        if($data){
            $dataChart = [
                'days' => $days,
                'data' => $data,
                'type' => $type
            ];
        }

        return $dataChart;
    }
}