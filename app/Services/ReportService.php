<?php 

namespace App\Services;

use Auth;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Customer;
use App\Models\OrderDetail;
use Yajra\DataTables\DataTables;
use App\Services\CustomerService;
use Illuminate\Support\Facades\DB;
use App\Models\ProductExportDetail;
use App\Models\ProductImportDetail;

class ReportService
{
    const REVENUE_FINISH = 1; //Doanh số hoàn thành
    const REVENUE_SHIPPING = 2; //Doanh số đang giao
    const REVENUE_CUSTOMER_GROUP = 3; //Doanh số đại lý
    const REVENUE_CUSTOMER_NEW = 4; //Doanh số KH mới

    public function buildQueryRevenue($request){
        $query = Order::query();

        $userIds = (new UserService)->getUserIdsByPermission();
        if($userIds){
            $query->whereIn('saler_id', $userIds);
        }

        if($request->saler_id){
            $query->where('saler_id', $request->saler_id);
        }

        //Kiểm tra quyền Đại lý/Khách lẻ
        $query = (new CustomerService)->buildQueryCheckRole($query);

        if($request->date_from){
            $query->where('sell_date', '>=', format_date($request->date_from, 'Y-m-d'));
        }
        else{
            $query->where('sell_date', '>=', format_date(Carbon::now(), 'Y-m-d'));
        }

        if($request->date_to){
            $query->where('sell_date', '<=', format_date($request->date_to, 'Y-m-d'));
        }
        else{
            $query->where('sell_date', '<=', format_date(Carbon::now(), 'Y-m-d'));
        }

        //Loại đơn hàng (đã xác nhận/chưa xác nhận)
        if($request->revenue_type == self::REVENUE_SHIPPING){
            $query->where('admin_status', 0);
            $query->whereNotIn('status', [Order::FAILED, Order::CANCEL]);
        }
        else{
            $query->where('admin_status', 1);
            $query->where('status', Order::FINISH);
        }

        //Loại KH
        if($request->customer_group == Customer::GROUP_SINGLE){
            $query->whereHas('customer', function($q){
                $q->where('customer_group', Customer::GROUP_SINGLE);
            });
        }
        else{
            $agencyId = $request->agency_id;
            $query->whereHas('customer', function($q) use($agencyId){
                $q->where('customer_group', Customer::GROUP_AGENCY);

                if($agencyId){
                    $q->where('customers.id', $agencyId);
                }
            });
        }
        
        return $query->with('user', 'customer')
                        ->orderBy('id', 'DESC')
                        ->select('*');
    }

    public function buildQueryRevenueProduct($request){
        $query = OrderDetail::leftJoin('products', 'products.id', '=', 'order_detail.product_id')
        ->leftJoin('orders', 'orders.id', '=', 'order_detail.order_id')
        ->leftJoin('brands', 'brands.id', '=', 'products.brand_id');

        if($request->product_id){
            $query->where('products.id', $request->product_id);
        }

        if($request->brand_id){
            $query->where('products.brand_id', (int)$request->brand_id);
        }

        if($request->date_from){
            $query->where('orders.sell_date', '>=', format_date($request->date_from, 'Y-m-d'));
        }
        else{
            $query->where('orders.sell_date', '>=', format_date(Carbon::now(), 'Y-m-d'));
        }

        if($request->date_to){
            $query->where('orders.sell_date', '<=', format_date($request->date_to, 'Y-m-d'));
        }
        else{
            $query->where('orders.sell_date', '<=', format_date(Carbon::now(), 'Y-m-d'));
        }

        $query->where('orders.status', Order::FINISH);
        
        return $query->select([
            'products.id as product_id',
            'products.code as product_code',
            'products.name as product_name',
            'brands.name as brand_name',
            DB::raw('SUM(orders.total_quantity) as total_quantity'),
            DB::raw('SUM(orders.total_money) as total_money'),
        ])->groupBy(['products.id', 'product_id', 'product_code', 'product_name', 'brand_name']);
    }

    public function buildQueryImport($request){
        $query = ProductImportDetail::query();

        $query->leftJoin("product_imports as import", "import.id", "=", "product_import_detail.import_id");
        $query->join("products", "products.id", "=", "product_import_detail.product_id");
        $query->join("users", "users.id", "=", "import.created_by");
        $query->join("suppliers", "suppliers.id", "=", "import.supplier_id");
        $query->join("warehouses", "warehouses.id", "=", "import.warehouse_id");

        $userIds = (new UserService)->getUserIdsByPermission();
        if($userIds){
            $query->whereIn('import.created_by', $userIds);
        }

        if($request->saler_id){
            $query->where('import.created_by', $request->saler_id);
        }

        if($request->supplier_id){
            $query->where('import.supplier_id', $request->supplier_id);
        }

        if($request->date_from){
            $query->where('import.import_date', '>=', format_date($request->date_from, 'Y-m-d'));
        }
        else{
            $query->where('import.import_date', '>=', format_date(Carbon::now()->startOfMonth(), 'Y-m-d'));
        }

        if($request->date_to){
            $query->where('import.import_date', '<=', format_date($request->date_to, 'Y-m-d'));
        }
        else{
            $query->where('import.import_date', '<=', format_date(Carbon::now()->endOfMonth(), 'Y-m-d'));
        }
        
        $import = $query->orderBy('import.id', 'DESC')
                        ->select(
                            'import.id as id',
                            'import.code as code',
                            'import.import_date as import_date',
                            'import.total as total',
                            'product_import_detail.qty as qty',
                            'product_import_detail.subtotal as price',
                            'warehouses.name as warehouse_name',
                            'suppliers.name as supplier_name',
                            'products.name as product_name',
                            'users.name as created_by',
                        );

        return $import;
    }

    public function buildQueryExport($request){
        $query = ProductExportDetail::query();

        $query->leftJoin("product_exports as export", "export.id", "=", "product_export_detail.export_id");
        $query->leftJoin("order_detail", "order_detail.id", "=", "product_export_detail.order_detail_id");
        $query->join("orders", "orders.id", "=", "export.order_id");
        $query->join("products", "products.id", "=", "product_export_detail.product_id");
        $query->join("users", "users.id", "=", "export.created_by");
        $query->join("customers", "customers.id", "=", "orders.customer_id");
        $query->join("warehouses", "warehouses.id", "=", "product_export_detail.warehouse_id");

        $userIds = (new UserService)->getUserIdsByPermission();
        if($userIds){
            $query->whereIn('export.created_by', $userIds);
        }

        if($request->saler_id){
            $query->where('export.created_by', $request->saler_id);
        }

        if(!isRoleAgency()){ //Nếu ko có quyền QL đại lý
            $query->where('customers.customer_group', '!=', Customer::GROUP_AGENCY);
        }

        if(!isRoleCustomer()){ //Nếu ko có quyền QL KH lẻ
            $query->where('customers.customer_group', '!=', Customer::GROUP_SINGLE);
        }

        if(!isRoleCustomerSale()){ //Nếu ko có quyển QL KH là nhân viên sale
            $query->where('customers.customer_group', '!=', Customer::GROUP_SALE);
        }

        if($request->customer_id && $request->agency_id){
            $query->where('orders.customer_id', $request->customer_id);
            $query->orWhere('orders.customer_id', $request->agency_id);
        }
        else{
            if($request->customer_id){
                $query->where('orders.customer_id', $request->customer_id);
            }
    
            if($request->agency_id){
                $query->where('orders.customer_id', $request->agency_id);
            }
        }

        if($request->date_from){
            $query->where('export.export_date', '>=', format_date($request->date_from, 'Y-m-d'));
        }
        else{
            $query->where('export.export_date', '>=', format_date(Carbon::now()->startOfMonth(), 'Y-m-d'));
        }

        if($request->date_to){
            $query->where('export.export_date', '<=', format_date($request->date_to, 'Y-m-d'));
        }
        else{
            $query->where('export.export_date', '<=', format_date(Carbon::now()->endOfMonth(), 'Y-m-d'));
        }
        
        $import = $query->orderBy('export.id', 'DESC')
                        ->select(
                            'export.id as id',
                            'export.code as code',
                            'export.export_date as export_date',
                            'orders.code as order_code',
                            'order_detail.qty as qty',
                            'order_detail.subtotal as price',
                            'warehouses.name as warehouse_name',
                            'customers.name as customer_name',
                            'customers.customer_group as customer_group',
                            'products.name as product_name',
                            'users.name as created_by',
                        );

        return $import;
    }

    public function renderDatatableRevenueProduct($table){
        $data = Datatables::of($table)
            ->editColumn('product_code', function ($row) {
                return '<a href="/admin/products/edit/'.$row->product_id.'" class="text-info btn-detail-order">'.$row->product_code.'</a>';
            })
            ->editColumn('product_name', function ($row) {
                return '<a href="/admin/products/edit/'.$row->product_id.'" class="text-info btn-detail-order">'.$row->product_name.'</a>';
            })
            ->editColumn('brand_name', function ($row) {
                return  $row->brand_name;
            })
            ->editColumn('quantity', function ($row) {
                return  $row->total_quantity;
            })
            ->editColumn('total_money', function ($row) {
                return  number_format($row->total_money);
            })
            ->rawColumns(['product_code', 'product_name'])
            ->make(true);
        return $data;
    }
}