<?php 

namespace App\Services;
use Carbon\Carbon;
use App\Models\Book;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class BookService
{
    public function getBookSku(){
        $prefix = 'SP';
        $object = DB::table('book')->orderBy('id', 'DESC')->first();

        if(!$object){
            return $prefix.'0000001';
        }

        $objectCode = $object->code;
        $maxCode = (int)(str_replace($prefix, '', $objectCode)) + 1;

        if ($maxCode < 10)
            return $prefix.'000000' . ($maxCode);
        else if ($maxCode < 100)
            return $prefix.'00000' . ($maxCode);
        else if ($maxCode < 1000)
            return $prefix.'0000' . ($maxCode);
        else if ($maxCode < 10000)
            return $prefix.'000' . ($maxCode);
        else if ($maxCode < 100000)
            return $prefix.'00' . ($maxCode);
        else if ($maxCode < 1000000)
            return $prefix.'0' . ($maxCode);
        else if ($maxCode < 10000000)
            return $prefix.'' . ($maxCode);
    }
    
    public function getDatatable($table){
        $data = Datatables::of($table)
            // ->editColumn('image', function ($row) {
            //     return '<img src="'.asset($row->image).'" style="width:60px;">';
            // })
            ->editColumn('sku', function ($row) {
                return '<div class="text-center"></div><div class="mt-2 text-center">'.$row->sku.'</div><div class="mt-2 text-center"></div>';
            })
            ->editColumn('name', function ($row) {
                $html = '<a class="text-success" href="'.url('/admin/book/edit/'.$row->id).'">'.$row->name.'</a>';
                return $html;
            })
            ->editColumn('price', function ($row) {
                return format_price($row->price) . ' đ';
            })
            ->editColumn('status', function ($row) {
                $status = "";
                switch($row->status){
                    case "new":
                        $status = '<label class="label label-info">Khởi tạo</label>';
                     break;
                    case "reviewing":
                        $status = '<label class="label label-warning">Chờ xét duyệt</label>';
                     break;
                    case "approved":
                        $status = '<label class="label label-primary">Đã duyệt</label>';
                        break;
                    case "published":
                        $status = '<label class="label label-success">Đã phát hành</label>';
                        break;
                    case "removed":
                        $status = '<label class="label label-danger">Đã xoá</label>';
                        break;
                    case "paused":
                        $status = '<label class="label label-danger">Đã dừng phát hành</label>';
                        break;
                                             
                }
                return $status;
            })
            ->addColumn('action', function ($row) {
                $user = Auth::user();
                $action = "";
                if($user->can('edit_books')){
                    $action .= '<a class="btn btn-primary" href="'.url('admin/book/edit/'.$row->id).'" title="Chỉnh sửa">
                                <i class="feather icon-edit-1"></i></a>';
                }
                if($user->can('delete_books')){
                    $action .= '<a href="'.url('admin/book/delete/'.$row->id).'" class="btn btn-danger notify-confirm" title="Xóa">
                        <i class="feather icon-trash-2"></i>
                    </a>';
                }

                return $action;
            })
            ->rawColumns(['sku', 'name', 'price', 'status','action'])
            ->make(true);
        return $data;
    }

    public function getBookRelated($category_ids, $limit = 8){
        return Book::whereHas('categories', function($query) use($category_ids){
            $query->whereIn('category_id', $category_ids);
        })
        ->limit($limit)
        ->get();
    }
}