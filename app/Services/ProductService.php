<?php 

namespace App\Services;
use Carbon\Carbon;
use App\Models\Product;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ProductService
{
    public function getProductSku(){
        $prefix = 'SP';
        $object = DB::table('products')->orderBy('id', 'DESC')->first();

        if(!$object){
            return $prefix.'0000001';
        }

        $objectCode = $object->code;
        $maxCode = (int)(str_replace($prefix, '', $objectCode)) + 1;

        if ($maxCode < 10)
            return $prefix.'000000' . ($maxCode);
        else if ($maxCode < 100)
            return $prefix.'00000' . ($maxCode);
        else if ($maxCode < 1000)
            return $prefix.'0000' . ($maxCode);
        else if ($maxCode < 10000)
            return $prefix.'000' . ($maxCode);
        else if ($maxCode < 100000)
            return $prefix.'00' . ($maxCode);
        else if ($maxCode < 1000000)
            return $prefix.'0' . ($maxCode);
        else if ($maxCode < 10000000)
            return $prefix.'' . ($maxCode);
    }
    
    public function getDatatable($table){
        $data = Datatables::of($table)
            // ->editColumn('image', function ($row) {
            //     return '<img src="'.asset($row->image).'" style="width:60px;">';
            // })
            ->editColumn('code', function ($row) {
                return '<div class="text-center"><img width="50" src="'.asset_image('product_code/'.$row->image_code).'" alt="" /></div><div class="mt-2 text-center">'.$row->code.'</div><div class="mt-2 text-center"><a href="'.url('admin/imports/create?product_id='.$row->id).'" class="btn btn-primary"><i class="feather icon-plus"></i> Nhập hàng</a></div>';
            })
            ->editColumn('name', function ($row) {
                $html = '<a class="text-success" href="'.url('/admin/products/edit/'.$row->id).'">'.$row->name.'</a>';
                $html .= '<div class="text-left">Thương hiệu: '.optional($row->brand)->name.'</div>';
                return $html;
            })
            ->editColumn('price', function ($row) {
                return format_price($row->price) . ' đ';
            })
            ->editColumn('status', function ($row) {
                $status =  $row->status === 1 ? '<label class="label label-success">Hiển thị</label>' : '<label class="label label-danger">Ẩn</label>';
                if( !empty($row->deleted_at) ){
                    $status = '<label class="label label-danger">Đã xóa</label><br> <p class="white-space"> Ngày xóa: '.date('d-m-Y',strtotime($row->deleted_at)).' </p>';
                }
                return $status;
            })
            ->addColumn('action', function ($row) {
                $user = Auth::user();
                $action = "";
                if($user->can('edit_products')){
                    $action .= '<a class="btn btn-primary" href="'.url('admin/products/edit/'.$row->id).'" title="Chỉnh sửa">
                                <i class="feather icon-edit-1"></i></a>';
                }
                if($user->can('delete_products')){
                    $action .= '<a href="'.url('admin/products/delete/'.$row->id).'" class="btn btn-danger notify-confirm" title="Xóa">
                        <i class="feather icon-trash-2"></i>
                    </a>';
                }

                return $action;
            })
            ->rawColumns(['code', 'name', 'price', 'status','action'])
            ->make(true);
        return $data;
    }

    public function getProductRelated($category_ids, $limit = 8){
        return Product::whereHas('categories', function($query) use($category_ids){
            $query->whereIn('category_id', $category_ids);
        })
        ->limit($limit)
        ->get();
    }
}