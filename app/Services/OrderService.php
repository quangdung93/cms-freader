<?php

namespace App\Services;

use App\Asynq\Client;
use App\Models\BookOwner;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Setting;
use App\Models\StorageObject;
use App\Vault\VaultClient;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class OrderService
{
    protected $order;
    protected $bookOwner;
    protected $vaultClient;
    protected $asynqClient;

    function __construct(Order $order, BookOwner $bookOwner, VaultClient $vaultClient, Client $asynqClient)
    {
        $this->order = $order;
        $this->bookOwner = $bookOwner;
        $this->vaultClient = $vaultClient;
        $this->asynqClient = $asynqClient;
    }
    public function enqueueTask($orderDetailId){
        $bo = BookOwner::where("order_detail_id", $orderDetailId)->first();
        if ($bo) {
            $enqueueData = [
                // "from_path" => "",// book url
                // "key" => "",// book key in vault
                // "pkey" => "",// public key of user in vault
                "upload_path" => "", // in setttings -> book_distribute_path
                "book_owner_id" => $bo->id, // book_owner_id
            ];
            // get from_path
            $storageBook = StorageObject::where("object", "book")->where("object_id", $bo->book_id)->first();
            if($storageBook){
                $enqueueData['from_path'] = $storageBook->data;
            }else{
                return [
                    "error" => "Không thể tìm thấy nơi sách lưu trữ"
                ];
            }
            // get book key from vault
            $vaultBook = $this->vaultClient->getKey(env("VAULT_BOOK_SECRET", "books"), $bo->book_id);
            if(isset($vaultBook['key'])){
                $enqueueData['key'] = $vaultBook['key'];
            }else{
                return [
                    "error" => "Không thể tìm key giải mã sách"
                ];
            }
            $vaultUser = $this->vaultClient->getKey(env("VAUTL_USER_SECRET", "users"), $bo->user_id);
            if(isset($vaultUser['public_key'])){
                $enqueueData['pkey'] = $vaultUser['public_key'];
            }else{
                return [
                    "error" => "Không thể tìm key của người dùng"
                ];
            }
            $distributeBook = setting("book_distribute_storage", "s3biz:/books_public");
            if($distributeBook){
                $enqueueData['upload_path'] = $distributeBook;
            }else{
                return [
                    "error" => "Không thể tìm thư mục cấp phát sách"
                ];
            }
            $this->asynqClient->Enqueue([
                'typename' => 'book:generate',
                'payload' => $enqueueData
            ]);
            return [
                "status" => "success"
            ];
        }

    }
    public function getOrderCode()
    {
        $order = DB::table('orders')->orderBy('id', 'DESC')->first();
        $prefix = 'DH';
        $orderCode = $order->code;
        $maxCode = (int) (str_replace($prefix, '', $orderCode)) + 1;

        if ($maxCode < 10) {
            return $prefix . '000000' . ($maxCode);
        } else if ($maxCode < 100) {
            return $prefix . '00000' . ($maxCode);
        } else if ($maxCode < 1000) {
            return $prefix . '0000' . ($maxCode);
        } else if ($maxCode < 10000) {
            return $prefix . '000' . ($maxCode);
        } else if ($maxCode < 100000) {
            return $prefix . '00' . ($maxCode);
        } else if ($maxCode < 1000000) {
            return $prefix . '0' . ($maxCode);
        } else if ($maxCode < 10000000) {
            return $prefix . ($maxCode);
        }

    }
    public function getBookOwner($customerId, $productIds)
    {
        return $this->bookOwner->getBookOwners($customerId, $productIds);
    }
    public function renderOrderDetail($order, $type = 'customer')
    {
        $exchangePointHtml = '';
        if ($order->exchange_points) {
            $exchangePoints = $order->exchange_points;
            $exchangePointHtml = "(Đổi $exchangePoints điểm tích lũy)";
        }

        $statusOrder = renderOrderStatusLabel($order->status);

        $html = '
        <form id="frm-order-detail" action="">
        <div class="row">
        <div class="col-sm-6">
            <input type="hidden" class="form-control" id="store" name="store_id"/>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Khách hàng:</label>
                <div class="col-sm-8 col-form-label">
                    <a class="text-primary" href="' . url('admin/customers/detail/' . $order->customer_id) . '">' . optional($order->customer)->name . '</a>
                </div>
            </div>
            <div class="form-group row">
                <input type="hidden" value="' . $order->id . '" name="order_id"/>
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Tiền hàng:</label>
                <div class="col-sm-8 col-form-label text-primary">' . number_format($order->original_price) . ' đ</div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Giảm giá tổng đơn:</label>
                <div class="col-sm-8 col-form-label text-success">' . number_format($order->original_price - $order->price) . ' đ ' . $exchangePointHtml . '</div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Tổng đơn hàng:</label>
                <div class="col-sm-8 col-form-label text-danger">' . number_format($order->price) . ' đ</div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Người bán:</label>
                <div class="col-sm-8 col-form-label">
                    ' . optional($order->saler)->name . '
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right font-weight-bold">Trạng thái:</label>
                <div class="col-sm-4 col-form-label">
                    <label class="label label-' . $statusOrder[1] . '">' . $statusOrder[0] . '</label>
                </div>
            </div>
        </div>
        </div>
        <div class="card-datatable table-responsive"><table class="table-order-detail table" width="100%">
        <thead class="thead-light">
            <tr class="bg-primary text-white">
            <td>Mã sản phẩm</td>
            <td>Tên sản phẩm</td>
            <td>Số lượng</td>
            <td>Giá bán</td>
            <td>Chiết khấu</td>
            <td>Thành tiền</td>
            <td>Trạng thái phát hành</td>
            <td>Hành động</td>
            </tr>
        </thead><tbody>';

        if ($order && $order->detail) {
            foreach ($order->detail as $key => $value) {

                $discountText = $value->pivot->discount <= 100 ? number_format($value->pivot->discount) . '%' : number_format($value->pivot->discount) . 'đ';

                $html .= '<tr data-order-detail-id="'.$value->pivot->id.'" data-book-id="'.$value->id.'">
                    <td style="width:30%">' . $value->id . '</td>
                    <td>' . $value->name . '</td>
                    <td>' . $value->pivot->quantity . '</td>
                    <td>' . number_format($value->pivot->price) . 'đ</td>
                    <td>' . $discountText . '</td>
                    <td>' . number_format($value->pivot->subtotal) . 'đ</td>
                    <td id="task-'.$value->pivot->id.'"></td>
                    <td><div class="btn btn-primary btn-restart-book"><i class="feather icon-refresh-cw"></i></div></td>
                </tr>';
            }
        }

        $html .= '</tbody></table></div>';
        $html .= '<div class="form-group row"><div class="col-sm-12 text-center">';
        $html .= '</div></div></form>';

        return $html;
    }

    public function renderDatatable($table, $exportOrderId = '')
    {
        $data = Datatables::of($table)
            ->editColumn('id', function ($row) {
                $html = '<p><a href="#" class="text-info btn-detail-order" data-id="' . $row->id . '" data-code="' . $row->coode . '" data-toggle="modal" data-target="#modal-order-detail">' . ($row->code ?? $row->id) . '</a></p>';
                return $html;
            })
            ->editColumn('info', function ($row) {
                $name = '<div>';
                $name .= '<div class="text-left">Nhân viên: ' . optional($row->saler)->name . '</div>';
                $name .= '<div class="text-left">Khách hàng: <a href="customers/detail/' . optional($row->customer)->id . '">' . optional($row->customer)->name .' ('.optional($row->customer)->phone_number.') - ' .optional($row->customer)->email.'</a></div>';
                $name .= '<div class="text-left">Ngày tạo: ' . Carbon::parse($row->created_at)->format('d/m/Y h:i') . '</div>';
                // $name .= '<div class="text-left">Ngày bán: '.Carbon::parse($row->sell_date)->format('d/m/Y').'</div>';
                $name .= '<div class="text-left">Thanh toán: <label class="label label-info">' . renderPaymentMethod($row->payment_method) . '</label</div>';
                $name .= '</div>';

                return $name;

            })
            ->editColumn('status', function ($row) {
                $html = '<div>' . $this->renderOptionStatus($row) . '</div>';
                return $html;
            })
        // ->editColumn('total_qty', function ($row) {
        //     // $inventoryStatus = renderOrderInventoryStatusLabel($row->inventory_status);
        //     $html = '<div>'.$row->quantity.'</div>';
        //     // $html .= '<div class="mb-3"><label class="label label-'.$inventoryStatus[1].'">'.$inventoryStatus[0].'</label></div>';
        //     return $html;
        // })
            ->editColumn('total_money', function ($row) {
                return format_price($row->price) . ' đ';
            })
            ->addColumn('action', function ($row) {
                $user = Auth::user();
                $action = "";
                if ($user->can('delete_orders')) {
                    $action .= '<a href="' . url('admin/orders/delete/' . $row->id) . '" class="btn btn-danger notify-confirm" title="Xóa">
                        <i class="feather icon-trash-2"></i>
                    </a>';
                }
                // $action .= '<a class="btn btn-warning" target="_blank" href="'.url(route('order.print', ['id' => $row->id])).'"><i class="feather icon-printer" title="In đơn hàng"></i></a>';
                $action .= '<a class="btn btn-success btn-detail-order" href="#" data-id="' . $row->id . '" data-code="' . $row->id . '" data-toggle="modal" data-target="#modal-order-detail"><i class="feather icon-eye" title="Xem"></i></a>';
                $action .= '<a class="btn btn-primary" href="' . route("orders.edit", [
                    "id" => $row->id,
                ]) . '" ><i class="feather icon-edit" title="Edit"></i></a>';

                return $action;
            })
            ->rawColumns(['id', 'info', 'quantity', 'status', 'action'])
            ->make(true);
        return $data;
    }

    public function renderDatatableRevenue($table)
    {
        $data = Datatables::of($table)
            ->editColumn('id', function ($row) {
                $html = '<p><a href="#" class="text-info btn-detail-order" data-id="' . $row->id . '" data-code="' . $row->id . '" data-toggle="modal" data-target="#modal-order-detail">' . $row->id . '</a></p>';
                return $html;
            })
            ->editColumn('sell_date', function ($row) {
                return Carbon::parse($row->sell_date)->format('d/m/Y');
            })
            ->editColumn('saler', function ($row) {
                return optional($row->saler)->name;
            })
            ->editColumn('customer', function ($row) {
                return '<a class="text-info" target="_blank" href="' . url('admin/customers/detail/' . $row->customer_id) . '">' . optional($row->customer)->name . '</a>';
            })
            ->editColumn('total_qty', function ($row) {
                return $row->total_quantity;
            })
            ->editColumn('discount', function ($row) {
                return format_price($row->discount) . ' đ';
            })
            ->editColumn('total_money', function ($row) {
                return format_price($row->total_money) . ' đ';
            })
            ->editColumn('lack', function ($row) {
                return format_price($row->lack) . ' đ';
            })
            ->editColumn('status', function ($row) {
                $status = renderOrderStatusLabel($row->status);
                return '<label class="label label-' . $status[1] . '">' . $status[0] . '</label>';
            })
            ->rawColumns(['id', 'customer', 'status'])
            ->make(true);
        return $data;
    }

    public function renderOptionStatus($order)
    {
        return [
            'new' => '<label class="label label-info">Đơn mới</label>',
            'paid' => '<label class="label label-success">Đã thanh toán</label>',
            'finish' => '<label class="label label-primary">Hoàn thành</label>',
            'cancel' => '<label class="label label-danger">Huỷ</label>',
        ][$order->status];
        // $user = Auth::user();
        // $disableStatus = [];

        // $listStatus = [
        //     Order::WAITING => 'Chờ xác nhận',
        //     Order::EXPORTED => 'Đã xuất hàng',
        //     Order::SHIPPING => 'Đang vận chuyển',
        //     Order::FINISH => 'Hoàn thành',
        //     Order::FAILED => 'Thất bại',
        //     Order::CANCEL => 'Hủy'
        // ];

        // $classPrevent = '';
        // if(!$user->can('edit_orders')){
        //     $classPrevent = BLOCKED;
        // }

        // //Nếu ko phải admin thì ẩn xuất hàng/đang giao hàng
        // if(!isAdmin()){
        //     $disableStatus = [Order::EXPORTED, Order::SHIPPING];
        // }

        // //Nếu trạng thái đơn hàng là Hủy/Thất bại thì ko cho chỉnh sửa
        // // if(in_array($order->status, [Order::FAILED, Order::CANCEL])){
        // //     $classPrevent = BLOCKED;
        // // }

        // // Nếu trạng thái đơn hàng là Đã xuất kho/hoàn thành thì chỉ admin mới được đổi trạng thái
        // if(in_array($order->status, [Order::EXPORTED, Order::FINISH])){
        //     if(!isAdmin()){
        //         $classPrevent = BLOCKED;
        //     }
        // }

        // $html = '<select class="form-control populate select2 order-status '.$classPrevent.'" data-id="'.$order->id.'" style="height:unset">';
        // foreach ($listStatus as $key => $value) {
        //     $selected = $order->status == $key ? 'selected' : '';
        //     $disabled = in_array($key, $disableStatus) ? 'disabled' : '';
        //     $html .= '<option value="'.$key.'" '.$selected.' '.$disabled.'>'.$value.'</option>';
        // }

        // $html .= '</select>';

        // return $html;
    }

    public function renderOptionAdminStatus($order)
    {
        return $order->status;
        // $user = Auth::user();

        // $listStatus = [
        //     Order::ADMIN_WAITING => 'Chưa xác nhận',
        //     Order::ADMIN_APPROVE => 'Đã xác nhận',
        // ];

        // $classPrevent = '';
        // if(!isLeader()){
        //     $classPrevent = BLOCKED;
        // }

        // $html = '<select class="form-control populate select2 order-admin-status '.$classPrevent.'" data-id="'.$order->id.'" style="height:unset">';
        // foreach ($listStatus as $key => $value) {
        //     $selected = $order->admin_status == $key ? 'selected' : '';
        //     $html .= '<option value="'.$key.'" '.$selected.'>'.$value.'</option>';
        // }

        // $html .= '</select>';

        // return $html;
    }

    public function calculatorAddPoints($products)
    {
        $settingCategories = explode(',', setting('config_not_points_categories'));
        $settingBrands = explode(',', setting('config_not_points_brand'));
        $settingProducts = explode(',', setting('config_not_points_products'));

        $listProductNonPoints = [];
        if ($settingCategories) {
            $productLists = Category::whereIn('id', $settingCategories)->with('products')->get()->toArray();
            $productIds = data_get($productLists, '*.products.*.id');
            if ($productIds) {
                $listProductNonPoints = $productIds;
            }
        }

        if ($settingBrands) {
            $productLists = Brand::whereIn('id', $settingBrands)->with('products')->get()->toArray();
            $BrandProductIds = data_get($productLists, '*.products.*.id');
            if ($productIds) {
                $listProductNonPoints = array_merge($listProductNonPoints, $BrandProductIds);
            }
        }

        if ($settingProducts) {
            $productIds = array_map('intval', $settingProducts);
            if ($productIds) {
                $listProductNonPoints = array_merge($listProductNonPoints, $productIds);
            }
        }

        $orderDetail = $products->detail;

        $totalPriceAllowPoints = 0;
        $productAllowPoints = [];
        foreach ($orderDetail as $detail) {
            $productId = $detail->pivot->product_id;
            $productName = $detail->name;
            $productPrice = $detail->pivot->price;
            $productQty = $detail->pivot->qty;
            if (!in_array($productId, $listProductNonPoints)) {
                $totalPrice = $productPrice * $productQty;
                $totalPriceAllowPoints += $totalPrice;
                $productAllowPoints[] = [
                    'product_id' => $productId,
                    'product_name' => $productName,
                    'product_price' => $productPrice,
                    'product_qty' => $productQty,
                    'points' => calCustomerPoints($totalPrice),
                ];
            }
        }

        return [
            'totalPrice' => $totalPriceAllowPoints,
            'productAllowPoints' => $productAllowPoints,
        ];
    }

    //Lấy KH mới - là KH chỉ có đơn hàng duy nhất
    public function getNewCustomerIds()
    {
        return DB::table('orders')
            ->select('customer_id', DB::raw('count(customer_id) as count'))
            ->whereNull('deleted_at')
            ->groupBy('customer_id')
            ->having(DB::raw('count'), 1)
            ->get()
            ->pluck('customer_id')
            ->toArray();
    }
    public function updateOrder($orderId, $products, $userId, $createBy = null)
    {
        DB::beginTransaction();
        try {
            $totalPrice = 0;
            $subTotalPrice = 0;
            $orderDetailList = [];
            $bookIds = [];
            foreach ($products as $product) {
                $productDiscount = $product['discount'] ? (float) $product['discount'] : 0;
                $productPrice = $product['price'] ? (int) str_replace(',', '', $product['price']) : 0;
                $subTotal = $productPrice - ($productDiscount > 100 ? $productDiscount : $productPrice * $productDiscount / 100);
                $subTotalPrice += $subTotal;
                $totalPrice += $productPrice;
                $orderDetail = [
                    'order_id' => $orderId,
                    'book_id' => $product['id'],
                    'quantity' => 1,
                    'price' => $productPrice,
                    'discount' => $productDiscount,
                    'subtotal' => $subTotal,
                ];

                $orderDetailList[] = ($orderDetail);
                $bookIds[] = $product['id'];
            }
            $order = Order::whereId($orderId)->update([
                "price" => $subTotalPrice,
                "original_price" => $totalPrice,
                "status" => "new",
                "created_by" => $createBy,
            ]);
            if ($order) {
                $orderDId = [];
                foreach ($orderDetailList as $orderD) {
                    $updatedOrderDetail = OrderDetail::updateOrCreate([
                        "order_id" => $orderD['order_id'],
                        "book_id" => $orderD['book_id'],
                    ], [
                        'quantity' => $orderD['quantity'],
                        'price' => $orderD['price'],
                        'discount' => $orderD['discount'],
                        'subtotal' => $orderD['subtotal'],
                    ]);
                    BookOwner::updateOrCreate([
                        "user_id" => $userId,
                        "book_id" => $orderD['book_id']
                    ],[
                        "status" => "new",
                        "order_detail_id" => $updatedOrderDetail->id]);
                    $orderDId[] = $updatedOrderDetail->id;
                    // if($instance->wasRecentlyCreated){
                        
                    // }
                }
                BookOwner::whereRaw("order_detail_id in (select id from order_detail where order_id = ?)", [$orderId])->whereNotIn("order_detail_id", $orderDId)->delete();
                OrderDetail::where("order_id", $orderId)->whereNotIn('id', $orderDId)->delete();
                DB::commit();
                return true;
            }
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e);
            return false;
        }
    }

    public function createOrder($products, $userId, $createBy = null, $status = "new")
    {
        DB::beginTransaction();
        try {
            $totalPrice = 0;
            $subTotalPrice = 0;
            $orderDetailList = [];
            foreach ($products as $product) {
                $productDiscount = $product['discount'] ? (float) $product['discount'] : 0;
                $productPrice = $product['price'] ? (int) str_replace(',', '', $product['price']) : 0;
                $subTotal = $productPrice - ($productDiscount > 100 ? $productDiscount : $productPrice * $productDiscount / 100);
                $subTotalPrice += $subTotal;
                $totalPrice += $productPrice;
                $orderDetail = [
                    'book_id' => $product['id'],
                    'quantity' => 1,
                    'price' => $productPrice,
                    'discount' => $productDiscount,
                    'subtotal' => $subTotal,
                ];

                $orderDetailList[] = ($orderDetail);
            }
            $numOrder = Order::where("created_at", ">=" , (new Carbon())->now()->startOfDay())->where("created_at", "<=" , (new Carbon())->now()->endOfDay())->count();
            $randStr = mb_substr(md5(rand()), 0, 3);
            $oCode = 'O' . strtoupper($randStr) . str_pad($numOrder, 10,"0", STR_PAD_LEFT);
            $order = Order::create([
                "user_id" => $userId,
                "code" => $oCode,
                "price" => $subTotalPrice,
                "original_price" => $totalPrice,
                "status" => $status,
                "created_by" => $createBy,
            ]);
            if ($order) {
                // foreach($orderDetailList as $key => $value){
                //     $orderDetailList[$key]['order_id'] = $order->id;
                // }
                $newDetails = $order->orderDetail()->createMany($orderDetailList);
                if ($newDetails) {
                    $bookOwnerNews = array_map(function ($orderDetail) use ($userId) {
                        return ["user_id" => $userId,
                            "book_id" => $orderDetail["book_id"],
                            "status" => "new",
                            "order_detail_id" => $orderDetail["id"]];
                    }, $newDetails->toArray());
                    BookOwner::insert($bookOwnerNews);
                }
            }
            DB::commit();
            return true;
        } catch (\Exception $e) {
            dd($e);
            DB::rollBack();
            return false;
        }
    }
}
