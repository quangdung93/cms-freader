<?php

namespace App\Services;

use App\Models\AccessToken;
use Auth;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Customer;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class CustomerService
{
    public function getCustomerCode(){
        $customer = DB::table('customers')->orderBy('id', 'DESC')->first();
        $code = $customer->customer_code;
        $maxCode = (int)(str_replace('CX', '', $code)) + 1;

        if ($maxCode < 10)
            return 'CX000000' . ($maxCode);
        else if ($maxCode < 100)
            return 'CX00000' . ($maxCode);
        else if ($maxCode < 1000)
            return 'CX0000' . ($maxCode);
        else if ($maxCode < 10000)
            return 'CX000' . ($maxCode);
        else if ($maxCode < 100000)
            return 'CX00' . ($maxCode);
        else if ($maxCode < 1000000)
            return 'CX0' . ($maxCode);
        else if ($maxCode < 10000000)
            return 'CX' . ($maxCode);
    }

    public function getDatatable($table){
        $data = Datatables::of($table)
//            ->editColumn('id', function ($row) {
//                $path = route("customers.edit", ["customer" => $row->id]);
//                return  '<div><a class="text-primary" href="'. $path .'">'. $row->id .'</a></div>';
//            })
            ->editColumn('name', function ($row) {
                $path = route("customers.detail-view", ["id" => $row->id]);
                $name = '<div>';
                $name .= '<a class="text-primary" href="'.$path.'">'. "$row->first_name $row->name" .'</a>';
                $name .= '</div>';
                return $name;

            })
            ->editColumn('phone_number', function ($row) {
                $path = route("customers.detail-view", ["id" => $row->id]);
                $phoneNumber = '<div>';
                $phoneNumber .= '<a class="text-primary" href="'.$path.'">'. $row->phone_number .'</a>';
                $phoneNumber .= '</div>';
                return $phoneNumber;

            })
            ->editColumn('email', function ($row) {
                return "<div>$row->email</div>";

            })
            ->editColumn('dbo', function ($row) {
                $dob = $row->dob == null ? "N/A" :  '<div>' . Carbon::parse($row->dob)->format('d/m/Y') . '</div>';
                return $dob;
            })
            ->editColumn('is_active', function ($row) {
                $status =  $row->is_active === 1 ? '<label class="label label-success">Hoạt động</label>' : '<label class="label label-warning">Ngưng hoạt động</label>';
                if($row->is_deleted){
                    $deletedAt = isset($row->deleted_at) ?  date('d-m-Y',strtotime($row->deleted_at)) : 'N/A';
                    $status = '<label class="label label-danger">Đã xóa</label><br> <p class="white-space"> Ngày xóa: '. $deletedAt .' </p>';
                }
                return $status;
            })
            ->addColumn('action', function ($row) {
                $user = Auth::user();
                $action = "";
                if($user->can('edit_customers') && !$row->is_deleted){
                    $editPath = route("customers.edit", ["customer" => $row->id]);
                    $action .= '<a class="btn btn-primary" href="'. $editPath .'" title="Chỉnh sửa">
                                <i class="feather icon-edit-1"></i></a>';
                }

                if($user->can('delete_customers') && !$row->is_deleted){
                    $deletePath = route("customers.destroy", ['id' => $row->id]);
                    $action .= '<a href="'. $deletePath . '" class="btn btn-danger notify-confirm" title="Xóa">
                        <i class="feather icon-trash-2"></i>
                    </a>';
                }
                $detailPath = route("customers.detail-view", ['id' => $row->id]);
                $action .= '<a class="btn btn-success" href="'. $detailPath .'" target="_blank"><i class="feather icon-eye" title="Xem"></i></a>';

                return $action;
            })
            ->rawColumns(['dbo', 'name', 'phone_number', 'email', 'is_active','action'])
            ->make(true);
        return $data;
    }

    public function buildQueryCheckRole($query){
        //Kiểm tra quyền Đại lý/Khách lẻ
        if(!isRoleAgency()){ //Nếu ko có quyền QL đại lý
            $query->withHas('customer', function($q){
                $q->where('customer_group', '!=', Customer::GROUP_AGENCY);
            });
        }

        if(!isRoleCustomer()){ //Nếu ko có quyền QL KH lẻ
            $query->withHas('customer', function($q){
                $q->where('customer_group', '!=', Customer::GROUP_SINGLE);
            });
        }

        if(!isRoleCustomerSale()){ //Nếu ko có quyền QL KH nhân viên sale
            $query->withHas('customer', function($q){
                $q->where('customer_group', '!=', Customer::GROUP_SALE);
            });
        }

        return $query;
    }

    public function buildQueryFilterCustomerLevel($query, $level){
        if($level == 'non-member'){
            $query->where('total_money', '<', [Customer::TYPE_NON_MEMBER]);
        }
        elseif($level == 'member'){
            $query->where('total_money', '>=', [Customer::TYPE_NON_MEMBER]);
            $query->where('total_money', '<', [Customer::TYPE_MEMBER]);
        }
        elseif($level == 'silver'){
            $query->where('total_money', '>=', [Customer::TYPE_MEMBER]);
            $query->where('total_money', '<', [Customer::TYPE_SILVER]);
        }
        elseif($level == 'golden'){
            $query->where('total_money', '>=', [Customer::TYPE_SILVER]);
            $query->where('total_money', '<', [Customer::TYPE_GOLDEN]);
        }
        elseif($level == 'platinum'){
            $query->where('total_money', '>=', [Customer::TYPE_GOLDEN]);
            $query->where('total_money', '<', [Customer::TYPE_PLATINUM]);
        }
        elseif($level == 'diamond'){
            $query->where('total_money', '>=', [Customer::TYPE_PLATINUM]);
        }

        return $query;
    }

    public function buildQuerySortCustomer($query, $level){
        if($level == 'money_min'){
            $query->orderBy('total_money', 'ASC');
        }
        elseif($level == 'money_max'){
            $query->orderBy('total_money', 'DESC');
        }
        elseif($level == 'total_min'){
            $query->orderBy('total_order', 'ASC');
        }
        elseif($level == 'total_max'){
            $query->orderBy('total_order', 'DESC');
        }

        return $query;
    }

    public function buildQueryFilterCustomerTotalOrderMoney($query, $priceFrom = 0, $priceTo = 0){
        $priceFromTemp = $priceFrom ?: 0;
        $priceToTemp = $priceTo ?: 0;
        $priceFromFinal = str_replace(',', '', $priceFromTemp);
        $priceToFinal = str_replace(',', '', $priceToTemp);

        return $query->where('total_money', '>=', [$priceFromFinal])
                    ->where('total_money', '<=', [$priceToFinal]);
    }

    public function cronJobCalculateOrder(){
        $result = Order::selectRaw("count(store_id) as quantity, count(customer_id) as count, customer_id,
                SUM(total_money) as total_money,
                SUM(lack) as lack,
                SUM(total_quantity) as total_quantity")
                ->where('status', 1)
                // ->where('admin_status', 1)
                ->groupBy('customer_id')
                ->get();

        $count = 0;
        foreach ($result as $value) {
            if($value->total_money == 0){
                continue;
            }

            Customer::where('id', $value['customer_id'])->update([
                'total_money' => $value->total_money,
                'total_order' => $value->count,
                'total_lack' => $value->lack,
            ]);

            $count++;
        }

        return $count;
    }

    public function getPrefix($customerGroup){
        $prefix = '';
        if($customerGroup == Customer::GROUP_AGENCY){
            $prefix = 'agency';
        }
        elseif($customerGroup == Customer::GROUP_SALE){
            $prefix = 'saler';
        }

        return $prefix;
    }

    public function findCustomer($id)
    {
        $query = Customer::select()
                          ->with("devices")
                          ->where("users.id", $id);

        return $query->first();
    }
    public function findCustomerInfo($id)
    {
        $query = Customer::select(DB::raw("id, name, phone_number, email"))
                          ->where("users.id", $id);

        return $query->first();
    }

    public function updateCustomer($array, $id)
    {
        $query = Customer::where("users.id", $id);

        return $query->update($array, $id);
    }

    public function getCustomerDeviceDatatable($table){
        $data = Datatables::of($table)
            ->editColumn('device_id', function ($row) {
                return "<div>$row->device_id</div>";
            })
            ->editColumn('device_name', function ($row) {
                return "<div>$row->device_name</div>";
            })
            ->editColumn('device_type', function ($row) {
                return "<div>$row->device_type</div>";
            })
            ->editColumn('is_active', function ($row) {
                return $row->is_active === 1 ? '<label class="label label-success">Hoạt động</label>' : '<label class="label label-warning">Ngưng hoạt động</label>';
            })
            ->addColumn('action', function ($row) {
                $user = Auth::user();
                $action = "";
                if($user->can('delete_customers') && !$row->is_deleted){
                    $path = route("customers.device.destroy", ["id" => $row->id]);
                    $action .= '<a href="'. $path .'" class="btn btn-danger notify-confirm" title="Xóa">
                        <i class="feather icon-trash-2"></i>
                    </a>';
                }
                return $action;
            })
            ->rawColumns(['device_id', 'device_name', 'device_type', 'is_active', 'action'])
            ->make(true);
        return $data;
    }

    public function findCustomerDevice($id)
    {
        $query = AccessToken::where("id", $id);
        return $query->first();

    }

    public function updateCustomerDevice(array $array, $id)
    {
        $query = AccessToken::where("id", $id);
        return $query->update($array, $id);
    }
}
