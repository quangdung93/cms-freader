<?php

use App\Http\Controllers\Admin\BookController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Admin\BlockController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\ImageController;
use App\Http\Controllers\Admin\MediaController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\StoreController;
use App\Http\Controllers\Admin\ReportController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\CustomerController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\PermissionGroupController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Login
Route::get('/login', [LoginController::class, 'login']);
Route::post('/login', [LoginController::class, 'postLogin'])->name('login');

//****************/ ADMIN /*********************

Route::group(['prefix' => 'admin','middleware' => 'auth'], function () {
    Route::get('/', function(){
        return redirect('admin/dashbroad', 301);
    });

    Route::get('system-logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);

    Route::get('/dashbroad', [DashboardController::class, 'index'])->name('dashbroad');
    Route::get('/logout', [LoginController::class, 'logout']);
    // Book
    Route::group(['prefix' => 'book', 'middleware' => ['can:read_books']], function () {
        Route::get('/', [BookController::class, 'index'])->name('book.index');
        Route::get('datatable', [BookController::class,'getDatatable'])->name('book.view');
        Route::get('list', [BookController::class,'getLists'])->name('book.list');
        Route::get('/create', [BookController::class, 'create'])->middleware('can:add_books');
        Route::get('/search', [BookController::class, 'search']);
        Route::post('/search-paging', [BookController::class, 'searchPaging'])->name('book.list-paging');
        Route::post('/create', [BookController::class, 'store'])->middleware('can:add_books');
        Route::get('/edit/{book}', [BookController::class, 'edit'])->name("book.edit")->middleware('can:edit_books');
        Route::post('/approve', [BookController::class, 'approvePublisher'])->name("book.approvePublisher");//->middleware('can:edit_books');
        Route::post('/edit/{id}', [BookController::class, 'update'])->middleware('can:edit_books');
        Route::get('/delete/{id}', [BookController::class, 'destroy'])->middleware('can:delete_books');

    });

    //Customers
    Route::group(['prefix' => 'customers', 'middleware' => ['can:read_customers']], function () {
        Route::get('/', [CustomerController::class, 'index'])->name('customers.index');
        Route::get('datatable', [CustomerController::class,'getDatatable'])->name('customers.view');
        Route::get('list', [CustomerController::class,'getLists'])->name('customers.list');
        Route::get('/search', [CustomerController::class, 'search']);
        Route::get('/detail/{id}', [CustomerController::class, 'detail'])->name("customers.detail-view");
        Route::get('/get-detail/{id}', [CustomerController::class, 'getDetail'])->name("customers.detail");
        Route::get('/create', [CustomerController::class, 'create'])->middleware('can:add_customers');
        Route::get('/edit/{customer}', [CustomerController::class, 'edit'])->name("customers.edit")->middleware('can:edit_customers');
        Route::get('/delete/{id}', [CustomerController::class, 'destroy'])->name("customers.destroy")->middleware('can:delete_customers');

        Route::post('/create', [CustomerController::class, 'store'])->middleware('can:add_customers');
        Route::post('/edit/{id}', [CustomerController::class, 'update'])->middleware('can:edit_customers');

        Route::group(['prefix' => 'devices'], function (){
            Route::get('/datatable/{id}', [CustomerController::class,'getDeviceDatatable'])->name('customers.device.view');
            Route::post('/list', [CustomerController::class,'getCustomerDeviceList'])->name('customers.device.list');
            Route::get('/delete/{id}', [CustomerController::class,'destroyCustomerDevice'])->name('customers.device.destroy')->middleware('can:delete_customers');;
        });
    });

    //Blocks
    Route::group(['prefix' => 'blocks'], function () {
        Route::get('/', [BlockController::class, 'indexAction'])->name('blocks.index');
        Route::get('datatable', [BlockController::class,'getDatatableAction'])->name('blocks.view');
        Route::get('/detail/{id}', [BlockController::class, 'detailAction'])->name("blocks.detail");
        Route::get('/edit/{id}', [BlockController::class, 'editAction'])->name("blocks.edit");
        Route::post('/edit/{id}', [BlockController::class, 'updateAction'])->name("blocks.update");
        Route::post('/add/attribute', [BlockController::class, 'addAttributeAction'])->name("blocks.add-attribute");
        Route::post('list', [BlockController::class,'getBlocks'])->name('blocks.list');
        Route::get('create', [BlockController::class,'createAction'])->name('blocks.add');
        Route::post('/store', [BlockController::class, 'storeAction'])->name("blocks.store");
        Route::get('block-type-content', [BlockController::class,'getBlockContent'])->name('blocks.add.block-content');
    });

    //User
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', [UserController::class, 'index'])->middleware('can:read_users');
        Route::get('/create', [UserController::class, 'create'])->middleware('can:add_users');
        Route::post('/create', [UserController::class, 'store'])->middleware('can:add_users');
        Route::get('/edit/{id}', [UserController::class, 'edit']);
        Route::post('/edit/{id}', [UserController::class, 'update']);
        Route::get('/delete/{id}', [UserController::class, 'destroy'])->middleware('can:delete_users');
    });

    //Role
    Route::group(['prefix' => 'roles', 'middleware' => ['can:read_roles']], function () {
        Route::get('/', [RoleController::class, 'index']);
        Route::get('/create', [RoleController::class, 'create'])->middleware('can:add_roles');
        Route::post('/create', [RoleController::class, 'store'])->middleware('can:add_roles');
        Route::get('/edit/{id}', [RoleController::class, 'edit'])->middleware('can:edit_roles');
        Route::post('/edit/{id}', [RoleController::class, 'update'])->middleware('can:edit_roles');

        //Create permission // url : /admin/roles/create_permission/{permission_group_name}
        Route::get('/create_permission/{permission}/{name?}', [RoleController::class, 'createPermission'])
        ->middleware('role:'.config('permission.role_dev'));
    });

    Route::group(['prefix' => 'permissions'], function () {
        Route::group(['prefix' => 'group'], function () {
            Route::get('/', [PermissionGroupController::class, 'index']);
            Route::get('/create', [PermissionGroupController::class, 'create'])->middleware('can:add_roles');
            Route::post('/create', [PermissionGroupController::class, 'store'])->middleware('can:add_roles');
            Route::get('/edit/{id}', [PermissionGroupController::class, 'edit'])->middleware('can:edit_roles');
            Route::post('/edit/{id}', [PermissionGroupController::class, 'update'])->middleware('can:edit_roles');
        });
    });

    //Product
    Route::group(['prefix' => 'products', 'middleware' => ['web', 'can:read_books']], function () {
        Route::get('/', [ProductController::class, 'index'])->name('products.index');
        Route::get('datatable', [ProductController::class,'getDatatable'])->name('products.view');
        Route::get('list', [ProductController::class,'getLists'])->name('product.list');
        Route::get('/create', [ProductController::class, 'create'])->middleware('can:add_books');
        Route::get('/search', [ProductController::class, 'search']);
        Route::post('/search-paging', [ProductController::class, 'searchPaging'])->name('product.list-paging');
        Route::post('/create', [ProductController::class, 'store'])->middleware('can:add_books');
        Route::get('/edit/{product}', [ProductController::class, 'edit'])->name("products.edit")->middleware('can:edit_books');
        Route::post('/edit/{id}', [ProductController::class, 'update'])->middleware('can:edit_books');
        Route::get('/delete/{id}', [ProductController::class, 'destroy'])->middleware('can:delete_books');
    });

    //Store
    Route::group(['prefix' => 'stores', 'middleware' => ['web', 'can:read_stores']], function () {
        Route::get('/', [StoreController::class, 'indexAction'])->name('stores.index');
        Route::get('datatable', [StoreController::class,'getDatatableAction'])->name('stores.view');
        Route::get('list', [StoreController::class,'getLists'])->name('stores.list');
        Route::get('/create', [StoreController::class, 'create'])->name('stores.create')->middleware('can:add_stores');
        Route::post('/create', [StoreController::class, 'storeAction'])->middleware('can:add_stores');
        Route::get('/search', [StoreController::class, 'search']);
        Route::post('/search-paging', [StoreController::class, 'searchPaging'])->name('stores.list-paging');
        Route::get('/detail/{id}', [StoreController::class, 'detail'])->name("stores.detail");
        Route::get('/edit/{id}', [StoreController::class, 'edit'])->name("stores.edit")->middleware('can:edit_stores');
        Route::post('/edit/{id}', [StoreController::class, 'updateAction'])->name("stores.update")->middleware('can:edit_stores');
        Route::get('/delete/{id}', [StoreController::class, 'destroyAction'])->name("stores.destroy")->middleware('can:delete_stores');
    });

    //Menu
    Route::group(['prefix' => 'menus', 'middleware' => ['can:read_menus']], function () {
        Route::get('/', [MenuController::class, 'index']);
        Route::get('/create', [MenuController::class, 'create'])->middleware('can:add_menus');
        Route::post('/create', [MenuController::class, 'store'])->middleware('can:add_menus');
        Route::get('/edit/{id}', [MenuController::class, 'edit'])->middleware('can:edit_menus');
        Route::post('/edit/{id}', [MenuController::class, 'update'])->middleware('can:edit_menus');
        Route::get('/builder/{id}', [MenuController::class, 'builder'])->middleware('can:edit_menus');
        Route::get('/delete/{id}', [MenuController::class, 'destroy'])->middleware('role:'.config('permission.role_dev'));

        //Menu Items
        Route::group(['prefix' => 'item'], function () {
            Route::post('/order', [MenuController::class, 'orderItem'])
            ->middleware('can:edit_menus')->name('menus.item.order');
            Route::post('/add', [MenuController::class, 'addItem'])
            ->middleware('can:add_menus')->name('menus.item.add');
            Route::post('/add_custom', [MenuController::class, 'addItemCustom'])
            ->middleware('can:add_menus')->name('menus.item.addcustom');
            Route::post('/update/{id}', [MenuController::class, 'updateItem'])
            ->middleware('can:edit_menus')->name('menus.item.update');
            Route::get('/delete/{id}', [MenuController::class, 'deleteItem'])
            ->middleware('can:delete_menus')->name('menus.item.delete');
        });
    });

    //Setting
    Route::group(['prefix' => 'settings', 'middleware' => ['can:read_settings']], function () {
        Route::get('/', [SettingController::class, 'index']);
        Route::post('/', [SettingController::class, 'update'])->middleware('can:edit_settings');
        Route::post('/create', [SettingController::class, 'store'])
        ->middleware('role:'.config('permission.role_dev'))->name('settings.add');
        Route::get('/delete/{id}', [SettingController::class, 'destroy'])
        ->middleware('role:'.config('permission.role_dev'))->name('settings.delete');
        Route::post('/order', [SettingController::class, 'order'])->name('settings.order');

        Route::get('points', [SettingController::class,'getConfigProduct'])->middleware('can:edit_settings')->name('admin.config.points');
        Route::post('points', [SettingController::class,'postConfigProduct'])->middleware('can:edit_settings');

        Route::post('/rcloneHealthCheck', [SettingController::class, 'rcloneHealthCheck'])->name('settings.rcloneCheck');

    });

    //Brand
    Route::group(['prefix' => 'brands', 'middleware' => ['can:read_products']], function () {
        Route::get('/', [BrandController::class, 'index']);
        Route::get('/create', [BrandController::class, 'create'])->middleware('can:add_products');
        Route::post('/create', [BrandController::class, 'store'])->middleware('can:add_products');
        Route::get('/edit/{id}', [BrandController::class, 'edit'])->middleware('can:edit_products');
        Route::post('/edit/{id}', [BrandController::class, 'update'])->middleware('can:edit_products');
        Route::get('/delete/{id}', [BrandController::class, 'destroy'])->middleware('can:delete_products');
    });

    //Images
    Route::group(['prefix' => 'images'], function () {
        Route::post('/upload', [ImageController::class, 'upload'])->name('images.upload');
        Route::post('/order', [ImageController::class, 'order'])->name('images.order');
        Route::post('/delete', [ImageController::class, 'delete'])->name('images.delete');
    });

    //Order
    Route::group(['prefix' => 'orders'], function () {
        Route::get('/', [OrderController::class, 'index'])->name('orders.index')->middleware('can:read_orders');
        Route::get('datatable', [OrderController::class,'getDatatable'])->name('orders.view');
        Route::get('list', [OrderController::class,'getLists'])->name('order.list')->middleware('can:read_orders');
        Route::get('create', [OrderController::class,'create'])->middleware('can:add_orders')->middleware('check_store');
        Route::get('edit/{id}', [OrderController::class,'edit'])->middleware('can:edit_orders')->name('orders.edit')->middleware('check_store');
        Route::post('edit/{id}', [OrderController::class,'update'])->middleware('can:update_orders')->name('orders.update')->middleware('check_store');
        Route::post('create', [OrderController::class,'store'])->middleware('can:add_orders');
        Route::get('/detail/{id}', [OrderController::class, 'detailOrder'])->name('orders.detail');
        Route::get('/delete/{id}', [OrderController::class, 'destroy'])->middleware('can:delete_orders');
        Route::get('/checkTask/{orderDetailId}', [OrderController::class, 'checkTask']);
        Route::post('/enqueueTask', [OrderController::class, 'enqueueTask']);
    });

    //Media
    Route::group(['prefix' => 'media'], function () {
        Route::get('/', [MediaController::class, 'index']);
        Route::group(['prefix' => 'filemanager'], function () {
            \UniSharp\LaravelFilemanager\Lfm::routes();
        });
    });

    //Report
    Route::group(['prefix' => 'chart'], function () {
        Route::post('/order-source', [ReportController::class, 'chartOrderSourceAction'])->name('api.chart.order.source');
        Route::post('/order-revenue', [ReportController::class, 'chartOrderRevenueAction'])->name('api.chart.order.revenue');
    });

    Route::get('cache', function(){
        Cache::flush();
        return back();
    })->name('cache.clear');

});

//****************/ SITE /*********************

//Site Route
Route::get('/', function(){
    return redirect('/login', 301);
});

// Route::get('/{url}', [RouteController::class, 'handle'])->where('url', '.*');
