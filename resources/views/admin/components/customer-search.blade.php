<div class="customer-searchbox" data-id="{{ !empty($old_customer_id) ? $old_customer_id : ($customer_id ?? '') }}">
    <div class="form-group row mb-0">
        <div class="col-sm-12">
            <div class="input-group search-tool">
                <input type="hidden" class="customer_id" name="{{$inputName}}" value="{{ !empty($old_customer_id) ? $old_customer_id : ($customer_id ?? '') }}"  {{ isset($orderCustomerId) ? 'disabled' : ''}}/>
                <span class="input-group-addon bg-primary"><i class="feather icon-search"></i></span>
                <input type="text" class="form-control searchbox-input customer-search-input" 
                    data-name="{{$inputName}}" onfocus="customerSearchBox.call(this)" 
                    placeholder="{{ (isset($customer_name) && $customer_name != '') ? $customer_name : 'Nhập tên khách hàng' }}" {{ isset($orderCustomerId) ? 'disabled' : ''}}/>
            </div>
        </div>  
    </div>
</div>
@push('javascript')
<script type="text/javascript">
$(document).ready(function(){

        const customerId = $('.customer-searchbox').data('id');
        if(customerId){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'GET',
                url: `/admin/customers/get-detail/${customerId}`,
                data: $(this).serialize(),
                success: function (response) {
                    $('.customer-search-input').attr('placeholder', `${response.name} (${response.phone_number}) - ${response.email}`);
                }
            });
        }
        
        //Search product into table
        customerSearchBox = function(){
            var self = $(this);
            var nameValue = $(this).data('name');
            var parent = $(this).closest('.customer-searchbox');
            $(this).autocomplete({
                minLength: 3,
                delay: 500,
                source: function (request, response) {
                    let url = "/admin/customers/search?key=" + request.term;
        
                    $.getJSON(url, function (data) {
                        if(data.error == 0){
                            let customers = data.data;
                            var itemList = $.map(customers, function (value, key) {
                                return {
                                    customer_id: value.id,
                                    customer_name: value.name,
                                    // points: value.points,
                                    email: value.email,
                                    phone: value.phone_number,
                                    class: 'product-item-search'
                                };
                            });
                        }
        
                        response(itemList);
                    }).fail(function() {
                        response([]);
                    });
                },
                select: function( event, ui ) {
                    self.closest('.search-tool').find('.customer_id').val(ui.item.customer_id);
                    $('.customer-points').html(ui.item.points);
                    $('#box-customer-points').removeClass('hidden');
                    return self.attr('placeholder', `${ui.item.customer_name} (${ui.item.phone}) - ${ui.item.email}`);
                }
            }).autocomplete("instance")._renderItem = function( ul, item ) {
                if(item.length == 0){
                    return $("<li class='search-item'>")
                        .append(renderCustomerSearchEmpty())
                        .appendTo(ul);
                }
                else{
                    return $("<li class='search-item'>")
                        .append(renderCustomerSearchItem(item))
                        .appendTo(ul);
                }
            };
        }

        function renderCustomerSearchItem(item){
            return `<a class='customer-search-box'>
                    <div><i class="feather icon-user text-primary"></i> ${item.customer_name}</div>
                    <div class='product-name'><i class="feather icon-map-pin text-primary"></i> ${item.email}</div>
                    <div class='product-name'><i class="feather icon-phone text-primary"></i> ${item.phone}</div>
                </a>`;
        }

        function renderCustomerSearchEmpty(){
            return `<a class='customer-search-box'>
                    <div>Không tìm thấy khách hàng</div>
                </a>`;
        }

    });
</script>
@endpush