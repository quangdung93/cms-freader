@extends('admin.body')
@php
    $pageName = 'Chi tiết tồn kho';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">{{ $pageName }}</h4>
                        <div class="dt-responsive table-responsive">
                            @if($product)
                                <p>Tồn kho sản phẩm: <span class="font-weight-bold">{{ $product->name }}</span></p>
                            @endif
                            <table id="datatable" class="table stableweb-table center w100">
                                <thead>
                                    <tr>
                                        <th>Mã SP</th>
                                        <th>Mã SP Kho</th>
                                        <th>Kho hàng</th>
                                        <th>Ngày nhập</th>
                                        <th>Ngày hết hạn</th>
                                        <th>Trạng thái</th>
                                        <th>Mã ĐH</th>
                                        <th>Người xuất</th>
                                        <th>Ngày xuất</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        const ajax_url = "{!! route('inventory.detail.view', ['productId' => $productId ?? 0]) !!}";
        var columns = [
            { data: 'product_code',name: 'product_code',orderable: false, searchable: false},
            { data: 'inventory_code',name: 'inventory_code'},
            { data: 'warehouse',name: 'warehouse'},
            { data: 'import_date',name: 'import_date'},
            { data: 'expried_date',name: 'expried_date'},
            { data: 'is_export',name: 'is_export'},
            { data: 'order_code',name: 'order_code'},
            { data: 'export_by',name: 'export_by'},
            { data: 'export_date',name: 'export_date'},
        ];

        showDataTableServerSide($('#datatable'), ajax_url, columns);

        const URL_LIST = "{!! route('inventory.list') !!}";
        $(document).on('submit', '#search-inventory', function(e){
            e.preventDefault();

            let self = $(this);
            let url = `${URL_LIST}?${$(this).serialize()}`;

            showDataTableServerSide($('#datatable'), url, columns);
        });
    });
</script>
@endsection