<div class="row">
    <div class="col-sm-3">
        <div class="bg-green-400 panel widget center bgimage" style="border-radius:6px; position:relative; overflow:hidden; background-image: linear-gradient(to right, #b8cbb8 0%, #b8cbb8 0%, #5ac37e 0%, #5ed76d 33%, #b3f67e 86%, #60ee80 100%); min-height:110px; padding: 15px 20px">
            <div style="width:100%; display:flex; position:relative; z-index:20; justify-content: start; color:#fff;">
                <div class="relative" style="margin-left:14px; text-align:left;">
                    <p style="color:#ffffff; display:block; margin:0px; font-weight:500; font-size:20px; line-height:17px; margin-bottom:13px;">{{number_format($totalMoney)}} đ</p>
                    <p style="display:block; text-align:left; line-height:14px; margin:0px; margin-bottom: 3px; font-size:12px">Doanh số bán được</p>
                </div>
            </div>
        </div>
    </div>
</div>