@extends('admin.body')
@php
    $pageName = 'Thống kê xuất hàng';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Bộ lọc</h4>
                        <form id="ajax-search">
                            <div class="row mb-3">
                                @can('read_customers')
                                <div class="col-sm-3">
                                    <select class="form-control populate select2" id="customer-id" name="customer_id">
                                        <option value="0">Chọn khách hàng</option>
                                        @if($customers)
                                            @foreach ($customers as $supplier)
                                                <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                @endcan

                                @can('read_agents')
                                <div class="col-sm-3">
                                    <select class="form-control populate select2" id="agency-id" name="agency_id">
                                        <option value="0">Chọn đại lý</option>
                                        @if($agency)
                                            @foreach ($agency as $supplier)
                                                <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                @endcan
                                <div class="col-sm-3">
                                    <select class="form-control populate select2" id="saler-id" name="saler_id">
                                        <option value="0">Chọn nhân viên xuất hàng</option>
                                        @if($users)
                                            @foreach ($users as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class='input-group date datetime-pick' id="date-from-pick">
                                        <input type='text' class="form-control" id="date_from" name="date_from" value="{{ format_date(\Carbon\Carbon::now()->startOfMonth()) }}"/>
                                        <span class="input-group-addon bg-primary">
                                        <span class="feather icon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class='input-group date datetime-pick' id="date-to-pick">
                                        <input type='text' class="form-control" id="date_to" name="date_to" value="{{ format_date(\Carbon\Carbon::now()->endOfMonth()) }}"/>
                                        <span class="input-group-addon bg-primary">
                                        <span class="feather icon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm-3 text-center mt-1">
                                    <div class="btn-group order-btn-calendar">
                                        <button type="button" id="current-week" class="btn btn-default">Tuần</button>
                                        <button type="button" id="current-month" class="btn btn-primary">Tháng</button>
                                        <button type="button" id="current-quarter" class="btn btn-default">Quý</button>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-success mt-1">
                                        <span><i class="feather icon-search"></i></span>
                                        Xem thống kê
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="dashbroad mb-4" id="report-revenue"></div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">{{ $pageName }}</h4>
                        <div class="dt-responsive table-responsive">
                            <table id="datatable" class="table stableweb-table center w100">
                                <thead>
                                    <tr>
                                        <th>Mã phiếu</th>
                                        <th>Mã đơn hàng</th>
                                        <th>Sản phẩm</th>
                                        <th>Khách hàng</th>
                                        <th>Số lượng</th>
                                        <th>Giá bán</th>
                                        <th>Ngày bán</th>
                                        <th>Người bán</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-body end -->

    <div class="modal fade" id="modal-order-detail">
        <div class="modal-dialog" role="document" style="max-width: 80%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Chi tiết đơn hàng <span id="order-code"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        $('.datetime-pick').datepicker({ firstDay: 1, format: 'dd/mm/yyyy'});

        const ajax_url = "{!! route('report.export') !!}";
        const REPORT_EXPORT_URL = "{!! route('report.export.analytic') !!}";
        var columns = [
            { data: 'code', name: 'code', orderable: false, searchable: false},
            { data: 'order_code',name: 'order_code'},
            { data: 'product_name',name: 'product_name'},
            { data: 'customer_name',name: 'customer_name'},
            { data: 'qty',name: 'qty'},
            { data: 'price',name: 'price'},
            { data: 'export_date',name: 'export_date'},
            { data: 'created_by',name: 'created_by'}
        ];

        showDataTableServerSide($('#datatable'), ajax_url, columns);

        loadReportRevenue();

        $(document).on('click', '.btn-detail-order', function(){
            let order_id = $(this).data('id');
            let order_code = $(this).data('code');

            if(!order_id){
                return;
            }

            $.get(`${URL_MAIN}admin/orders/detail/${order_id}?type=order`, {}, function(response){
                if(response.error == 0){
                    $('#order-code').text(order_code);
                    $('#modal-order-detail .modal-body').html(response.data);
                }
            });
        });

        $(document).on('submit', '#ajax-search', function(e){
            e.preventDefault();

            let self = $(this);
            let url = `${ajax_url}?${$(this).serialize()}`;
            loadReportRevenue();
            showDataTableServerSide($('#datatable'), url, columns);
        });

        function loadReportRevenue(){
            const dateFrom = $('#date_from').val();
            const dateTo = $('#date_to').val();
            const salerId = $('#saler-id').find(':selected').val();
            const customerId = $('#customer-id').find(':selected').val();
            const agencyId = $('#agency-id').find(':selected').val();
            
            $.get(`${REPORT_EXPORT_URL}?saler_id=${salerId}&date_from=${dateFrom}&date_to=${dateTo}&customer_id=${customerId}&agency_id=${agencyId}`, {}, function(response){
                if(response.error == 0){
                    $('#report-revenue').html(response.data);
                }
            });
        }

        $(document).on('click', '#current-week', function(){
            setCurrentWeek();
            resetClassBtn($(this));
        });

        $(document).on('click', '#current-quarter', function(){
            setCurrentQuarter();
            resetClassBtn($(this));
        });

        $(document).on('click', '#current-month', function(){
            setCurrentMonth();
            resetClassBtn($(this));
        });

    });
</script>
@endsection