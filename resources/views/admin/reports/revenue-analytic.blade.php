<div class="row">
    <div class="col-sm-3">
        <div class="bg-green-400 panel widget center bgimage" style="border-radius:6px; position:relative; overflow:hidden; background-image: linear-gradient(to right, #6a11cb 0%, #2575fc 100%);">
            <div style="width:100%; height:40px; display:flex; position:relative; z-index:20; justify-content: start; color:#fff;">
                <div class="relative" style="margin-left:14px; text-align:left;">
                    <p style="color:#ffffff; display:block; margin:0px; font-weight:500; font-size:20px; line-height:17px; margin-bottom:13px;">{{ $countTotal}}/{{$totalQuantity }}</p>
                    <p style="display:block; color:#dce9fe; text-align:left; line-height:14px; margin:0px;font-size:12px">Số đơn / Số lượng sản phẩm</p>
                </div>
            </div>

        </div>
    </div>
    <div class="col-sm-3">
        <div class="bg-green-400 panel widget center bgimage" style="border-radius:6px; position:relative; overflow:hidden; background-image: linear-gradient(to right, #b8cbb8 0%, #b8cbb8 0%, #b465da 0%, #cf6cc9 33%, #ee609c 66%, #ee609c 100%);">
            <div style="width:100%; height:40px; display:flex; position:relative; z-index:20; justify-content: start; color:#fff;">
                <div class="relative" style="margin-left:14px; text-align:left;">
                    <p style="color:#ffffff; display:block; margin:0px; font-weight:500; font-size:20px; line-height:17px; margin-bottom:13px;">{{ number_format($totalDiscount)}} đ</p>
                    <p style="display:block; text-align:left; line-height:14px; margin:0px;font-size:12px">Tiền thưởng / tháng</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="bg-green-400 panel widget center bgimage" style="border-radius:6px; position:relative; overflow:hidden; background-image: linear-gradient(to right, #b8cbb8 0%, #b8cbb8 0%, #5ac37e 0%, #5ed76d 33%, #b3f67e 86%, #60ee80 100%); min-height:110px; padding: 15px 20px">
            <div style="width:100%; display:flex; position:relative; z-index:20; justify-content: start; color:#fff;">
                <div class="relative" style="margin-left:14px; text-align:left;">
                    <p style="color:#ffffff; display:block; margin:0px; font-weight:500; font-size:20px; line-height:17px; margin-bottom:13px;">{{number_format($totalMoney)}} đ</p>
                    <p style="display:block; text-align:left; line-height:14px; margin:0px; margin-bottom: 3px; font-size:12px">Doanh số bán được</p>
                    @if($revenue_type == 2)
                        <p style="display:block; text-align:left; line-height:14px; margin:0px; margin-bottom: 3px; font-size:12px">Khách mới: {{ number_format($totalMoneyNewCustomer) }} đ</p>
                        <p style="display:block; text-align:left; line-height:14px; margin:0px; margin-bottom: 3px; font-size:12px">Khách cũ: {{ number_format($totalMoney - $totalMoneyNewCustomer) }} đ</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @if($revenue_type == 2 || $revenue_type == 4)
        <div class="col-sm-3">
            <div class="bg-green-400 panel widget center bgimage" style="border-radius:6px; position:relative; overflow:hidden; background-image: url({{ asset('assets/images/tails-bg.png') }}); background-size:cover; min-height:110px; padding: 15px 20px">
                <div style="width:100%; height:40px; display:flex; position:relative; z-index:20; justify-content: start; color:#fff;">
                    <div class="relative" style="margin-left:14px; text-align:left;">
                        <p style="color:#ffffff; display:block; margin:0px; font-weight:500; font-size:20px; line-height:17px; margin-bottom:13px;">{{ number_format($totalMoneyShipping) }} đ</p>
                        <p style="display:block; text-align:left; color:#dce9fe; line-height:14px; margin:0px; margin-bottom: 3px;font-size:12px">Doanh số đang giao</p>
                        @if($revenue_type == 2)
                            <p style="display:block; text-align:left; line-height:14px; margin:0px; margin-bottom: 3px; font-size:12px">Khách mới: {{ number_format($totalShippingNewCustomer) }} đ</p>
                            <p style="display:block; text-align:left; line-height:14px; margin:0px; margin-bottom: 3px; font-size:12px">Khách cũ: {{ number_format($totalMoneyShipping - $totalShippingNewCustomer) }} đ</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>