@extends('admin.body')
@php
    $pageName = 'Nhập hàng';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="col-sm-12">
            @can('add_product_imports')
                <div class="text-right mb-3">
                    <a href="{{url($routeName.'/create')}}" class="btn btn-primary"><i
                            class="feather icon-shopping-cart"></i> Nhập hàng</a>
                </div>
            @endcan
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">{{ $pageName }}</h4>
                        <div class="dt-responsive table-responsive">
                            <table id="datatable" class="table stableweb-table center w100">
                                <thead>
                                    <tr>
                                        <th>Mã đơn</th>
                                        <th>Thông tin</th>
                                        <th>Tổng tiền</th>
                                        <th>Ngày nhập hàng</th>
                                        <th>Ngày tạo</th>
                                        <th>Thao tác</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-body end -->

    <div class="modal fade" id="modal-order-detail">
        <div class="modal-dialog" role="document" style="max-width: 80%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Chi tiết đơn nhập hàng <span id="order-code"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        const ajax_url = "{!! route('import.view') !!}";
        var columns = [
            { data: 'code',name: 'code',orderable: false, searchable: false},
            { data: 'info',name: 'info'},
            { data: 'total',name: 'total'},
            { data: 'import_date',name: 'import_date'},
            { data: 'created_at',name: 'created_at'},
            { data: 'action',orderable: false, searchable: false, className: 'nowrap'}
        ];

        showDataTableServerSide($('#datatable'), ajax_url, columns);

        $(document).on('click', '.btn-detail-order', function(){
            let order_id = $(this).data('id');
            let order_code = $(this).data('code');

            if(!order_id){
                return;
            }

            $.get(`${URL_MAIN}admin/imports/detail/${order_id}`, {}, function(response){
                if(response.error == 0){
                    $('#order-code').text(order_code);
                    $('#modal-order-detail .modal-body').html(response.data);
                }
            });
        });
    });
</script>
@endsection