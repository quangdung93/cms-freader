@extends('admin.body')
@php
    $pageName = 'Nhập hàng';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="panel-body">
            <form class="form-horizontal" action="{{url($routeName)}}" method="POST" role="form"
                enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Chọn sản phẩm</h4>
                                <div class="card-block">
                                    @include('admin.components.product-search-import', 
                                    [
                                        'inputName' => 'products[]',
                                        'products' => $products ?? null,
                                        'mode' => 'import'
                                    ])
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Thông tin {{ $pageName }}</h4>
                                <div class="form-group row">
                                    <input type="hidden" class="form-control" id="store" name="store_id"/>
                                    <label class="col-sm-3 col-form-label text-right">Nhà phân phối:</label>
                                    <div class="col-sm-3">
                                        <select class="form-control populate select2" name="supplier_id">
                                            @if($suppliers)
                                                @foreach($suppliers as $supplier)
                                                    <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if ($errors->has('supplier_id'))
                                            <div class="text-danger mt-2">{{ $errors->first('supplier_id') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Nhập vào:</label>
                                    <div class="col-sm-3 warehouse-box" style="display: none">
                                        <select id="warehouses" class="form-control select2" name="warehouse_id"></select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Ngày nhập hàng:</label>
                                    <div class="col-sm-3">
                                        <div class='input-group date datetime-pick'>
                                            <input type='text' class="form-control" name="import_date" value="{{ format_date(\Carbon\Carbon::now()) }}"/>
                                            <span class="input-group-addon bg-primary">
                                            <span class="feather icon-calendar"></span>
                                            </span>
                                        </div>
                                        @if ($errors->has('import_date'))
                                            <div class="text-danger mt-2">{{ $errors->first('import_date') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <x-textarea type="" title="Ghi chú" name="notes" value=""/>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Tổng cộng</label>
                                    <div class="col-sm-3">
                                        <input type="text" id="total-money" name="total" class="form-control prevent-event text-right" value="{{ isset($product) ? number_format($product->price) : 0 }}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6 text-right">
                        <a class="btn btn-danger btn-label-left f-right" href="{{url($routeName)}}">
                            <span><i class="feather icon-chevrons-left"></i></span>
                            Hủy
                        </a>
                        <button type="submit" class="btn btn-primary btn-label-left">
                            <span><i class="feather icon-save"></i></span>
                            Lưu
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<!-- Page-body end -->

<div class="modal fade" id="modal-add-customer">
    <div class="modal-dialog" role="document" style="max-width: 900px">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="feather icon-plus-circle text-success"></i> Tạo khách hàng <span id="order-code"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form id="frm-add-customer" class="form-validate" data-action="{{url(route('customer.api.add'))}}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label class="col-sm-3 text-right">Loại KH</label>
                                <div class="col-sm-9">
                                    <input type="radio" id="gender-male" name="customer_group" value="1" checked>&nbsp;<label for="gender-male">Khách lẻ</label>&nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="gender-famale" name="customer_group" value="2">&nbsp;<label for="gender-famale">Khách sỉ</label>
                                </div>
                            </div>
                            <x-input type="text" title="Họ tên" name="name" value=""/>
                            <x-input type="text" title="Điện thoại" name="phone" value=""/>
                            <x-input type="text" title="Email" name="email" value=""/>
                            <x-input type="text" title="Địa chỉ" name="address" value=""/>
                            <div class="form-group row">
                                <label class="col-sm-3 text-right">Giới tính</label>
                                <div class="col-sm-9">
                                    <input type="radio" id="gender-male" name="gender" value="0" checked>&nbsp;<label for="gender-male">Nam</label>&nbsp;&nbsp;&nbsp;
                                    <input type="radio" id="gender-famale" name="gender" value="1">&nbsp;<label for="gender-famale">Nữ</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Ngày sinh</label>
                                <div class="col-sm-9">
                                    <div class='input-group date datetime-pick'>
                                        <input type='text' class="form-control" name="birthday" value="{{ format_date(\Carbon\Carbon::now()) }}"/>
                                        <span class="input-group-addon bg-primary">
                                        <span class="feather icon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <x-textarea type="" title="Ghi chú" name="note" value="" />
                            <x-textarea type="" title="Tình trạng da" name="note_skin" value="" />
                            <x-textarea type="" title="Tính cách" name="note_genitive" value="" />
                        </div>
                        <div class="col-12 d-flex flex-sm-row justify-content-center flex-column mt-2">
                            <button type="reset" data-dismiss="modal" class="btn btn-outline-secondary mr-sm-1" aria-label="Close">Hủy bỏ</button>
                            <button type="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0">Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        $('.datetime-pick').datepicker({format: 'dd/mm/yyyy'});
        getWarehouseByStore();

        $(document).on('submit', '#frm-add-customer', function(e){
            e.preventDefault();

            let self = $(this);

            if($('input[name="name"]').val() === ''){
                pushNotify('Bạn chưa nhập họ tên!', text = '', type = 'danger');
                return;
            }

            if($('input[name="phone"]').val() === ''){
                pushNotify('Bạn chưa nhập số điện thoại!', text = '', type = 'danger');
                return;
            }
            
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: $(this).data('action'),
                data: $(this).serialize(),
                success: function (response) {
                    if (response.error == 0) {
                        pushNotify('Tạo khách hàng thành công!');
                        $('#modal-add-customer').modal('hide');
                        self[0].reset();
                        $('.customer-search-input').attr('placeholder', response.data.name);
                        $('#customer_id').val(response.data.id);
                    }
                }
            });
        });

        setTimeout(() => {
            $('#store').val($("#store-id option:selected").val());
        }, 500);
    });
</script>
@endsection