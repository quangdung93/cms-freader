
@if(isset($devices) && count($devices) > 0)
    <table id="datatable" class="table stableweb-table center w100">
        <tbody>
        @foreach($devices as $device)
            <tr>
                <td>
                    <div>{{ $device['device_name'] }}</div>
                    <div>{{ $device['device_id'] }}</div>
                </td>
                <td>
                    @if($device['is_active'])
                        <label class="label label-success">Hoạt động</label>
                    @else
                        <label class="label label-warning">Ngưng hoạt động</label>
                    @endif
                </td>
                <td>
                    @if($device['is_active'])
                        <a href="{{ route("customers.device.destroy", ['id' => $device['id']]) }}" class="btn btn-danger notify-confirm" title="Xóa">
                            <i class="feather icon-trash-2"></i>
                        </a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <div>Khách hàng chưa đăng nhập trên thiết bị nào</div>
@endif
