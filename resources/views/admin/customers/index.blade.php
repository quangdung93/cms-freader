@extends('admin.body')
@php
    $pageName = 'Người dùng';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="row ">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Bộ lọc</h4>
                        <form id="search-customer">
                            <div class="row">
                                <div class="col-sm-4">
                                    <input class="form-control" name="name" placeholder="Nhập tên, mã hoặc SDT khách hàng"/>
                                </div>
                                <div class="col-sm-4">
                                    <button type="submit" class="btn btn-success mt-1">
                                        <span><i class="feather icon-search"></i></span>
                                        Tìm kiếm
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">{{ $pageName }}</h4>
                        <div class="dt-responsive table-responsive">
                            <table id="datatable" class="table stableweb-table center w100">
                                <thead>
                                    <tr>
{{--                                        <th>Mã KH</th>--}}
                                        <th>Tên KH</th>
                                        <th>Số điện thoại</th>
                                        <th>Email</th>
                                        <th>Ngày sinh</th>
                                        <th>Trạng thái</th>
                                        <th>Chỉnh sửa</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-body end -->
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        const ajax_url = "{!! route('customers.view') !!}";
        const URL_ORDER_LIST = "{!! route('customers.list') !!}";
        const columns = [
            // { data: 'id',name: 'id', orderable: false, searchable: false},
            { data: 'name',name: 'name',width: '25%'},
            { data: 'phone_number',name: 'phone_number'},
            { data: 'email',name: 'email'},
            { data: 'dbo',name: 'dbo'},
            { data: 'is_active',name: 'is_active'},
            { data: 'action',orderable: false, searchable: false, className: 'nowrap'}
        ];

        showDataTableServerSide($('#datatable'), ajax_url, columns);

        $(document).on('submit', '#search-customer', function(e){
            e.preventDefault();

            let self = $(this).serialize();
            let url = `${URL_ORDER_LIST}?${self}`;
            showDataTableServerSide($('#datatable'), url, columns);
        });

    });
</script>
@endsection
