@extends('admin.body')
@php
    $type = request()->get('type');
//    $pageName = getCustomerGroup($type);
    $routeName = getCurrentSlug();
    $pageName = "Người dùng";

@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="panel-body">
            <form class="form-horizontal" action="{{url($routeName)}}" method="POST" role="form"
                enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Thông tin {{ $pageName }}</h4>
                                <x-input type="text" title="Họ" name="first_name" value="{{ $customer->first_name ?? ''  }}"/>
                                <x-input type="text" title="Tên" name="name" value="{{ $customer->name ?? ''  }}"/>
                                <x-input type="text" title="Điện thoại" name="phone" value="{{ $customer->phone_number ?? ''  }}"/>
                                <x-input type="text" title="Email" name="email" value="{{ $customer->email ?? ''  }}"/>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Ngày sinh</label>
                                    <div class="col-sm-9">
                                        <div class='input-group date datetime-pick'>
                                            <input type='text' class="form-control" name="birthday" value="{{ isset($customer->dob) ? format_date($customer->dob) : format_date(\Carbon\Carbon::now()) }}"/>
                                            <span class="input-group-addon bg-primary">
                                            <span class="feather icon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 text-right">Giới tính</label>
                                    <div class="col-sm-9">
                                        <input type="radio" id="gender-male" name="gender" value="0" {{ isset($customer) ? ($customer->gender == 0 ? 'checked' : '') : 'checked' }}>&nbsp;<label for="gender-male">Nam</label>&nbsp;&nbsp;&nbsp;
                                        <input type="radio" id="gender-famale" name="gender" value="1" {{(isset($customer) && $customer->gender == 1) ? 'checked' : '' }}>&nbsp;<label for="gender-famale">Nữ</label>
                                    </div>
                                </div>

{{--                                <div class="form-group row">--}}
{{--                                    <label class="col-sm-3 text-right">Trạng thái</label>--}}
{{--                                    <div class="col-sm-12">--}}
{{--                                        <x-switch-box--}}
{{--                                            type="long"--}}
{{--                                            title="Trạng thái"--}}
{{--                                            name="is_active"--}}
{{--                                            checked="{{ !isset($customer) ? 'true' : ($customer->is_active ? 'true' : '') }}"/>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </div>
                            <x-submit-button :route="$routeName"/>
                        </div>
                    </div>
                    <div class="col-sm-3">

                        <div class="card">
                            <div class="card-block">
                                <div class="col-sm-12">
                                    <x-switch-box
                                        type="short"
                                        title="Trạng thái"
                                        name="is_active"
                                        checked="{{ !isset($customer) ? 'true' : ($customer->is_active ? 'true' : '') }}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
<!-- Page-body end -->
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        $('.datetime-pick').datepicker({format: 'dd/mm/yyyy'});
        getDeviceList("{!! $customer->id !!}");
        function getDeviceList(customerId){
            $.post(
                "{!! route("customers.device.list") !!}",
                {
                    customer_id: customerId
                },
                function(response){
                    console.log(response);
                    if(response.error == 0){
                        $('#devices').html(response.data);
                    }
                });
        }

    });
</script>
@endsection
