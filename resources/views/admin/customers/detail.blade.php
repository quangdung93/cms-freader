@extends('admin.body')
@php
    $pageName = 'Khách hàng';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="col-sm-12 mb-3">
            <div class="text-right">
                 @can('edit_customers')
                    <a href="{{ route("customers.edit", ['customer' => $customer->id])}}" class="btn btn-primary"><i
                    class="feather icon-edit"></i> Sửa</a>
                @endcan
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Thông tin khách hàng</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Tên khách hàng</label>
                                    <div class="col-sm-8 col-form-label">{{ $customer->first_name ?? "" }} {{ $customer->name }}</div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Số điện thoại</label>
                                    <div class="col-sm-8 col-form-label">{{ $customer->phone_number }}</div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Giới tính</label>
                                    <div class="col-sm-8 col-form-label">{{ $customer->gender ? 'Nữ' : 'Nam' }}</div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Email</label>
                                    <div class="col-sm-8 col-form-label">{{ $customer->email ?: '(Chưa có)' }}</div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Ngày sinh</label>
                                    <div class="col-sm-8 col-form-label">{{ is_numeric($customer->dob) ? Carbon\Carbon::parse((int)$customer->birthday)->format('d/m/Y') : format_date($customer->birthday) }}</div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>

                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Thông tin thiết bị</h4>
                        <div class="card-datatable table-responsive p-2">
                            <table id="device-datatable" class="datatable stableweb-table table">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Mã thiết bị</th>
                                        <th>Tên thiết bị</th>
                                        <th>Loại thiết bị</th>
                                        <th>Trạng thái</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(!empty($customer->devices))
                                    @foreach($customer->devices as $row)
                                        <tr>

                                            <td>{{$row->device_id}}</td>
                                            <td>{{$row->device_name}}</td>
                                            <td>{{$row->device_type}}</td>
                                            <td>
                                                @if($row->is_active)
                                                    <label class="label label-success">Hoạt động</label>
                                                @else
                                                    <label class="label label-warning">Ngưng hoạt động</label>
                                                @endif
                                            </td>
                                            <td>
                                                @can('delete_customers')
                                                    <a href="{{route("customers.device.destroy", ["id" => $row->id])}}" class="btn btn-danger notify-confirm" title="Xóa">
                                                        <i class="feather icon-trash-2"></i>
                                                    </a>
                                                @endcan

                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        const deviceListUrl = "{!! route('customers.device.view', ['id' => $customer->id]) !!}";
        const columns = [
            // { data: 'id',name: 'id', orderable: false, searchable: false},
            { data: 'device_id',name: 'device_id'},
            { data: 'device_name',name: 'device_name'},
            { data: 'device_type',name: 'device_type'},
            { data: 'is_active',name: 'is_active'},
            { data: 'action',orderable: false, searchable: false, className: 'nowrap'}
        ];

        // showDataTableServerSide($('#device-datatable'), deviceListUrl, columns, true);
        // $(document).on('click', '.receive-care', function(e){
        //     e.preventDefault();
        //     var url_target = $(this).attr("href");
        //     var notify_text = $(this).data("text");
        //     var notice = new PNotify({
        //         title: $(this).data("title") || 'Xác nhận',
        //         text: notify_text ? `<p>${notify_text}</p>` : '<p>Bạn có muốn nhận chăm sóc khách hàng này?</p>',
        //         hide: false,
        //         type: 'success',
        //         confirm: {
        //             confirm: true,
        //             buttons: [{text: 'Xác nhận', addClass: 'btn btn-sm btn-primary'},{text:'Hủy bỏ', addClass: 'btn btn-sm btn-link'}]
        //         },
        //         buttons: {closer: false,sticker: false},
        //         history: {history: false}
        //     });
        // })
    });
</script>
@endsection
