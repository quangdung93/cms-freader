@extends('admin.body')
@php
    $pageName = 'Công việc';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="row ">
            <div class="col-sm-12">
                @can('add_tasks')
                    <div class="text-right mb-3">
                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal-task-add"><i class="feather icon-plus"></i> Thêm mới</a>
                    </div>
                @endcan
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Tìm kiếm</h4>
                        <form id="search-customer">
                        <div class="row">
                                <div class="col-sm-4">
                                    <input class="form-control" name="task_name" placeholder="Nhập tên công việc"/>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control populate select2" name="assign_by">
                                        <option value="0">Người thực hiện</option>
                                        @if($users)
                                            @foreach($users as $key => $user)
                                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control populate select2" name="task_type">
                                        <option value="0">Loại công việc</option>
                                        @foreach(App\Models\Task::TYPE as $key => $type)
                                            <option value="{{ $key }}">{{ $type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-4 mt-3">
                                    <select class="form-control populate select2" name="status">
                                        <option value="0">Trạng thái</option>
                                        @foreach(App\Models\Task::STATUS as $key => $status)
                                            <option value="{{ $key }}">{{ $status }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-4 mt-3">
                                    <select class="form-control populate select2" name="priority">
                                        <option value="0">Độ ưu tiên</option>
                                        @foreach(App\Models\Task::PRIORITY as $key => $priority)
                                            <option value="{{ $key }}">{{ $priority }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-4 mt-3">
                                    <button type="submit" class="btn btn-success mt-1">
                                        <span><i class="feather icon-search"></i></span>
                                        Tìm kiếm
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">{{ $pageName }}</h4>
                        <div class="dt-responsive table-responsive">
                            <table id="datatable" class="table stableweb-table center w100">
                                <thead>
                                    <tr>
                                        <th>Tên công việc</th>
                                        <th>Độ ưu tiên</th>
                                        <th>Trạng thái</th>
                                        <th>Người thực hiện</th>
                                        <th>Người tạo</th>
                                        <th>Thao tác</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-body end -->

    @include('admin.modal.add-task', [
        'mode' => 'add_task',
        'users' => $users, 
        'customers' => $customers
    ])

    <div id="show-edit"></div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        const ajax_url = "{!! route('task.view') !!}";
        const URL_TASK_LIST = "{!! route('task.list') !!}";
        const columns = [
            { data: 'task_name',name: 'task_name',width: '25%'},
            { data: 'priority',name: 'priority'},
            { data: 'status',name: 'status'},
            { data: 'assign_by',name: 'assign_by'},
            { data: 'created_by',name: 'created_by', className: 'nowrap'},
            { data: 'action',orderable: false, searchable: false, className: 'nowrap'}
        ];

        showDataTableServerSide($('#datatable'), ajax_url, columns);

        $(document).on('submit', '#search-customer', function(e){
            e.preventDefault();

            let self = $(this);
            let url = `${URL_TASK_LIST}?${$(this).serialize()}`;

            showDataTableServerSide($('#datatable'), url, columns);
            
            // $.ajax({
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     },
            //     method: 'GET',
            //     url: url,
            //     success: function (response) {
            //         if (response.error == 0) {
            //             showDataTableServerSide($('#datatable'), ajax_url, columns);
            //         }
            //     }
            // });
        });

        $(document).on('click', '.show-edit-task', function(e){
            e.preventDefault();
            const self = $(this);
            const taskId = $(this).data('id');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: "{{ route('task.show.edit') }}",
                data: {task_id: taskId},
                success: function (response) {
                    if (response.error == 0) {
                        $('#show-edit').html(response.data);
                        $('#modal-task-edit').find('.select2').select2();
                        tinymce.init({selector:'textarea'});
                        $('#modal-task-edit').find('.datetime-pick').datepicker({format: 'dd/mm/yyyy'});
                        $('#modal-task-edit').modal('show');
                    }
                }
            });
        })

        //Create Task
        $(document).on('submit', '#frm-task-add', function(e){
            e.preventDefault();

            let self = $(this);

            const assignBy = $(this).find('.assign-by').find(':selected').val();

            if($(this).find('.task-name').val() === ''){
                pushNotify('Bạn chưa nhập tên task!', text = '', type = 'danger');
                return;
            }

            if(assignBy == 0){
                pushNotify('Bạn chưa chọn người thực hiện', text = '', type = 'danger');
                return;
            }
            
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: $(this).data('action'),
                data: $(this).serialize(),
                success: function (response) {
                    if (response.error == 0) {
                        showDataTableServerSide($('#datatable'), ajax_url, columns);
                        pushNotify('Tạo công việc thành công!');
                        $('#modal-task-add').modal('hide');
                        self[0].reset();
                    }
                }
            });
        });


        //Update Task
        $(document).on('submit', '#frm-task-edit', function(e){
            e.preventDefault();

            let self = $(this);

            const assignBy = $(this).find('.assign-by').find(':selected').val();

            if($(this).find('.task-name').val() === ''){
                pushNotify('Bạn chưa nhập tên task!', text = '', type = 'danger');
                return;
            }

            if(assignBy == 0){
                pushNotify('Bạn chưa chọn người thực hiện', text = '', type = 'danger');
                return;
            }
            
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: $(this).data('action'),
                data: $(this).serialize(),
                success: function (response) {
                    if (response.error == 0) {
                        showDataTableServerSide($('#datatable'), ajax_url, columns);
                        pushNotify('Cập nhật công việc thành công!');
                        $('#modal-task-add').modal('hide');
                        self[0].reset();
                    }
                }
            });
        });

    });
</script>
@endsection