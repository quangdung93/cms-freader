@extends('admin.body')
@php
    $pageName = 'Khách hàng';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="col-sm-12 mb-3">
            <div class="text-right">
                <a href="{{url('/admin/orders/create?customer_id='.$customer->id)}}" class="btn btn-primary"><i
                    class="feather icon-shopping-cart"></i> Đặt hàng</a>
                <a href="#" data-toggle="modal" data-target="#modal-customer-care" class="btn btn-primary"><i class="feather icon-plus"></i> Tạo chăm sóc</a>
                <a href="#" class="btn btn-primary receive-care"><i class="feather icon-user-plus"></i> Nhận chăm sóc</a>
                @can('edit_customers')
                    <a href="{{url('admin/customers/edit/'.$customer->id)}}" class="btn btn-primary"><i
                    class="feather icon-edit"></i> Sửa</a>
                @endcan
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Thông tin khách hàng</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Tên khách hàng</label>
                                    <div class="col-sm-8 col-form-label">{{ $customer->name }}</div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Số điện thoại</label>
                                    <div class="col-sm-8 col-form-label">{{ $customer->phone }}</div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Giới tính</label>
                                    <div class="col-sm-8 col-form-label">{{ $customer->gender ? 'Nữ' : 'Nam' }}</div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Nhóm khách hàng</label>
                                    <div class="col-sm-8 col-form-label">{{ getCustomerGroup($customer->customer_group) }}</div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Điểm tích lũy</label>
                                    <div class="col-sm-8 col-form-label">{{ $customer->points }}</div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Địa chỉ</label>
                                    <div class="col-sm-8 col-form-label">{{ $customer->address }}</div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Nhân viên chăm sóc</label>
                                    <div class="col-sm-8 col-form-label">{{ optional($customer->saler)->name ?? 'Chưa có' }}</div>
                                </div>
                                {{-- @if($customer->points > 0)
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label text-right font-weight-bold">Số điểm tích lũy trừ</label>
                                        <div class="col-sm-5 col-form-label pr-0">
                                            <input class="form-control" type="number" value="" placeholder="Nhập số điểm tích lũy trừ" id="minus-points"/>
                                        </div>
                                        <div class="col-sm-2 col-form-label">
                                            <a href="#" class="btn btn-primary">Xác nhận</a>
                                        </div>
                                    </div>
                                @endif --}}
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Mã khách hàng</label>
                                    <div class="col-sm-8 col-form-label">{{ $customer->customer_code }}</div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Email</label>
                                    <div class="col-sm-8 col-form-label">{{ $customer->email ?: '(Chưa có)' }}</div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Ngày sinh</label>
                                    <div class="col-sm-8 col-form-label">{{ is_numeric($customer->birthday) ? Carbon\Carbon::parse((int)$customer->birthday)->format('d/m/Y') : format_date($customer->birthday) }}</div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Loại thành viên</label>
                                    <div class="col-sm-8 col-form-label">
                                        <span class="label {{ $typeCustomer['class'] }}">{{ $typeCustomer['level'] }}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Ghi chú</label>
                                    <div class="col-sm-8 col-form-label">{{ $customer->note ?: '' }}</div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Tình trạng da</label>
                                    <div class="col-sm-8 col-form-label">{{ $customer->note_skin }}</div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Tích cách</label>
                                    <div class="col-sm-8 col-form-label">{{ $customer->note_genitive }}</div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label text-right font-weight-bold">Tổng tiền mua hàng</label>
                                    <div class="col-sm-8 col-form-label">{{ number_format($totalMoney) }} đ</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Lịch sử mua hàng</h4>
                        <div class="card-datatable table-responsive p-2">
                            <table class="datatable stableweb-table table">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Mã đơn hàng</th>
                                        <th>Kho xuất</th>
                                        <th>Ngày bán</th>
                                        <th>Nhân viên</th>
                                        <th>Khách hàng</th>
                                        <th>Tổng SL</th>
                                        <th>Tổng tiền</th>
                                        <th>Hình thức</th>
                                        <th>Trạng thái</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($orders))
                                        @foreach($orders as $row)
                                            @php
                                                $status = ($row->status);
                                            @endphp
                                            <tr>
                                                <td><a href="#" class="text-info btn-detail-order" data-id="{{$row->id}}" data-code="{{$row->code}}" data-toggle="modal" data-target="#modal-order-detail">{{$row->code}}</a></td>
                                                <td>{{$row->store_id}}</td>
                                                <td>{{format_date($row->created_at)}}</td>
                                                <td>{{optional($row->user)->name}}</td>
                                                <td>{{optional($row->customer)->name}}</td>
                                                <td>{{$row->total_quantity}}</td>
                                                <td>{{number_format($row->total_price)}}</td>
                                                <td>
                                                    @if($row->payment_method == 1)
                                                        <label class="label label-success">Tiền mặt</label>
                                                    @else
                                                        <label class="label label-danger">Chuyển khoản</label>
                                                    @endif
                                                </td>
                                                <td>
                                                    <label class="label label-{{ $status[1] }}">{{ $status[0] }}</label>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Lịch sử tích điểm</h4>
                        <div class="card-datatable table-responsive p-2">
                            <table class="datatable stableweb-table table">
                                <thead class="thead-light">
                                    <tr>
                                        <th>STT</th>
                                        <th>Đơn hàng</th>
                                        <th>Điểm</th>
                                        <th>Hành động</th>
                                        <th>Ghi chú</th>
                                        <th>Ngày tạo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($pointsHistory))
                                        @foreach($pointsHistory as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ optional($row->order)->code }}</td>
                                                <td>{{$row->action == 'add' ? '+' : '-'}}{{$row->points}}</td>
                                                <td>
                                                    @if($row->action == 'add')
                                                        <label class="label label-success">Cộng</label>
                                                    @elseif($row->action == 'remove')
                                                        <label class="label label-danger">Trừ</label>
                                                    @endif
                                                </td>
                                                <td style="width: 30%">{{$row->note}}</td>
                                                <td>{{format_date($row->created_at, 'd-m-Y h:i')}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Lịch sử chăm sóc</h4>
                        <div class="card-datatable table-responsive p-2">
                            <table class="datatable stableweb-table table">
                                <thead class="thead-light">
                                    <tr>
                                        <th>STT</th>
                                        <th>Công việc</th>
                                        <th>Ghi chú</th>
                                        <th>Ngày tạo</th>
                                        <th>Người tạo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($customerCares))
                                        @foreach($customerCares as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>
                                                    @if($row->task == 'phone')
                                                        <label class="label label-info">Gọi điện</label>
                                                    @elseif($row->task == 'message')
                                                        <label class="label label-danger">Nhắn tin</label>
                                                    @elseif($row->task == 'advise')
                                                        <label class="label label-warning">Tư vấn</label>
                                                    @else
                                                        <label class="label label-default">Khác</label>
                                                    @endif
                                                </td>
                                                <td>{{$row->note}}</td>
                                                <td>{{format_date($row->created_at, 'd-m-Y h:i')}}</td>
                                                <td>{{optional($row->user)->name}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-order-detail">
        <div class="modal-dialog" role="document" style="max-width: 80%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Chi tiết đơn hàng <span id="order-code"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="modal-customer-care">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tạo chăm sóc khách hàng <span id="order-code"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <form class="form-validate" action="{{url(route('customers.care'))}}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label text-right">Công việc</label>
                                            <div class="col-sm-9">
                                                <select class="form-control populate" name="task">
                                                    <option value="phone">Gọi điện</option>
                                                    <option value="message">Nhắn tin</option>
                                                    <option value="advise">Tư vấn</option>
                                                    <option value="other">Khác</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        @if ($errors->has('task'))
                                            <div class="text-danger mt-2">{{ $errors->first('task') }}</div>
                                        @endif
                                    </div>
                                    <div class="col-sm-12">
                                        <x-textarea type="" title="Ghi chú" name="note" value="" />
                                    </div>
                                    <div class="col-12 d-flex flex-sm-row justify-content-end flex-column mt-2">
                                        <button type="reset" data-dismiss="modal" class="btn btn-outline-secondary mr-sm-1">Hủy bỏ</button>
                                        <button type="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0">Lưu</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){

        $(document).on('click', '.receive-care', function(e){
            e.preventDefault();
            var url_target = $(this).attr("href");
            var notify_text = $(this).data("text");
            var notice = new PNotify({
                title: $(this).data("title") || 'Xác nhận',
                text: notify_text ? `<p>${notify_text}</p>` : '<p>Bạn có muốn nhận chăm sóc khách hàng này?</p>',
                hide: false,
                type: 'success',
                confirm: {
                    confirm: true,
                    buttons: [{text: 'Xác nhận', addClass: 'btn btn-sm btn-primary'},{text:'Hủy bỏ', addClass: 'btn btn-sm btn-link'}]
                },
                buttons: {closer: false,sticker: false},
                history: {history: false}
            })

            // On confirm
            notice.get().on('pnotify.confirm', function() {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'POST',
                    url: '{{ route('customer.api.update') }}',
                    data: {customer_id: '{{ $customer->id }}', saler_id: '{{ \Auth::id() }}'},
                    success: function (response) {
                        if (response.error == 0) {
                            pushNotify('Nhận chăm sóc thành công!');
                        }
                    }
                });
            })

            // On cancel
            notice.get().on('pnotify.cancel', function() {
                // do nothing
            }); 
        })

        $(document).on('click', '.btn-detail-order', function(){
            let order_id = $(this).data('id');
            let order_code = $(this).data('code');

            if(!order_id){
                return;
            }

            $.get(`${URL_MAIN}admin/orders/detail/${order_id}`, {}, function(response){
                if(response.error == 0){
                    $('#order-code').text(order_code);
                    $('#modal-order-detail .modal-body').html(response.data);
                }
            });
        });
    });
</script>
@endsection