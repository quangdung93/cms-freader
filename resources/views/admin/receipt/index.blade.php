@extends('admin.body')
@php
    $pageName = 'Phiếu thu';
    $routeName = getCurrentSlug();
@endphp
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets\icon\icofont\css\icofont.css')}}">
@endsection
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                {{-- @can('add_debts')
                    <div class="text-right mb-3">
                        <a href="{{url(route('receipt.add'))}}" class="btn btn-primary"><i
                                class="feather icon-airplay"></i> Tạo Phiếu Thu</a>
                        <a href="{{url($routeName.'/export')}}" class="btn btn-success"><i
                            class="feather icon-download"></i> Xuất Excel</a>
                    </div>
                @endcan --}}
            </div>
            <div class="col-sm-12">
                <div class="dashbroad mb-4" id="report-revenue"></div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">{{ $pageName }}</h4>
                        <div class="dt-responsive table-responsive">
                            <table id="datatable" class="table stableweb-table center w100">
                                <thead>
                                    <tr>
                                        <th>Mã phiếu thu</th>
                                        <th>Mã đơn hàng</th>
                                        <th>Tên khách hàng</th>
                                        <th>Số tiền thu</th>
                                        <th>Loại phiếu</th>
                                        <th>Người tạo</th>
                                        <th>Ngày tạo</th>
                                        <th>Thao tác</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-body end -->
@endsection
@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        $('.datetime-pick').datepicker({ firstDay: 1, format: 'dd/mm/yyyy'});

        const ajax_url = "{!! route('receipt.view') !!}";
        // const REPORT_DEBT_URL = "{!! route('debt.analytic') !!}";
        var columns = [
            { data: 'code', name: 'code', orderable: false, searchable: false},
            { data: 'order_code',name: 'order_code'},
            { data: 'customer',name: 'customer'},
            // { data: 'customer_group',name: 'customer_group'},
            { data: 'value',name: 'value'},
            { data: 'type',name: 'type'},
            { data: 'created_by',name: 'created_by'},
            { data: 'created_at',name: 'created_at'},
            { data: 'action',name: 'action'},
        ];

        showDataTableServerSide($('#datatable'), ajax_url, columns);

        // loadReportDebt();

        // $(document).on('submit', '#ajax-search', function(e){
        //     e.preventDefault();

        //     let self = $(this);
        //     let url = `${ajax_url}?${$(this).serialize()}`;
        //     loadReportDebt();
        //     showDataTableServerSide($('#datatable'), url, columns);
        // });

        // function loadReportDebt(){
        //     const status = $('#status').find(':selected').val();
        //     const salerId = $('#saler-id').find(':selected').val();
        //     const customerId = $('#customer-id').find(':selected').val();
        //     const agencyId = $('#agency-id').find(':selected').val();
        //     const dateFrom = $('#date_from').val();
        //     const dateTo = $('#date_to').val();
            
        //     $.get(`${REPORT_DEBT_URL}?saler_id=${salerId}&status=${status}&date_from=${dateFrom}&date_to=${dateTo}&customer_id=${customerId}&agency_id=${agencyId}`, {}, function(response){
        //         if(response.error == 0){
        //             $('#report-revenue').html(response.data);
        //         }
        //     });
        // }
    });
</script>
@endsection