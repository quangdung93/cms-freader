<div class="modal fade" id="modal-add-banner">
    <div class="modal-dialog" role="document" style="max-width: 900px">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="feather icon-plus-circle text-success"></i> Thêm thuộc tính <span id="order-code"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form id="frm-add-banner-attribute" class="form-validate" data-action="" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-block">
                                            <h4 class="sub-title">Hình banner</h4>
                                            <x-upload-file
                                                max-height="100px"
                                                type="short"
                                                title="Ảnh đại diện"
                                                name="input_file"
                                                image=""
                                                width="100%"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Loại thuộc tính</label>
                                <div class="col-sm-6">
                                    <select class="form-control populate select2" name="banner_type" id="banner_type">
                                        <option value="slider">Slider</option>
                                        <option value="book">Sách</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row" id="group-slider">

                                <label class="col-sm-3 col-form-label text-right">Slider</label>
                                <div class="col-sm-6">
                                    <select class="form-control populate select2" name="slider_id" id="slider_id">
                                        <option value="0">Chọn slider</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row hide" id="group-book">
                                <label class="col-sm-3 col-form-label text-right">Sách</label>
                                <div class="col-sm-6">
                                    <select class="form-control populate select2" name="book_id" id="book_id">
                                        <option value="0">Chọn sách</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 d-flex flex-sm-row justify-content-center flex-column mt-2">
                            <button type="reset" data-dismiss="modal" class="btn btn-outline-secondary mr-sm-1" aria-label="Close">Hủy bỏ</button>
                            <a id="btn-save-banner-attribute" class="btn btn-primary mb-1 mb-sm-0 mr-0">Lưu</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
