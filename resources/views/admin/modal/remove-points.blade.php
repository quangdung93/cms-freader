<div class="modal fade" id="modal-points">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Trừ điểm tích lũy <span>khách hàng: {{ $customer->name }}</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form id="frm-remove-points" class="form-validate" data-action="{{url(route('customer.api.update.points'))}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <x-input type="number" title="Số điểm trừ" name="points" value="0"/>
                                </div>
                                <div class="col-sm-12">
                                    <x-textarea type="" title="Ghi chú" name="note" value="" />
                                </div>
                                <div class="col-12 d-flex flex-sm-row justify-content-end flex-column mt-2">
                                    <button type="reset" data-dismiss="modal" class="btn btn-outline-secondary mr-sm-1">Hủy bỏ</button>
                                    <button type="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0">Lưu</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@push('javascript')
<script type="text/javascript">
    $(document).on('submit', '#frm-remove-points', function(e){
        e.preventDefault();

        let self = $(this);
        
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'POST',
            url: $(this).data('action'),
            data: $(this).serialize(),
            success: function (response) {
                if (response.error == 0) {
                    pushNotify(response.message.title);
                    $('#modal-points').modal('hide');
                    self[0].reset();
                    
                    setTimeout(() => {
                        window.location.reload();
                    }, 1000);
                }
                else{
                    pushNotify(response.message.title, text = '', type = 'danger');
                }
            }
        });
    });
</script>
@endpush