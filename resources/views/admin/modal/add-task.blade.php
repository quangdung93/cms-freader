<div class="modal fade" id="modal-task{{ isset($task) ? '-edit' : '-add' }}">
    <div class="modal-dialog" role="document" style="max-width: 900px">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="feather icon-plus-circle text-success"></i> Tạo công việc <span id="order-code"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <form id="frm-task{{ isset($task) ? '-edit' : '-add' }}" 
                    class="form-validate" 
                    data-action="{{ isset($task) ? url(route('task.api.update')) : url(route('task.api.add'))}}" 
                    method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    @if(isset($task))
                        <input type="hidden" name="task_id" value="{{ $task->id }}">
                    @endif
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right">Tên công việc</label>
                                <div class="col-sm-8">
                                    <input type="text" name="task_name" value="{{ isset($task) ? $task->task_name : '' }}" class="task-name form-control" placeholder="Nhập tên công việc"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 text-right col-form-label">Loại công việc</label>
                                <div class="col-sm-8">
                                    <select class="form-control populate select2" name="task_type">
                                        @foreach(App\Models\Task::TYPE as $key => $type)
                                            <option 
                                                value="{{ $key }}" 
                                                {{ (isset($task) && $task->task_type == $key) ? 'selected' : '' }}
                                            >{{ $type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right">Số giờ dự kiến</label>
                                <div class="col-sm-8">
                                    <input type="number" name="task_time" value="{{ isset($task) ? $task->task_time : '' }}" class="form-control" placeholder="Nhập số giờ"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right">Ngày bắt đầu</label>
                                <div class="col-sm-8">
                                    <div class='input-group mb-0 date datetime-pick'>
                                        <input type='text' class="form-control" name="date_start" value="{{ isset($task) ? format_date($task->date_end) : format_date(\Carbon\Carbon::now()) }}"/>
                                        <span class="input-group-addon bg-primary">
                                        <span class="feather icon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group row">
                                <label class="col-sm-4 text-right col-form-label">Người thực hiện</label>
                                <div class="col-sm-8">
                                    <select class="form-control populate select2 assign-by" name="assign_by">
                                        <option value="0">Người thực hiện</option>
                                        @foreach($users as $key => $user)
                                            <option 
                                                value="{{ $user->id }}"
                                                {{ isset($task) ? ($task->assign_by == $user->id ? 'selected' : '') : (\Auth::id() == $user->id ? 'selected' : '') }}
                                                >
                                                {{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 text-right col-form-label">Độ ưu tiên</label>
                                <div class="col-sm-8">
                                    <select class="form-control populate select2" name="priority">
                                        @foreach(App\Models\Task::PRIORITY as $key => $item)
                                            <option value="{{ $key }}" {{ (isset($task) && $task->priority == $key) ? 'selected' : '' }}>{{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 text-right col-form-label">Trạng thái</label>
                                <div class="col-sm-8">
                                    <select class="form-control populate select2" name="status">
                                        @foreach(App\Models\Task::STATUS as $key => $item)
                                            <option value="{{ $key }}" {{ (isset($task) && $task->status == $key) ? 'selected' : '' }}>{{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right">Ngày kết thúc</label>
                                <div class="col-sm-8">
                                    <div class='input-group mb-0 date datetime-pick'>
                                        <input type='text' class="form-control" name="date_end" value="{{ isset($task) ? format_date($task->date_end) : format_date(\Carbon\Carbon::now()) }}"/>
                                        <span class="input-group-addon bg-primary">
                                        <span class="feather icon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label class="col-sm-2 text-right col-form-label">Khách hàng</label>
                                <div class="col-sm-10 {{ (isset($mode) && $mode == 'customer_care') ? BLOCKED : '' }}">
                                    @include('admin.components.customer-search', [
                                        'inputName' => 'customer_id',
                                        'customer_id' => $customerId ?? '',
                                        'customer_name' => $customerName ?? ''
                                    ])
                                    <div class="alert-warning" role="alert">
                                        (*) Người dùng chọn khách hàng cần chăm sóc nếu công việc có liên quan đến khách hàng.<br>
                                        Lưu ý: công việc này sẽ hiển thị vào lịch sử chăm sóc của khách hàng đã chọn.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label class="col-sm-12 text-left col-form-label">Ghi chú công việc:</label>
                            <x-textarea type="tinymce" title="" name="description" value="{!! isset($task) ? $task->description : '' !!}" />
                        </div>
                        <div class="col-12 d-flex flex-sm-row justify-content-center flex-column mt-2">
                            <button type="reset" data-dismiss="modal" class="btn btn-outline-secondary mr-sm-1" aria-label="Close">Hủy bỏ</button>
                            <button type="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0">Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- @push('javascript')
<script type="text/javascript">
    
</script>

@endpush --}}