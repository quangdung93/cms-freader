@extends('admin.body')
@php
    $pageName = 'Phiếu thu';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="panel-body">
            <form class="form-horizontal" action="{{url($routeName)}}" method="POST" role="form"
                enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" class="form-control" id="store" name="store_id"/>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Thông tin {{ $pageName }}</h4>
                                <div class="form-group row mb-0">
                                    <label class="col-sm-3 col-form-label text-right">Khách hàng:</label>
                                    <div class="col-sm-4 col-form-label">{{ $debt->customer->name }}</div>
                                </div>
                                <div class="form-group row mb-0">
                                    <label class="col-sm-3 col-form-label text-right">Đơn hàng:</label>
                                    <div class="col-sm-4 col-form-label">{{ $debt->order->code }}</div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Loại phiếu thu:</label>
                                    <div class="col-sm-4 col-form-label">Thu công nợ</div>
                                </div>
                                <x-textarea type="" title="Ghi chú" name="note" value=""/>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Thông tin Thanh toán</h4>
                                <div class="form-group row prevent-event">
                                    <label class="col-sm-3 col-form-label text-right">Công nợ hiện tại</label>
                                    <div class="col-sm-4">
                                        <input type="text" id="lack-total" name="lack_total" class="form-control prevent-event text-right text-danger" value="{{ number_format($debt->debt) }}"/>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Số tiền thu</label>
                                    <div class="col-sm-4">
                                        <input type="text" id="total-money" name="value" class="form-control text-right" value="{{ number_format($debt->debt) }}"/>
                                        @if ($errors->has('value'))
                                            <div class="text-danger mt-2">{{ $errors->first('value') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row prevent-event">
                                    <label class="col-sm-3 col-form-label text-right">Còn lại</label>
                                    <div class="col-sm-4">
                                        <input type="text" id="remain" name="lack_remain" class="form-control prevent-event text-right" value="0"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6 text-right">
                        <a class="btn btn-danger btn-label-left f-right" href="{{url($routeName)}}">
                            <span><i class="feather icon-chevrons-left"></i></span>
                            Hủy
                        </a>
                        {{-- <button type="submit" name="submit" value="print" class="btn btn-info btn-label-left">
                            <span><i class="feather icon-save"></i></span>
                            In Phiếu Thu
                        </button> --}}
                        <button type="submit" name="submit" value="create" class="btn btn-primary btn-label-left">
                            <span><i class="feather icon-save"></i></span>
                            Lưu
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<!-- Page-body end -->
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        $('#datetimepicker1').datepicker({format: 'dd/mm/yyyy'});

        $(document).on('submit', '#frm-add-customer', function(e){
            e.preventDefault();

            let self = $(this);

            if($('input[name="name"]').val() === ''){
                pushNotify('Bạn chưa nhập họ tên!', text = '', type = 'danger');
                return;
            }

            if($('input[name="phone"]').val() === ''){
                pushNotify('Bạn chưa nhập số điện thoại!', text = '', type = 'danger');
                return;
            }
            
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: $(this).data('action'),
                data: $(this).serialize(),
                success: function (response) {
                    if (response.error == 0) {
                        pushNotify('Tạo khách hàng thành công!');
                        $('#modal-add-customer').modal('hide');
                        self[0].reset();
                        $('.customer-search-input').attr('placeholder', response.data.name);
                        $('#customer_id').val(response.data.id);
                    }
                }
            });
        });

        $(document).on('keyup', 'input#total-money',function () {
            handlePrice();
        });

        function handlePrice(){
            let totalLack = $('#lack-total').val();
            let totalMoney = $('#total-money').val();

            totalLack = parseInt(totalLack.replaceAll(',', ''));
            totalMoney = parseInt(totalMoney.replaceAll(',', ''));
            let remain = totalLack - totalMoney;

            if(totalMoney > totalLack){
                $('#total-money').val(encodeCurrencyFormat(totalLack));
                $('#remain').val(0);
            }

            if(remain >= 0){
                $('#remain').val(encodeCurrencyFormat(remain));
            }
        }

        setTimeout(() => {
            $('#store').val($("#store-id option:selected").val());
        }, 500);

        let isCheck = true;
    });
</script>
@endsection