@extends('admin.body')
@php
    $pageName = 'Chi nhánh';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        @can('add_stores')
            <div class="col-sm-12">
                <div class="text-right mb-3">
                    <a href="{{route('stores.edit', ['id' => $data->id])}}" class="btn btn-primary"><i
                            class="feather icon-edit"></i> Chỉnh sửa </a>
                </div>
            </div>
        @endcan
        <div class="panel-body">
            <form class="form-horizontal" action="{{url($routeName)}}" method="POST" role="form"
                enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Thông tin {{ $pageName }}</h4>
                                <x-input type="text" :title="$pageName" name="name" value="{{ $data->name ?? '' }}" :disabled="true"/>
                                <x-input type="text" title="Tên công ty" name="company_name" value="{{ $data->company_name ?? '' }}"/>
                                <x-input type="text" title="Điện thoại" name="phone" value="{{ $data->phone ?? '' }}"/>
                                <x-input type="text" title="Website" name="website" value="{{ $data->website ?? '' }}"/>
                                <x-input type="text" title="Facebook" name="facebook" value="{{ $data->facebook ?? '' }}"/>
                                <x-input type="text" title="Zalo" name="zalo" value="{{ $data->zalo ?? '' }}"/>
                                <x-input type="text" title="Email" name="email" value="{{ $data->email ?? '' }}"/>
                                <x-input type="text" title="Tài khoản ngân hàng" name="bank_account" value="{{ $data->bank_account ?? '' }}"/>
                                <x-textarea type="" title="Địa chỉ" name="address" value="{{ $data->address ?? ''  }}" />
                                <x-textarea type="" title="Ghi chú" name="note" value="{{ $data->note ?? ''  }}" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Trạng thái</h4>
                                <x-switch-box
                                type="short"
                                title="Trạng thái"
                                name="status"
                                checked="{{ !isset($data) ? 'true' : ($data->status ? 'true' : '') }}"/>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Logo công ty</h4>
                                <x-upload-file
                                type="short"
                                title="Logo công ty"
                                name="logo"
                                image="{{ $data->logo ?? '' }}"
                                width="100%"
                                note="(600px x 600px)"
                                />
                            </div>
                        </div>
                    </div>
                </div>
{{--                <x-submit-button :route="$routeName"/>--}}
            </form>
        </div>
    </div>
<!-- Page-body end -->
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){

    });
</script>
@endsection
