@extends('admin.body')
@php
    $pageName = 'Chi nhánh';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="row">
            @can('add_stores')
            <div class="col-sm-12">
                <div class="text-right mb-3">
                    <a href="{{url($routeName.'/create')}}" class="btn btn-primary"><i
                            class="feather icon-plus"></i> Thêm mới</a>
                </div>
            </div>
            @endcan

            <div class="col-sm-12">
                    <div class="card">
                        <div class="card-block">
                            <h4 class="sub-title">Bộ lọc</h4>
                            <form id="search-block">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input class="form-control" name="name" placeholder="Nhập tên chi nhánh "/>
                                    </div>
                                    <div class="col-sm-4">
                                        <button type="submit" class="btn btn-success mt-1">
                                            <span><i class="feather icon-search"></i></span>
                                            Tìm kiếm
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">{{ $pageName }}</h4>
                        <div class="dt-responsive table-responsive">
                            <table id="store-datatable" class="table stableweb-table center w100">
                                <thead>
                                    <tr>
                                        <th>Mã chi nhánh</th>
                                        <th>Tên {{ $pageName }}</th>
                                        <th>Số điện thoại</th>
                                        <th>Tên công ty</th>
                                        <th>Trạng thái</th>
                                        <th>Thao tác</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-body end -->
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function(){
            const ajax_url = "{!! route('stores.view') !!}";
            const columns = [
                { data: 'id',name: 'id'},
                { data: 'name',name: 'name'},
                { data: 'phone',name: 'phone'},
                { data: 'company_name',name: 'company_name'},
                { data: 'status',name: 'status'},
                { data: 'action',orderable: false, searchable: false, className: 'nowrap'}
            ];

            showDataTableServerSide($('#store-datatable'), ajax_url, columns);

            $(document).on('submit', '#search-block', function(e){
                console.log("123123");
                e.preventDefault();
                let self = $(this).serialize();
                let url = `${ajax_url}?${self}`;
                showDataTableServerSide($('#store-datatable'), url, columns);
            });

        });
    </script>
@endsection
