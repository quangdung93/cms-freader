@extends('admin.body')
@php
    $pageName = 'Nội dung';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="row ">
            <div class="col-sm-12">
                @can('add_homepage')
                    <div class="text-right mb-3">
                        <a href="{{url($routeName.'/create')}}" class="btn btn-primary"><i
                                class="feather icon-plus"></i> Thêm mới</a>
                    </div>
                @endcan
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Bộ lọc</h4>
                        <form id="search-block">
                            <div class="row">
                                <div class="col-sm-4">
                                    <input class="form-control" name="name" placeholder="Nhập tên nội dung"/>
                                </div>
                                <div class="col-sm-4">
                                    <button type="submit" class="btn btn-success mt-1">
                                        <span><i class="feather icon-search"></i></span>
                                        Tìm kiếm
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">{{ $pageName }}</h4>
                        <div class="dt-responsive table-responsive">
                            <table id="block-datatable" class="table stableweb-table center w100">
                                <thead>
                                    <tr>
{{--                                        <th>Mã nội dung</th>--}}
                                        <th>Tên nội dụng</th>
                                        <th>Loại nội dung</th>
                                        <th>Dạng hiển thị</th>
                                        <th>Độ ưu tiên</th>
                                        <th>Trạng thái</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-body end -->
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        const ajax_url = "{!! route('blocks.view') !!}";
        const columns = [
            // { data: 'id',name: 'id'},
            { data: 'name',name: 'name'},
            { data: 'block_type',name: 'block_type'},
            { data: 'type',name: 'type'},
            { data: 'priority',name: 'priority'},
            { data: 'is_active',name: 'is_active'},
            { data: 'action',orderable: false, searchable: true, className: 'nowrap'}
        ];

        showDataTableServerSide($('#block-datatable'), ajax_url, columns);

        $(document).on('submit', '#search-block', function(e){
            e.preventDefault();
            let self = $(this).serialize();
            let url = `${ajax_url}?${self}`;
            showDataTableServerSide($('#block-datatable'), url, columns);
        });

    });
</script>
@endsection
