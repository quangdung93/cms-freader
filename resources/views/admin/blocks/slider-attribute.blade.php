
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block book-searchbox">
                <div class="row sub-title">
                    <div class="col-sm-6">
                        <span>Thuộc tính</span>
                    </div>
                    @if($isEdit)
                        <div class="col-sm-6">
                        <div class="input-group search-tool">
                            <span class="input-group-addon bg-primary"><i class="feather icon-search"></i></span>
                            <input class="form-control searchbox-input"
                                   data-name="{" onfocus="bookSearchBox.call(this)"
                                   placeholder="Nhập mã sản phẩm hoặc tên sản phẩm"/>
                        </div>
                    </div>
                    @endif

                </div>
                <div class="form-group row mb-0">
                    <div class="col-sm-12">
                        <table id="table-slider-attribute" class="table table-products table-bordered w-100 sortable" >
{{--                            @if (empty($products)) style="display: none" @endif>--}}
                            <thead>
                            <tr>
{{--                                <th>Hình ảnh</th>--}}
                                <th>Giá trị</th>
                                <th>Độ ưu tiêu</th>
                                <th>Trạng thái</th>
                                @if($isEdit)
                                    <th></th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                                @if(isset($block))
                                    @foreach($block->block_items as $item)
                                        <tr class="data item" id="attribute_{{$item['object_id']}}">
                                            <td style="width: 70%">
                                                @if($item['type'] == "text")
                                                    <input
                                                        class="form-control w-100"
                                                        type="text"  name="data[]"
                                                        value="{{ $item['data'] }}"
                                                        {{$isEdit ? "" : "disabled"}}
                                                    />
                                                @elseif("int")
                                                    <input
                                                        class="form-control w-100"
                                                        type="number"
                                                        name="data[]"
                                                        value="{{ $item['data'] }}"
                                                        {{$isEdit ? "" : "disabled"}}
                                                    />
                                                @else
                                                    <textarea
                                                        class="form-control"
                                                        name="data[]"
                                                        {{$isEdit ? "" : "disabled"}}
                                                    >
                                                    {{ $item['data'] }}
                                                    </textarea>
                                                @endif
                                            </td>
                                            <td>
                                                <input
                                                    class="form-control"
                                                    type="number"
                                                    name="priorities[]"
                                                    value="{{ $item['priority'] }}"
                                                    {{$isEdit ? "" : "disabled"}}
                                                />
                                            </td>
                                            <td>
                                                <input
                                                    id="status_{{$item['object_id']}}"
                                                    name="status[]"
                                                    type="checkbox"
                                                    value="{{$item['is_active']}}"
                                                    class="comment-status"
                                                    @if($item['is_active'])
                                                        checked
                                                    @endif
                                                    @if(!$isEdit)
                                                        disabled
                                                    @endif
                                                />
                                            </td>
                                            @if($isEdit)
                                                <td><a href=""
                                                       object-id="{{$item['object_id']}}"
                                                       class="btn btn-danger custom-notify-confirm" title="Xóa">
                                                        <i class="feather icon-trash-2"></i>
                                                    </a></td>
                                            @endif
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
