<tr id="attribute_{{$item['id']}}">
    <td style="width: 70%">
        <input type="hidden" name="object_id[]" value="{{$item['object_id']}}"/>
        <input type="hidden" name="banner_image[]" value="{{$item['thumbnail']}}"/>
        <input type="hidden" name="object_type[]" value="{{$item['object_type']}}"/>

    @if($item['type'] == "text")
            <input
                class="form-control w-100"
                type="text"  name="data[]"
                value="{{ $item['data'] }}"
                {{$isEdit ? "" : "disabled"}}
            />
        @elseif($item['type'] == "int")
            <input
                class="form-control w-100"
                type="number"
                name="data[]"
                value="{{ $item['data'] }}"
                {{$isEdit ? "" : "disabled"}}
            />
        @else
            <textarea rows="9" cols="50"
                      class="form-control"
                      name="data[]"
            >{{ $item['data'] }}
                                                    </textarea>
        @endif
    </td>
    <td>
        <input
            class="form-control"
            type="number"
            name="priorities[]"
            value="{{ $item['priority'] }}"
            {{$isEdit ? "" : "disabled"}}
        />
    </td>
    <td><input
            id="status_{{$item['object_id']}}"
            name="status[]"
            type="checkbox"
            value="{{$item['is_active']}}"
            class="comment-status"
            @if($item['is_active'])
                checked
            @endif
            @if(!$isEdit)
                disabled
            @endif
        /></td>
    @if($isEdit)
        <td><a href=""
               object-id="{{$item['id']}}"
               class="btn btn-danger custom-notify-confirm"
               title="Xóa">
                <i class="feather icon-trash-2"></i>
            </a></td>
    @endif
</tr>
