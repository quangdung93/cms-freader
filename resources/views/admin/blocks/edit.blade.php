@extends('admin.body')
@php
    $pageName = 'Nội dung';
    $routeName = getCurrentSlug();
@endphp
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets\icon\icofont\css\icofont.css')}}">
@endsection
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body panels-wells">
        <form id="form" class="form-horizontal" method="post"
              action="{{url($routeName)}}" role="form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-block">
                            <h4 class="sub-title">Thông tin {{ $pageName }}</h4>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Loại nội dung</label>
                                <div class="col-sm-6">
                                    <select class="form-control populate select2" name="type" id="type" disabled>
                                        <option value="banner" @if($block->type == 'banner') selected="selected" @endif>
                                            Banner
                                        </option>
                                        <option value="slider" @if($block->type == 'slider') selected="selected" @endif>
                                            Slider
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Vị trí hiển thị nội dung</label>
                                <div class="col-sm-6">
                                    <select class="form-control populate select2" name="block_type">
                                        <option value="homepage"
                                                @if($block->block_type == 'homepage') selected="selected" @endif>Trang
                                            chủ
                                        </option>
                                        <option value="store"
                                                @if($block->block_type == 'store') selected="selected" @endif>Cửa hàng
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Tên nội dung</label>
                                <div class="col-sm-6">
                                    <input type="text" name="id" value="{{$block->id}}" class="form-control" hidden>
                                    <input type="text" name="name" value="{{$block->name}}" class="form-control"
                                           placeholder="Nhập tên nội dung">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Độ ưu tiên</label>
                                <div class="col-sm-6">
                                    <input type="number" name="priority" value="{{$block->priority}}" class="form-control"
                                           placeholder="Nhập độ ưu tiên">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <x-switch-box
                                        type="long"
                                        title="Trạng thái"
                                        name="is_active"
                                        checked="{{$block->is_active ? 'true' : 'false'}}"/>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            @if($block->type == 'slider')
                @include('admin.blocks.slider-attribute', ['block' => $block, 'isEdit' => true])
            @else
                @include('admin.blocks.banner-attribute', ['block' => $block, 'isEdit' => true])
            @endif

            <div class="form-group row" >
                <div class="col-sm-6">
                    <button id="cancel-button" type="button" class="btn btn-danger f-right custom-button" data-dismiss="modal">Hủy</button>
                </div>
                <div class="col-sm-6">
                    <button id="submit-button" type="button" class="btn btn-success f-left custom-button">Lưu</button>
                </div>
            </div>
{{--            <x-submit-button :route="$routeName"/>--}}
        </form>
    </div>
    @include('admin.modal.add-banner')
    <!-- Page-body end -->
@endsection
@section('javascript')
    <script type="text/javascript">
        $('table tbody tr').each(function () {
            if ($(this).find('.checkbox-item:checked').length == 4) {
                $(this).find('.check-all').prop('checked', true);
            }
        });

        $(document).on('click', '.check-all', function () {
            if ($(this).is(':checked')) {
                $(this).closest('tr').find('.checkbox-item').prop('checked', true);
            } else {
                $(this).closest('tr').find('.checkbox-item').prop('checked', false);
            }
        });

        $(document).on('click', '.checkbox-item', function () {
            let check_all = $(this).closest('tr').find('.check-all');

            if (!$(this).is(':checked')) {
                check_all.is(':checked') && check_all.prop('checked', false);
            } else {
                let count_checked = $(this).closest('tr').find('.checkbox-item:checked').length;
                count_checked == 4 && check_all.prop('checked', true);
            }
        })
    </script>


    <script type="text/javascript">
        $(document).ready(function () {
            //Search product into table
            let count = {!! $block['block_item_priority'] !!} + 1;
            bookSearchBox = function () {
                var self = $(this);
                var nameValue = $(this).data('name');
                var parent = $(this).closest('.book-searchbox');
                $(this).autocomplete({
                    minLength: 3,
                    delay: 500,
                    source: function (request, response) {
                        var listIds = parent.find('.id').map(function () {
                            return $(this).val();
                        }).get();

                        var listIdsParam = '';
                        if (listIds.length) {
                            listIdsParam = '&ids=' + listIds.join(',');
                        }
                        var key = request.term + listIdsParam;
                        let url = "/admin/products/search" + "?key=" + key;
                        // $.get(url, {
                        //     key: key
                        // }, function(response){
                        //     console.log(response);
                        // });
                        $.getJSON(url, function (data) {
                            if (data.error == 0) {
                                let products = data.data;
                                var itemList = $.map(products, function (value, key) {
                                    return {
                                        id: value.id,
                                        name: value.name,
                                        thumbnail: value.thumbnail,
                                        price: value.price,
                                        class: 'product-item-search'
                                    };
                                });
                            }

                            response(itemList);
                        }).fail(function () {
                            response([]);
                        });
                    },
                    select: function (event, ui) {
                        var html = renderBook(ui.item, count);
                        parent.find('table').prepend(html);
                        self.val('');
                        if (parent.find("table").css('display') == 'none') {
                            parent.find("table").css('display', 'table');
                        }

                        $('#has-debt').attr({disabled: false});

                        count++;
                    }
                }).autocomplete("instance")._renderItem = function (ul, item) {
                    return $("<li class='search-item'>")
                        .append(renderProductSearchItem(item))
                        .appendTo(ul);
                };
            }

            function renderProductSearchItem(item) {
                return `<a class='product-search-box'>
                    <span class='product-name'> ${item.name}</span>
                </a>`;
            }

            function renderBook(book, count) {
                return `<tr class="data"  id="attribute_${book.id}">
                 <td>
                    <input type="text" class="form-control w-100" name="data[]" value="${book.id}"/>
                 </td>
                <td>
                    <input type="number" class="form-control w-100" name="priorities[]" value="${count}"/>
                 </td>
                 <td>
                    <input id='status_${book.id}' name='status[]' type='checkbox' value='1' class='comment-status' checked/>
                </td>
                <td><a class="btn btn-danger custom-notify-confirm"
                        href=""
                        object-id="${book.id}"><i class="feather icon-trash-2"></i></a></td>
            </tr>`;
            }

            $(document).on("click", ".remove-product-searchbox", function () {
                if (confirm('Bạn có muốn xóa dòng này?')) {
                    $(this).closest("tr").remove();
                }
            });

            function format_price(price) {
                return `${parseInt(price).toLocaleString('en-US')}`;
            }

            $(document).on('click', '.custom-notify-confirm', function(e){
                e.preventDefault();
                var objectId = $(this).attr("object-id");
                var notify_text = $(this).data("text");
                var notice = new PNotify({
                    title: $(this).data("title") || 'Xác nhận',
                    text: notify_text ? `<p>${notify_text}</p>` : '<p>Bạn có muốn xóa dòng này?</p>',
                    hide: false,
                    type: 'warning',
                    confirm: {
                        confirm: true,
                        buttons: [
                            {
                                text: 'Xác nhận',
                                addClass: 'btn btn-sm btn-primary'
                            },
                            {
                                text:'Hủy bỏ',
                                addClass: 'btn btn-sm btn-link'
                            }
                        ]
                    },
                    buttons: {closer: false,sticker: false},
                    history: {history: false}
                })

                // On confirm
                notice.get().on('pnotify.confirm', function() {
                    // window.location.href = url_target;
                    $("#attribute_" + objectId).remove();
                })

                // On cancel
                notice.get().on('pnotify.cancel', function() {
                    // do nothing
                });
            });


            $(document).on('click', '#cancel-button', function(e){
                e.preventDefault();
                window.location.reload();
            });
            $(document).on('click', '#submit-button', function(e){
                e.preventDefault();
                var form = $('#form');
                form.validate({
                    rules: {
                        name: {
                            required: true,
                        }
                    },
                    messages: {
                        name: {
                            required: "Vui lòng nhập vào tên nội dung",
                        }
                    },
                });
                if (!form.valid()) {
                    return false;
                }

                var formData = new FormData();
                var type = $('#type').val();
                let formSubmit = form.serializeArray();
                formSubmit.forEach((item) => {
                    if(item['name'] != "status[]"){
                        formData.append(item['name'], item['value']);
                        if(item['name'] == "data[]"){
                            var objectId = item['value'];
                            var isActive = 1;
                            var json = isValidJSON(objectId);
                            if(json != null){
                                objectId = JSON.parse(objectId);
                                objectId = objectId["blocks_id"] ?? objectId["block_id"] ?? objectId["book_id"];
                                isActive = $('#status_' + objectId).val();
                            }else{
                                isActive = $('#status_' + objectId).val();
                            }
                            formData.append('status[]',isActive);
                        }
                    }
                });
                formData.append('type', type);
                $.ajax({
                    url:  "{!! route("blocks.update", ["id" => $block['id']]) !!}",
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    // enctype: 'multipart/form-data',
                    data: formData,
                    success: function (response) {
                        if(response.error == 0){
                            pushNotifyConfirm(title = 'Thông báo', response.message, type = 'danger', textConfirm = 'Xác nhận', function(){
                                window.location.reload();
                            });
                        }else{
                            pushNotify(title = 'Thông báo', response.message, type = 'danger');
                        }
                    },
                    error: function (response) {
                    var json = response.responseJSON;
                    var message = "Có lỗi xảy ra vui lòng thử lại";
                    if(json != null){
                        message = json['message']
                    }
                    pushNotify(title = 'Thông báo', message, type = 'danger');
                }
                });
            });

            function isValidJSON(str) {
                try {
                    return JSON.parse(str);
                } catch (e) {
                    return null;
                }
            }
            function getFormData(formId){
                var formData = new FormData();
                var params = $(formId).serializeArray();
                $.each(params, function(index, val) {
                    formData[val['name']] = val['value'];
                });
                $.each($(formId).find('input[type="file"]'), function(i, tag) {
                    $.each($(tag)[0].files, function(i, file) {
                        formData[tag.name] = file;
                    });
                });
                return formData;
            }
            function getSliderAttributes(){
                var data = $("#table-slider-attribute tr.data td [name='object_id']").map(function() {
                    return $(this).val();
                }).get();
                return data;
            }

            $(document).on('click', '#btn-add-banner-attribute', function(e){

                // Set rỗng hình ảnh upload
                $('.box-image img').attr("src","");
                // Set rỗng chọn loại thuộc tính
                $('#banner_type').prop('selectedIndex', 0)
                var typeText = $('#banner_type option:selected').text();
                var type = $('#banner_type option:selected').val();
                $('#select2-banner_type-container').html(typeText);

                // Set rôn chọn thuộc tính là slider
                $('select[name="slider_id"]').prop('selectedIndex', -1)
                $('#select2-slider_id-container').html("Chọn slider");

                // Set rỗng chọn thuộc tính là sách
                $('select[name="book_id"]').prop('selectedIndex', -1)
                $('#select2-book_id-container').html("Chọn sách");

                if(type == 'book'){
                    $('#group-slider').addClass("hide");
                    $('#group-book').removeClass("hide");
                }else{
                    $('#group-slider').removeClass("hide");
                    $('#group-book').addClass("hide");
                }

                $('#modal-add-banner').modal('show');

            });

            $('[name="banner_type"]').on('change', function() {
                if(this.value == 'book'){
                    $('#group-slider').addClass("hide");
                    $('#group-book').removeClass("hide");
                }else{
                    $('#group-slider').removeClass("hide");
                    $('#group-book').addClass("hide");
                }
            });
            $('input.select2-search__field').on('keyup', function() {
                alert($(this).val()); // it alerts with the string that the user is searching
            });
            $('#group-slider .select2').select2({
                tokenSeparators: [',', ' '],
                ajax: {
                    url: "{!! route('blocks.list') !!}",
                    dataType: "json",
                    type: "POST",
                    data: function (params) {

                        var queryParameters = {
                            search: params.term,
                            page: params.page || 1,
                            type: "slider"
                        }
                        return queryParameters;
                    },
                    processResults: function (response) {
                        var blocks = [];
                        if(response.data != null){
                            blocks = response.data['data'];
                        }
                        var results = $.map(blocks, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        });
                        return {
                            results: results,
                            pagination: {
                                more: response.data['next_page_url'] != null
                            }
                        };
                    }
                }
            });

            $('#group-book .select2').select2({
                tokenSeparators: [',', ' '],
                ajax: {
                    url: "{!! route('product.list-paging') !!}",
                    dataType: "json",
                    type: "POST",
                    data: function (params) {

                        var queryParameters = {
                            search: params.term,
                            page: params.page || 1
                        }
                        return queryParameters;
                    },
                    processResults: function (response) {
                        var blocks = [];
                        if(response.data != null){
                            blocks = response.data['data'];
                        }
                        var results = $.map(blocks, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        });
                        return {
                            results: results,
                            pagination: {
                                more: response.data['next_page_url'] != null
                            }
                        };
                    }
                }
            });

            $('#btn-save-banner-attribute').on('click', function (e){
                var image = $('input[name=input_file]')[0];
                var formData = new FormData();
                if(image.files.length > 0){
                    formData.append('image', image.files[0]);
                }

                let formSubmit = $('#frm-add-banner-attribute').serializeArray();
                formSubmit.forEach((item) => formData.append(item['name'], item['value']));

                formData.append("row_count", $('#banner-table').find('tr').length);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    url: "{!! route("blocks.add-attribute") !!}",
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function (response) {
                        if(response.error == 0){
                            $('#banner-table').find('tbody').prepend(response.data['view']);
                            var elemsingle = $("#status_" + response.data['object_id']);
                            var switchery = new Switchery(elemsingle, { color: '#4680ff', jackColor: '#fff' });
                            $('#modal-add-banner').modal('hide');
                            count++;
                        }else{
                            pushNotify(title = 'Thông báo', response.message, type = 'danger');
                        }
                    },
                    error: function (response) {
                        var data = response.responseJSON;
                        var errorsHtml = '';
                        if (response.status === 422) {
                            errorsHtml = '<ul>';
                            $.each(data.errors, function (key, value) {
                                errorsHtml += '<li>' + value[0] + '</li>';
                            });
                            errorsHtml += '</ul>';

                        }else{
                            errorsHtml = data.message;
                        }
                        pushNotify(title = 'Thông báo', errorsHtml, type = 'danger');
                    }
                });
            });

            $(document).on('change','.comment-status',function(){
                let active = 0;
                if ($(this).is(':checked')) {
                    active = 1;
                }
                $(this).val(active);
            });
        });
    </script>
@endsection
