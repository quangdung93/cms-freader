@extends('admin.body')
@php
    $pageName = 'Nội dung';
    $routeName = getCurrentSlug();
@endphp
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets\icon\icofont\css\icofont.css')}}">
@endsection
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body panels-wells">
        <div class="col-sm-12 mb-3">
            <div class="text-right">
                @can('edit_homepage')
                    <a href="{{route("blocks.edit", ['id' => $block->id])}}" class="btn btn-primary"><i
                            class="feather icon-edit"></i> Sửa</a>
                @endcan
            </div>
        </div>
        <form class="form-horizontal" method="post"
              action="{{url($routeName)}}" role="form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-block">
                            <h4 class="sub-title">Thông tin {{ $pageName }}</h4>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Loại nội dung</label>
                                <div class="col-sm-6">
                                    <select class="form-control populate select2" name="type" disabled>
                                        <option value="banner" @if($block->type == 'banner') selected="selected" @endif>
                                            Banner
                                        </option>
                                        <option value="slider" @if($block->type == 'slider') selected="selected" @endif>
                                            Slider
                                        </option>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Vị trí hiển thị nội dung</label>
                                <div class="col-sm-6">
                                    <select class="form-control populate select2" name="block_type" disabled>
                                        <option value="homepage"
                                                @if($block->block_type == 'homepage') selected="selected" @endif>Trang
                                            chủ
                                        </option>
                                        <option value="store"
                                                @if($block->block_type == 'store') selected="selected" @endif>Cửa hàng
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Tên nội dung</label>
                                <div class="col-sm-6">
                                    <input type="text" name="name" value="{{$block->name}}"
                                           class="form-control"
                                           disabled
                                           placeholder="Nhập tên nội dung">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label text-right">Độ ưu tiên</label>
                                <div class="col-sm-6">
                                    <input type="number" name="priority" value="{{$block->priority}}" class="form-control"
                                           disabled
                                           placeholder="Nhập độ ưu tiên">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <x-switch-box
                                        type="long"
                                        title="Trạng thái"
                                        name="is_active"
                                        checked="{{$block->is_active ? 'true' : 'false'}}"/>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($block->type == 'slider')
                @include('admin.blocks.slider-attribute', ['block' => $block, 'isEdit' => false])
            @else
                @include('admin.blocks.banner-attribute', ['block' => $block, 'isEdit' => false])
            @endif
        </form>
    </div>
    <!-- Page-body end -->
@endsection
@section('javascript')
    <script type="text/javascript">
        $('table tbody tr').each(function () {
            if ($(this).find('.checkbox-item:checked').length == 4) {
                $(this).find('.check-all').prop('checked', true);
            }
        });

        $(document).on('click', '.check-all', function () {
            if ($(this).is(':checked')) {
                $(this).closest('tr').find('.checkbox-item').prop('checked', true);
            } else {
                $(this).closest('tr').find('.checkbox-item').prop('checked', false);
            }
        });

        $(document).on('click', '.checkbox-item', function () {
            let check_all = $(this).closest('tr').find('.check-all');

            if (!$(this).is(':checked')) {
                check_all.is(':checked') && check_all.prop('checked', false);
            } else {
                let count_checked = $(this).closest('tr').find('.checkbox-item:checked').length;
                count_checked == 4 && check_all.prop('checked', true);
            }
        })
    </script>


    <script type="text/javascript">
        $(document).ready(function () {
            let count = 0;
            bookSearchBox = function () {
                var self = $(this);
                var nameValue = $(this).data('name');
                var parent = $(this).closest('.book-searchbox');
                $(this).autocomplete({
                    minLength: 3,
                    delay: 500,
                    source: function (request, response) {
                        var listIds = parent.find('.id').map(function () {
                            return $(this).val();
                        }).get();

                        var listIdsParam = '';
                        if (listIds.length) {
                            listIdsParam = '&ids=' + listIds.join(',');
                        }
                        var key = request.term + listIdsParam;
                        let url = "/admin/products/search/"
                        // $.get(url, {
                        //     key: key
                        // }, function(response){
                        //     console.log(response);
                        // });
                        $.getJSON(url, function (data) {
                            console.log(data);
                            if (data.error == 0) {
                                let products = data.data;
                                var itemList = $.map(products, function (value, key) {
                                    return {
                                        id: value.id,
                                        name: value.name,
                                        thumbnail: value.thumbnail,
                                        price: value.price,
                                        class: 'product-item-search'
                                    };
                                });
                            }

                            response(itemList);
                        }).fail(function () {
                            response([]);
                        });
                    },
                    select: function (event, ui) {
                        var html = renderBook(ui.item, count);
                        parent.find('table').prepend(html);
                        self.val('');
                        if (parent.find("table").css('display') == 'none') {
                            parent.find("table").css('display', 'table');
                        }

                        $('#has-debt').attr({disabled: false});

                        count++;
                        handlePrice();
                    }
                }).autocomplete("instance")._renderItem = function (ul, item) {
                    return $("<li class='search-item'>")
                        .append(renderProductSearchItem(item))
                        .appendTo(ul);
                };
            }

            function renderProductSearchItem(item) {
                return `<a class='product-search-box'>
                    <span class='product-name'> ${item.name}</span>
                </a>`;
            }

            function renderBook(book, count) {
                return `<tr>
                <input class="id" value="${book.id}" name="books[${count}][id]" type="hidden"/>
                 <td>
                    <img class="gallery-img small-square" width="80" src="${book.thumbnail}"/>
                 </td>
                <td>
                    ${book.name}<br>
                 </td>
                <td class="product-price">
                    <span>${parseInt(book.price).toLocaleString('en-US')}</span>
                </td>
                <td><i class="feather icon-trash-2 remove-product-searchbox text-danger"></i></td>
            </tr>`;
            }

            $(document).on("click", ".remove-product-searchbox", function () {
                if (confirm('Bạn có muốn xóa dòng này?')) {
                    $(this).closest("tr").remove();
                    handlePrice();
                }
            });

            function format_price(price) {
                return `${parseInt(price).toLocaleString('en-US')}`;
            }

            function handlePrice() {
                let qty = price = total = totalMoney = totalFinal = discount = 0;
                $('.table-products tbody tr').each(function (index, item) {
                    qty = $(this).find('.product-qty input').val();
                    discountInput = $(this).find('.product-discount input').val();
                    price = $(this).find('.product-price input').val();

                    if (price == '') {
                        $(this).find('.product-price input').val(0);
                    }

                    if (discountInput == '') {
                        $(this).find('.product-discount input').val(0);
                    }

                    price = price ? price.replaceAll(',', '') : 0;
                    let itemPrice = parseInt(price) - (parseInt(price) * discountInput / 100);
                    total = parseInt(qty) * itemPrice;
                    $(this).find('.product-total span').text(encodeCurrencyFormat(total));
                    totalMoney += total;
                });

                let customerPay = $('#customer-pay').val();

                if ($('#discount').val() == '') {
                    discount = 0;
                } else {
                    discount = $('#discount').val();
                }

                if (discount > totalMoney) {
                    $('#discount').val(totalMoney);
                    discount = totalMoney;
                }

                if (discount > 0 && discount <= 100) {
                    totalFinal = totalMoney - (totalMoney * discount / 100);
                    $('#discount-note').text(`Giảm giá ${discount}%`);
                } else {
                    totalFinal = totalMoney - discount;
                    if (discount > 0) $('#discount-note').text(`Giảm giá ${encodeCurrencyFormat(discount)} đ`);
                }

                $('#total-money').val(encodeCurrencyFormat(totalMoney));
                $('#total-final').val(encodeCurrencyFormat(totalFinal));
                $('#customer-pay').val(encodeCurrencyFormat(totalFinal));

                if ($('#has-debt').is(':checked')) {
                    $('#debt').val(encodeCurrencyFormat(totalFinal));
                    $('#customer-pay').val(0);
                }
            }
        });
    </script>
@endsection
