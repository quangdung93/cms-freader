
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-block">
                <div class="row sub-title">
                    <div class="col-sm-6">
                        <span>Thuộc tính</span>
                    </div>
                    <div class="col-sm-6">
                        @if($isEdit)
                            <div class="text-right mb-3">
                                <span id="btn-add-banner-attribute" class="btn btn-primary"><i
                                        class="feather icon-plus"></i>Thêm</span>
                            </div>
                        @endif
                    </div>

                </div>

                <div class="form-group row mb-0">
                    <div class="col-sm-12">
                        <table id="banner-table" class="table table-products table-bordered w-100">
                            {{--                            @if (empty($products)) style="display: none" @endif>--}}
                            <thead>
                            <tr>
                                <th>Giá trị</th>
                                <th>Độ ưu tiên</th>
                                <th>Trạng thái</th>
                                @if($isEdit)
                                    <th></th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($block))
                                @foreach($block->block_items as $index => $item)
                                    <tr id="attribute_{{$index}}">
                                        <td style="width: 70%">
                                            @if($item['type'] == "text")
                                                <input
                                                    class="form-control w-100"
                                                    type="text"  name="data[]"
                                                    value="{{ $item['data'] }}"
                                                    {{$isEdit ? "" : "disabled"}}
                                                />
                                            @elseif($item['type'] == "int")
                                                <input
                                                    class="form-control w-100"
                                                    type="number"
                                                    name="data[]"
                                                    value="{{ $item['data'] }}"
                                                    {{$isEdit ? "" : "disabled"}}
                                                />
                                            @else
                                                <textarea rows="9" cols="50"
                                                    class="form-control"
                                                    name="data[]"
                                                    >{{ $item['data'] }}
                                                    </textarea>
                                            @endif
                                        </td>
                                        <td>
                                            <input
                                                class="form-control"
                                                type="number"
                                                name="priorities[]"
                                                value="{{ $item['priority'] }}"
                                                {{$isEdit ? "" : "disabled"}}
                                            />
                                        </td>
                                        <td><input
                                                id="status_{{$item['object_id']}}"
                                                name="status[]"
                                                type="checkbox"
                                                value="{{$item['is_active']}}"
                                                class="comment-status"
                                                @if($item['is_active'])
                                                    checked
                                                @endif
                                                @if(!$isEdit)
                                                    disabled
                                                @endif
                                            /></td>
                                        @if($isEdit)
                                            <td><a href=""
                                                   object-id="{{$index}}"
                                                   class="btn btn-danger custom-notify-confirm"
                                                   title="Xóa">
                                                    <i class="feather icon-trash-2"></i>
                                                </a></td>
                                        @endif
                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
