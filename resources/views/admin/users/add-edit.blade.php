@extends('admin.body')
@php
    $pageName = 'Tài khoản';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="panel-body">
            <form class="form-horizontal" action="{{url($routeName)}}" method="POST" role="form"
                enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Thông tin {{ $pageName }}</h4>
                                <x-input type="text" title="Tên người dùng" name="name" value="{{ isset($user) ? $user->name : ''  }}"/>
                                <x-input type="text" title="Tên đăng nhập" name="username" value="{{ isset($user) ? $user->username : ''  }}"/>
                                <x-input type="text" title="Email" name="email" value="{{ isset($user) ? $user->email : ''  }}"/>
                                <x-input type="text" title="Điện thoại" name="phone" value="{{ isset($user) ? $user->phone : ''  }}"/>
                                {{-- <x-input type="text" title="Ngày sinh" name="birthday" value="{{ isset($user) ? format_date($user->birthday) : ''  }}"/> --}}
                                <x-textarea type="" title="Địa chỉ" name="address" value="{{ $user->address ?? ''  }}" />
                                <x-input type="password" title="Mật khẩu" name="password" value=""/>
                                
                                @if(isAdmin())
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label text-right">Nhóm quyền</label>
                                        <div class="col-sm-9">
                                            <select class="form-control populate select2" name="roles[]" data-placeholder="Chọn nhóm quyền" multiple>
                                                @if(isset($roles))
                                                    @foreach($roles as $key => $role)
                                                        <option value="{{ $role->name }}" {{ (isset($user) && in_array($role->name, $user->roles->pluck('name')->toArray())) ? 'selected' : '' }}>
                                                            {{ $role->display_name }}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            @if ($errors->has('roles'))
                                                <div class="text-danger mt-2">{{ $errors->first('roles') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                                {{-- <div class="form-group row">
                                    <label class="col-sm-3 text-right">Giới tính</label>
                                    <div class="col-sm-9">
                                        <input type="radio" id="gender-male" name="gender" value="0" {{ isset($user) ? ($user->gender == 0 ? 'checked' : '') : 'checked' }}>&nbsp;<label for="gender-male">Nam</label>&nbsp;&nbsp;&nbsp;
                                        <input type="radio" id="gender-famale" name="gender" value="1" {{(isset($user) && $user->gender == 1) ? 'checked' : '' }}>&nbsp;<label for="gender-famale">Nữ</label>
                                    </div>
                                </div> --}}
                                <x-switch-box 
                                    type="long" 
                                    title="Kích hoạt" 
                                    name="status" 
                                    checked="{{ !isset($user) ? 'true' : ($user->status ? 'true' : '') }}"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Ảnh đại diện</h4>
                                <x-upload-file 
                                type="short"
                                title="Ảnh đại diện" 
                                name="input_file"
                                image="{{ isset($user) ? $user->avatar : '' }}"
                                width="100%"/>
                            </div>
                        </div>
                    </div>
                </div>
                <x-submit-button :route="$routeName"/>
            </form>
        </div>
    </div>
<!-- Page-body end -->
@endsection