@extends('admin.body')
@php
    $pageName = 'Đơn hàng';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="col-sm-12">
            @can('add_orders')
                <div class="text-right mb-3">
                    <a href="{{url($routeName.'/create')}}" class="btn btn-primary"><i
                            class="feather icon-shopping-cart"></i> Bán hàng</a>
                </div>
            @endcan
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Tìm kiếm</h4>
                        <form id="ajax-search">
                        <div class="row">
                                <div class="col-sm-3">
                                    <input class="form-control" name="code" placeholder="Nhập mã đơn hàng"/>
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control populate select2" name="status">
                                        <option value="">Chọn trạng thái</option>
                                        <option value="new">Mới</option>
                                        <option value="paid">Đã thanh toán</option>
                                        <option value="finish">Hoàn thành</option>
                                        <option value="cancel">Huỷ</option>
                                   </select>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-success mt-1">
                                        <span><i class="feather icon-search"></i></span>
                                        Tìm kiếm
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">{{ $pageName }}</h4>
                        <div class="dt-responsive table-responsive">
                            <table id="datatable" class="table stableweb-table center w100">
                                <thead>
                                    <tr>
                                        <th>Mã đơn</th>
                                        <th>Thông tin</th>
                                        <th>Trạng thái</th>
                                        <th>Tổng tiền</th>
                                        <th>Thao tác</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-body end -->

    <div class="modal fade" id="modal-order-detail">
        <div class="modal-dialog" role="document" style="max-width: 80%">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Chi tiết đơn hàng <span id="order-code"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        const ajax_url = "{!! route('orders.view') !!}";
        var columns = [
            { data: 'id', name: 'id', orderable: false, searchable: false},
            { data: 'info',name: 'info',width: '25%'},
            { data: 'status',name: 'status', width: '16%'},
            // { data: 'total_qty',name: 'total_qty', className: 'nowrap'},
            { data: 'total_money',name: 'total_money'},
            { data: 'action',orderable: false, searchable: false, className: 'nowrap'}
        ];

        showDataTableServerSide($('#datatable'), ajax_url, columns);

        // Cập nhật trạng thái đơn hàng
        $(document).on('change', '.order-status', function(){
            let orderId = $(this).data('id');
            let statusId = $(this).find(':selected').val();

            if(!orderId || !statusId){
                return;
            }

            // $.ajax({
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     },
            //     method: 'POST',
            //     url: '',
            //     data: {order_id: orderId, status: statusId},
            //     success: function (response) {
            //         if (response.error == 0) {
            //             pushNotify('Cập nhật đơn hàng thành công!');
            //             setTimeout(() => {
            //                 window.location.reload(); 
            //             }, 500);
            //         }
            //     }
            // });
        });

        //Cập nhật xác nhận admin
        $(document).on('change', '.order-admin-status', function(){
            let orderId = $(this).data('id');
            let statusId = $(this).find(':selected').val();

            if(!orderId || !statusId){
                return;
            }

            // $.ajax({
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     },
            //     method: 'POST',
            //     url: '',
            //     data: {order_id: orderId, admin_status: statusId},
            //     success: function (response) {
            //         if (response.error == 0) {
            //             pushNotify('Cập nhật đơn hàng thành công!');
            //             setTimeout(() => {
            //                 window.location.reload(); 
            //             }, 500);
            //         }
            //     }
            // });
        });
        $(document).on('click', '.btn-detail-order', function(){
            let order_id = $(this).data('id');
            let order_code = $(this).data('code');
            console.log(order_id);
        });
        function loadingTaskStatus(orderDetailId){
            $.get(`${URL_MAIN}admin/orders/checkTask/${orderDetailId}`, {}, function(resDetail){
                let content = '';
                if(resDetail.status == "new"){
                    content+= `<label class="label label-primary">Mới</label>`
                }else if(resDetail.status == "processing"){
                    content+= `<a href="${resDetail['url']}"><label class="label label-success">Đang xử lý</label></a>`
                }else if(resDetail.status == "finish"){
                    content+= `<label class="label label-info">Thành công</label>`

                }
                $("#task-" + orderDetailId).html(content);
            })
        }
        $(document).on('click', '.btn-restart-book', function(){
            let restartId = $(this).closest("tr").data("order-detail-id");
            if(confirm("Bạn có chắc cấp phát lại DRM cho cuốn sách này chứ?")){
                $(this).prop('disabled', true);
                $.ajax({
                    method : "POST", 
                    url : `${URL_MAIN}admin/orders/enqueueTask`,
                    data : {
                        "order_detail_id" : restartId
                    },
                    success:(function(res){
                        if(res.error){
                            alert(res.error);
                        }else if(res.status == 'success'){
                            alert("Đã khởi động lại cấp phát sách");
                            loadingTaskStatus(restartId);
                        }
                    }).bind(this)
                })
            }
        });
        $(document).on('click', '.btn-detail-order', function(){
            let order_id = $(this).data('id');
            let order_code = $(this).data('code');

            //Reset modal
            $('#modal-order-detail .modal-body').html('');

            if(!order_id){
                return;
            }

            $.get(`${URL_MAIN}admin/orders/detail/${order_id}?type=order`, {}, function(response){
                if(response.error == 0){
                    $('#order-code').text(order_code);
                    $('#modal-order-detail .modal-body').html(response.data);
                    $('.select2-modal').each(function() { 
                        $(this).select2({ dropdownParent: $(this).parent()});
                    });
                    // processing load task ->
                    $(".table-order-detail tr").toArray().forEach((value, i) =>{
                        if(i == 0) return;
                        let orderDetailId = $(value).data("order-detail-id");
                        loadingTaskStatus(orderDetailId);
                        
                    });
                }
            });
        });

        const URL_LIST = "{!! route('order.list') !!}";
        $(document).on('submit', '#ajax-search', function(e){
            e.preventDefault();

            let self = $(this);
            let url = `${URL_LIST}?${$(this).serialize()}`;

            showDataTableServerSide($('#datatable'), url, columns);
        });
    });
</script>
@endsection