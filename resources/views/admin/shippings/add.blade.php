@extends('admin.body')
@php
    $pageName = 'Vận chuyển';
    $routeName = getCurrentSlug();
@endphp
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets\icon\icofont\css\icofont.css')}}">
@endsection
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <form id="frm-shipping" data-action="{{ route('shipping.add', ['id' => $order->id]) }}">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-block">
                            <h4 class="sub-title">Thông tin đơn vị vận chuyển</h4>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">Đơn vị vận chuyển</label>
                                <div class="col-sm-8">
                                    <select name="shipping_parner_id" class="form-control select2-custom">
                                        @foreach ($shippingPartners as $partner)
                                            <option value="{{ $partner['id'] }}" data-image="{{ asset($partner['logo']) }}">
                                                {{ $partner['name'] }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <div class="error text-danger mt-2"></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">Địa chỉ lấy hàng</label>
                                <div class="col-sm-8">
                                    <select name="pick_address_id" class="form-control select2-pick-address">
                                        @foreach ($listPicking as $item)
                                            <option value="{{ $item['pick_address_id'] }}">{{ $item['pick_name'] }} - {{ $item['pick_tel'] }} - {{ $item['address'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">Số nhà, tên đường</label>
                                <div class="col-sm-8 col-form-label">
                                    <textarea class="form-control prevent-event" name="pick_address">72/1B Huỳnh Văn Bánh</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">SĐT lấy hàng</label>
                                <div class="col-sm-8 col-form-label">
                                    <input type="text" class="form-control" name="pick_tel" value="0933999486" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right">Ngày lấy hàng:</label>
                                <div class="col-sm-8">
                                    <div class='input-group date datetime-pick'>
                                        <input type='text' class="form-control" name="pick_date" value="{{ format_date(\Carbon\Carbon::now()) }}"/>
                                        <span class="input-group-addon bg-primary">
                                        <span class="feather icon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-block">
                            <h4 class="sub-title">Thông tin sản phẩm</h4>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">Tổng khối lượng (kg)</label>
                                <div class="col-sm-8 col-form-label">
                                    <input type="text" class="form-control" name="total_weight" value="0.1" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">Giá trị hàng</label>
                                <div class="col-sm-8 col-form-label">
                                    <input type="text" 
                                        value="{{ format_price($order->total_price) }}" 
                                        name="value" 
                                        class="form-control input-price" 
                                        onkeyup="this.value=formatMoney(this.value)" 
                                        onclick="this.select()"
                                        placeholder="Nhập giá trị hàng"
                                        autocomplete="off" />
                                    <div class="alert-success mt-2" role="alert">
                                        (*) Giá trị đóng bảo hiểm, là căn cứ tính phí bảo hiểm và bồi thường khi có sự cố.
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">Thu tiền COD</label>
                                <div class="col-sm-8 col-form-label">
                                    <input type="text" 
                                        value="{{ format_price($order->total_price) }}"
                                        name="pick_money" 
                                        class="form-control input-price" 
                                        onkeyup="this.value=formatMoney(this.value)" 
                                        onclick="this.select()"
                                        placeholder="Nhập số tiền cần thu COD"
                                        autocomplete="off" />
                                    <div class="alert-success mt-2" role="alert">
                                        (*) Số tiền CoD. Nếu bằng 0 thì không thu tiền CoD. Tính theo VNĐ
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">Mã đơn hàng riêng</label>
                                <div class="col-sm-8 col-form-label">
                                    <input type="text" class="form-control" name="order_private_id" value="" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <div class="checkbox-zoom zoom-primary m-0">
                                        <label>
                                            <input type="checkbox" value="1" name="is_freeship" checked class="check-all">
                                            <span class="cr">
                                                <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                            </span>
                                            <span>Miễn phí ship</span>
                                        </label>
                                    </div>
                                    <div class="alert-success mt-2" role="alert">
                                        (*) Freeship cho người nhận hàng. Nếu chọn miễn phí ship COD sẽ chỉ thu người nhận hàng số tiền bằng "Thu tiền COD", nếu không chọn COD sẽ thu tiền người nhận số tiền bằng "Thu tiền COD" + phí ship của đơn hàng.
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="checkbox-zoom zoom-primary m-0">
                                        <label>
                                            <input type="checkbox" value="1" name="tags[]" checked class="check-all">
                                            <span class="cr">
                                                <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                            </span>
                                            <span>Hàng dễ vỡ <span class="alert-success"> (+ 20% phí dịch vụ)</span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="checkbox-zoom zoom-primary m-0">
                                        <label>
                                            <input type="checkbox" value="1" name="" checked class="check-all">
                                            <span class="cr">
                                                <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                            </span>
                                            <span>Hàng giá trị cao</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <div class="checkbox-zoom zoom-primary m-0">
                                        <label>
                                            <input type="checkbox" value="20" name="tags[]" checked class="check-all">
                                            <span class="cr">
                                                <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                            </span>
                                            <span>Hàng nguyên hộp <span class="alert-success"> (Phí 1.000 đ)</span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="checkbox-zoom zoom-primary m-0">
                                        <label>
                                            <input type="checkbox" value="10" name="tags[]" checked class="check-all">
                                            <span class="cr">
                                                <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                            </span>
                                            <span>Cho xem hàng <span class="alert-success"> (Miễn phí)</span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-block">
                            <h4 class="sub-title">Thông tin đơn hàng</h4>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">Tên khách hàng</label>
                                <div class="col-sm-8 col-form-label">{{ $customer->name }}</div>
                                
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">Số điện thoại</label>
                                <div class="col-sm-8 col-form-label">{{ $customer->phone }}</div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">Email</label>
                                <div class="col-sm-8 col-form-label">{{ $customer->email }}</div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">Ghi chú</label>
                                <div class="col-sm-8 col-form-label">{{ $order->notes }}</div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">Tổng sản phẩm</label>
                                <div class="col-sm-8 col-form-label">{{ $order->total_quantity }}</div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">Tổng tiền</label>
                                <div class="col-sm-8 col-form-label">{{ number_format($order->total_price) }} đ</div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">Trạng thái</label>
                                <div class="col-sm-8 col-form-label">{{ renderOrderStatus($order->status) }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-block">
                            <h4 class="sub-title">Địa chỉ giao hàng</h4>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">Tỉnh/thành phố</label>
                                <div class="col-sm-8">
                                    <div class="checkout-select">
                                        <select id="provinces" data-id="{{ isset($customer) ? $customer->province_id : '' }}" class="form-control select2">
                                            <option value="0">Chọn tỉnh/thành phố</option>
                                            @foreach($provinces as $key => $value)
                                                <option value="{{ $value->id }}" {{ (isset($customer) && $customer->province_id == $value->id) ? 'selected' : '' }}>{{ $value->name }}</option>
                                            @endforeach
                                        </select>
                                        <input type="hidden" id="shipping_province" name="shipping_province" value="{{ optional($customer->province)->name }}"/>
                                        <div class="error text-danger mt-2"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">Quận/huyện</label>
                                <div class="col-sm-8">
                                    <div class="checkout-select">
                                        <select id="districts" data-id="{{ isset($customer) ? $customer->district_id : '' }}" class="form-control select2">
                                            <option value="0">Chọn quận/huyện</option>
                                        </select>
                                        <input type="hidden" id="shipping_district" name="shipping_district" value="{{ optional($customer->district)->name }}"/>
                                        <div class="error text-danger mt-2"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">Phường/xã</label>
                                <div class="col-sm-8">
                                    <div class="checkout-select">
                                        <select id="wards" data-id="{{ isset($customer) ? $customer->ward_id : '' }}" class="form-control select2">
                                            <option value="0">Chọn phường/xã</option>
                                        </select>
                                        <input type="hidden" id="shipping_ward" name="shipping_ward" value="{{ optional($customer->ward)->name }}"/>
                                        <div class="error text-danger mt-2"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">Số nhà, tên đường</label>
                                <div class="col-sm-8 col-form-label">
                                    <textarea type="text" class="form-control" name="shipping_address">{{ $customer->address }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">SĐT nhận hàng</label>
                                <div class="col-sm-8 col-form-label">
                                    <input type="text" class="form-control" name="shipping_phone" value="{{ $customer->phone }}" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-right font-weight-bold">Ghi chú (nếu có)</label>
                                <div class="col-sm-8 col-form-label">
                                    <textarea type="text" class="form-control" name="note"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-block">
                            <h4 class="sub-title">Chi tiết đơn hàng</h4>
                            <div class="dt-responsive table-responsive">
                                <table class="table stableweb-table center w100">
                                    <thead>
                                        <tr>
                                            <th>Mã sản phẩm</th>
                                            <th>Sản phẩm</th>
                                            <th>Trọng lượng (kg)</th>
                                            <th class="text-right">Đơn giá</th>
                                            <th>Số lượng</th>
                                            <th>Thành tiền</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if($order->detail)
                                            @foreach($order->detail as $row)
                                                <tr>
                                                    <td class="text-left">{{ $row->code }}</td>
                                                    <td class="text-left"><span>{{$row->name}}</span></td>
                                                    <td>{{ $row->weight ?: 'n/a' }}</td>
                                                    <td class="text-right"> {{ number_format($row->pivot->price) }} đ</td>
                                                    <td> {{$row->pivot->qty}} </td>
                                                    <td class="text-right">{{ number_format($row->pivot->subtotal) }} đ</td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td class="text-right" colspan="6">
                                                    <span class="font-weight-bold">Tổng tiền: </span>
                                                    <span class="font-weight-bold text-danger">{{ number_format($order->total_price) }} đ</span>
                                                </td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @can('add_shippings')
                        <div class="text-center">
                            <a class="btn btn-warning" href="{{url('admin/orders')}}"><span><i class="feather icon-chevrons-left"></i></span>Hủy</a>
                            @if($shipping)
                                @if($shipping->status_id == -1)
                                <button type="submit" id="submit-shipping" class="btn btn-primary"><i class="feather icon-navigation"></i> Gửi đơn hàng</button>
                                @endif
                            @else
                            <button type="submit" id="submit-shipping" class="btn btn-primary"><i class="feather icon-navigation"></i> Gửi đơn hàng</button>
                            @endif
                        </div>
                    @endcan
                </div>
            </div>
        </form>
    </div>
    <!-- Page-body end -->
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        $('.datetime-pick').datepicker({format: 'dd/mm/yyyy'});
        $('.select2-pick-address').select2();

        $(".select2-custom").select2({
            templateResult: formatState,
            templateSelection: formatState
        });

        function formatState (opt) {
            if (!opt.id) {
                return opt.text.toUpperCase();
            } 

            var optimage = $(opt.element).attr('data-image'); 
            console.log(optimage)
            if(!optimage){
                return opt.text.toUpperCase();
            } else {                    
                var $opt = $(
                '<span><img width="20" style="margin-right: 10px" src="' + optimage + '" width="60px" /> ' + opt.text.toUpperCase() + '</span>'
                );
                return $opt;
            }
        };

        //Handle Address
        let activeProvincesId = $('#provinces').data('id');

        if(activeProvincesId){
            loadDistrict(activeProvincesId);
        }
            
        $(document).on('change', '#provinces', function () {
            const province_id = this.value;
            const text = $("#provinces option:selected").text();
            if (!province_id) {
                return false;
            }

            $('#shipping_province').val(text);
            loadDistrict(province_id);
        });

        $(document).on('change', '#districts', function () {
            const district_id = this.value;
            const text = $("#districts option:selected").text();
            if (!district_id) {
                return false;
            }

            $('#shipping_district').val(text);
            loadWards(district_id);
        });

        $(document).on('change', '#wards', function () {
            const text = $("#wards option:selected").text();
            $('#shipping_ward').val(text);
        });

        function loadDistrict(province_id){
            let activeDistrictId = $('#districts').data('id');
            $.get('/province/' + province_id, function (result) {
                if (result.error == 0) {
                    $('#districts').empty();
                    $('#districts').append(result.data);
                    $("#districts").trigger("chosen:updated");

                    if(activeDistrictId){
                        $('#districts').val(activeDistrictId).trigger('change');
                        $('#districts').data('id', "");
                    }

                } else {
                    alert(result.message.title);
                }
            });
        }

        function loadWards(district_id){
            let activeWardId = $('#wards').data('id');
            $.get('/district/' + district_id, function (result) {
                if (result.error == 0) {
                    $('#wards').empty();
                    $('#wards').append(result.data);
                    $("#wards").trigger("chosen:updated");

                    if(activeWardId){
                        $('#wards').val(activeWardId).trigger('change');
                        $('#wards').data('id', "");
                    }
                } else {
                    alert(result.message.title);
                }
            });
        }

        //End Handle Address

        $(document).on('submit', '#frm-shipping', function(e){
            e.preventDefault();
            const self = $(this);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: $(this).data('action'),
                data: $(this).serialize(),
                success: function (response) {
                    if (response.status) {
                        pushNotify(response.message.title, '', type = 'success');
                        $('#submit-shipping').addClass('prevent-event');
                        // self[0].reset();
                    }
                    else{
                        pushNotify(response.message.title, '', type = 'warning');
                    }
                }
            });
        });
    });
</script>
@endsection