@extends('admin.body')
@php
    $pageName = 'Roles';
    $routeName = getCurrentSlug();
@endphp
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets\icon\icofont\css\icofont.css')}}">
@endsection
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body panels-wells">
        <form class="form-horizontal" method="post"
                            action="{{url($routeName)}}" role="form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Thông tin {{ $pageName }}</h4>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-right">Tên {{ $pageName }}</label>
                            <div class="col-sm-6 prevent-event">
                                <input type="text" name="name" value="{{ $role->name }}" class="form-control" placeholder="Nhập {{ $pageName }}" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-right">Tên hiển thị</label>
                            <div class="col-sm-6">
                                <input type="text" name="display_name" value="{{ $role->display_name }}" class="form-control" placeholder="Nhập tên hiển thị" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-right">Chọn nhóm quyền</label>
                            <div class="col-sm-6">
                                <select class="form-control populate select2" name="permission_groups[]" data-placeholder="Chọn nhóm quyền" multiple>
                                    @if(isset($permissionGroups))
                                        @foreach($permissionGroups as $key => $permissionGroup)
                                            <option value="{{ $permissionGroup->id }}" {{ in_array($permissionGroup->id, $role->permissionGroups->pluck('id')->toArray()) ? 'selected' : '' }}>
                                                {{ $permissionGroup->group_name }}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('roles'))
                                    <div class="text-danger mt-2">{{ $errors->first('roles') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Phân quyền chức năng</h4>
                        <div class="form-group row">
                            <div class="col-sm-12 mt-2">
                                <div class="dt-responsive table-responsive">
                                    <table class="table stableweb-table center w100">
                                        <thead>
                                            <tr>
                                                <th class="text-left">Tên quyền</th>
                                                <th>Nhóm chức năng</th>
                                                <th>Áp dụng</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($permissions as $key => $permission)
                                                <tr>
                                                    <td class="text-capitalize text-left">{{ $permission['display_name'] }}</td>
                                                    <td class="text-capitalize">Quản lý {{ $permission->feature->feature_name }}</td>
                                                    <td class="text-center {{ in_array($permission->id, $permissionIdsByChooseGroup) ? "prevent-event" : '' }}">
                                                        <div class="checkbox-zoom zoom-primary m-0">
                                                            <label>
                                                                <input 
                                                                    type="checkbox" 
                                                                    name="permission[{{ $permission->id }}]" 
                                                                    id="{{ $permission->name }}" 
                                                                    class="checkbox-item"  
                                                                    {{ $role->permissions->pluck('id')->contains($permission->id) ? 'checked' : '' }}
                                                                >
                                                                <span class="cr">
                                                                    <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <x-submit-button :route="$routeName"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
    <!-- Page-body end -->
@endsection
@section('javascript')
<script type="text/javascript">
    $('table tbody tr').each(function(){
        if($(this).find('.checkbox-item:checked').length == 4){
            $(this).find('.check-all').prop('checked',true);
        }
    });

    $(document).on('click', '.check-all', function(){
        if($(this).is(':checked')){
            $(this).closest('tr').find('.checkbox-item').prop('checked',true);
        }
        else{
            $(this).closest('tr').find('.checkbox-item').prop('checked',false);
        }
    });

    $(document).on('click', '.checkbox-item', function(){
        let check_all = $(this).closest('tr').find('.check-all');

        if(!$(this).is(':checked')){
            check_all.is(':checked') && check_all.prop('checked',false);
        }
        else{
            let count_checked = $(this).closest('tr').find('.checkbox-item:checked').length;
            count_checked == 4 && check_all.prop('checked',true);
        }
    })
</script>
@endsection