@extends('admin.body')
@php
    $pageName = 'Cấu hình';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="row mb-3">
            <div class="col-sm-12">
                @role(config('permission.role_dev'))
                    <a href="#" id="add-setting" class="btn btn-primary"><i class="feather icon-plus-circle"></i> Thêm cấu hình</a>
                @endrole
                {{-- <a href="{{ route('cache.clear') }}" class="btn btn-danger"><i class="feather icon-refresh-cw"></i> Xóa cache</a> --}}
            </div>
        </div>

        <div class="panel-body">
            <form class="form-horizontal" action="{{url($routeName)}}" method="POST" role="form"
                enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @php
                    $groupSetting = $settings->groupBy('group');
                @endphp

                <ul class="nav nav-tabs md-tabs" role="tablist">
                    @foreach($groupSetting as $group => $settings)
                    <li class="nav-item">
                        <a class="nav-link {{ $loop->iteration == 1 ? 'active' : '' }}" data-toggle="tab" href="#{{ $group }}{{ $loop->iteration }}" role="tab" aria-expanded="true">
                            <i class="feather icon-settings"></i> {{ $group }}
                        </a>
                    </li>
                    @endforeach
                </ul>

                <div class="tab-content card-block p-0">
                @foreach($groupSetting as $group => $settings)
                <div class="tab-pane {{ $loop->iteration == 1 ? 'active' : '' }}" id="{{ $group }}{{ $loop->iteration }}" role="tabpanel" aria-expanded="true">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-block">
                                    <h4 class="sub-title">{{ $pageName }} {{ $group }}</h4>
                                    <div class="sortable">
                                        @foreach($settings->sortBy('order') as $setting)
                                            <div class="item bg-white" data-id="{{ $setting->id }}">
                                                <label class="code mb-0">{{ $setting->key }}</label>
                                                @if($setting->type == 'string' || $setting->type == 'int')
                                                    <x-input 
                                                    type="text" 
                                                    :title="$setting->description" 
                                                    :name="$setting->key" 
                                                    value="{{ $setting->value ?? '' }}" />
                                                @elseif($setting->type == 'json')
                                                    <x-textarea 
                                                        type="" 
                                                        :title="$setting->description" 
                                                        :name="$setting->key" 
                                                        value="{{ $setting->value ?? ''  }}" 
                                                    />
                                                @elseif($setting->type == 'image')
                                                    <x-upload-file
                                                    type="long"
                                                    :title="$setting->description" 
                                                    :name="$setting->key"
                                                    image="{{ $setting->value ?? '' }}"
                                                    width="150px" />
                                                @endif
                                                @role(config('permission.role_dev'))
                                                    <div class="form-group row">
                                                        <div class="col-sm-3 text-right">
                                                            @if($group == 'storage')
                                                            <button class="btn btn-outline-success btn-sm c-pointer check-storage"><i class="feather icon-search"></i> Kiểm tra Rclone</button>
                                                            @endif
                                                            <label class="label label-info pointer-move"><i class="feather icon-move"></i></label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <a href="{{ route('settings.delete',['id' => $setting->id]) }}" class="text-danger notify-confirm remove-setting">
                                                                <i class="feather icon-trash-2"></i> Xóa
                                                            </a>
                                                        </div>
                                                    </div>
                                                    @if($group == 'storage')
                                                    <div class="form-group row">
                                                        <div class="col-sm-3 text-right result-rclone">
                                                        </div>
                                                    </div>
                                                    @endif
                                                    @endrole
                                                <hr>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                </div>
                <div class="form-group row mt-3">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-primary btn-label-left">
                            <span><i class="feather icon-save"></i></span>
                            Cập nhật cấu hình
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<!-- Page-body end -->

<div class="modal modal-info fade" tabindex="-1" id="setting-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="m_hd_add" class="modal-title"><i class="feather icon-plus-circle"></i> Thêm cấu hình</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Đóng"><span aria-hidden="true">&times;</span></button>
            </div>
            <form action="{{ route('settings.add') }}" id="m_form" method="POST">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Key</label>
                        <input type="text" value="{{ old('key') }}" class="form-control" id="m_title" name="key" placeholder="Nhập key" required>
                        @if ($errors->has('key'))
                            <div class="text-danger mt-2">{{ $errors->first('key') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="type">Loại cấu hình</label>
                        <select id="m_type" class="form-control" name="type">
                            {{-- <option value="text" selected="selected">Text</option>
                            <option value="textarea">Textarea</option>
                            <option value="image">Image</option>
                            <option value="checkbox">Checkbox</option>
                            <option value="selectbox">Selectbox</option> --}}
                            <option value="string" selected="selected">String</option>
                            <option value="int">Int</option>
                            <option value="json">Json</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Mô tả</label>
                        <textarea class="form-control" name="description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="group">Nhóm cấu hình</label>
                        <select id="m_group" class="form-control" name="group">
                            {{-- <option value="site" selected="selected">Website</option> --}}
                            <option value="system" selected="selected">System</option>
                            <option value="storage">Storage</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success pull-right" value="Lưu">
                    <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Hủy</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection

@section('styles')
<style>
.loading-icon {
    height: 12px;
    width: 12px;
    background-repeat: no-repeat;
    background-image: url(data:image/gif;base64,R0lGODlhIAAgAPUAAP///15eXvv7+9nZ2fDw8PX19eHh4a2trb+/v/j4+O7u7vz8/Lm5ubKysuzs7NHR0cLCwvLy8svLy+jo6IWFhZSUlJqamqysrMfHx/Pz84yMjKKiomVlZV5eXt/f39vb2+bm5nl5eZmZmXBwcI2NjczMzAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAIAAgAAAG/0CAcEgkFjgcR3HJJE4SxEGnMygKmkwJxRKdVocFBRRLfFAoj6GUOhQoFAVysULRjNdfQFghLxrODEJ4Qm5ifUUXZwQAgwBvEXIGBkUEZxuMXgAJb1dECWMABAcHDEpDEGcTBQMDBQtvcW0RbwuECKMHELEJF5NFCxm1AAt7cH4NuAOdcsURy0QCD7gYfcWgTQUQB6Zkr66HoeDCSwIF5ucFz3IC7O0CC6zx8YuHhW/3CvLyfPX4+OXozKnDssBdu3G/xIHTpGAgOUPrZimAJCfDPYfDin2TQ+xeBnWbHi37SC4YIYkQhdy7FvLdpwWvjA0JyU/ISyIx4xS6sgfkNS4me2rtVKkgw0JCb8YMZdjwqMQ2nIY8BbcUQNVCP7G4MQq1KRivR7tiDEuEFrggACH5BAkKAAAALAAAAAAgACAAAAb/QIBwSCQmNBpCcckkEgREA4ViKA6azM8BEZ1Wh6LOBls0HA5fgJQ6HHQ6InKRcWhA1d5hqMMpyIkOZw9Ca18Qbwd/RRhnfoUABRwdI3IESkQFZxB4bAdvV0YJQwkDAx9+bWcECQYGCQ5vFEQCEQoKC0ILHqUDBncCGA5LBiHCAAsFtgqoQwS8Aw64f8m2EXdFCxO8INPKomQCBgPMWAvL0n/ff+jYAu7vAuxy8O/myvfX8/f7/Arq+v0W0HMnr9zAeE0KJlQkJIGCfE0E+PtDq9qfDMogDkGmrIBCbNQUZIDosNq1kUsEZJBW0dY/b0ZsLViQIMFMW+RKKgjFzp4fNokPIdki+Y8JNVxA79jKwHAI0G9JGw5tCqDWTiFRhVhtmhVA16cMJTJ1OnVIMo1cy1KVI5NhEAAh+QQJCgAAACwAAAAAIAAgAAAG/0CAcEgkChqNQnHJJCYWRMfh4CgamkzFwBOdVocNCgNbJAwGhKGUOjRQKA1y8XOGAtZfgIWiSciJBWcTQnhCD28Qf0UgZwJ3XgAJGhQVcgKORmdXhRBvV0QMY0ILCgoRmIRnCQIODgIEbxtEJSMdHZ8AGaUKBXYLIEpFExZpAG62HRRFArsKfn8FIsgjiUwJu8FkJLYcB9lMCwUKqFgGHSJ5cnZ/uEULl/CX63/x8KTNu+RkzPj9zc/0/Cl4V0/APDIE6x0csrBJwybX9DFhBhCLgAilIvzRVUriKHGlev0JtyuDvmsZUZlcIiCDnYu7KsZ0UmrBggRP7n1DqcDJEzciOgHwcwTyZEUmIKEMFVIqgyIjpZ4tjdTxqRCMPYVMBYDV6tavUZ8yczpkKwBxHsVWtaqo5tMgACH5BAkKAAAALAAAAAAgACAAAAb/QIBwSCQuBgNBcck0FgvIQtHRZCYUGSJ0IB2WDo9qUaBQKIXbLsBxOJTExUh5mB4iDo0zXEhWJNBRQgZtA3tPZQsAdQINBwxwAnpCC2VSdQNtVEQSEkOUChGSVwoLCwUFpm0QRAMVFBQTQxllCqh0kkIECF0TG68UG2O0foYJDb8VYVa0alUXrxoQf1WmZnsTFA0EhgCJhrFMC5Hjkd57W0jpDsPDuFUDHfHyHRzstNN78PPxHOLk5dwcpBuoaYk5OAfhXHG3hAy+KgLkgNozqwzDbgWYJQyXsUwGXKNA6fnYMIO3iPeIpBwyqlSCBKUqEQk5E6YRmX2UdAT5kEnHKkQ5hXjkNqTPtKAARl1sIrGoxSFNuSEFMNWoVCxEpiqyRlQY165wEHELAgAh+QQJCgAAACwAAAAAIAAgAAAG/0CAcEgsKhSLonJJTBIFR0GxwFwmFJlnlAgaTKpFqEIqFJMBhcEABC5GjkPz0KN2tsvHBH4sJKgdd1NHSXILah9tAmdCC0dUcg5qVEQfiIxHEYtXSACKnWoGXAwHBwRDGUcKBXYFi0IJHmQEEKQHEGGpCnp3AiW1DKFWqZNgGKQNA65FCwV8bQQHJcRtds9MC4rZitVgCQbf4AYEubnKTAYU6eoUGuSpu3fo6+ka2NrbgQAE4eCmS9xVAOW7Yq7IgA4Hpi0R8EZBhDshOnTgcOtfM0cAlTigILFDiAFFNjk8k0GZgAxOBozouIHIOyKbFixIkECmIyIHOEiEWbPJTTQ5FxcVOMCgzUVCWwAcyZJvzy45ADYVZNIwTlIAVfNB7XRVDLxEWLQ4E9JsKq+rTdsMyhcEACH5BAkKAAAALAAAAAAgACAAAAb/QIBwSCwqFIuicklMEgVHQVHKVCYUmWeUWFAkqtOtEKqgAsgFcDFyHJLNmbZa6x2Lyd8595h8C48RagJmQgtHaX5XZUYKQ4YKEYSKfVKPaUMZHwMDeQBxh04ABYSFGU4JBpsDBmFHdXMLIKofBEyKCpdgspsOoUsLXaRLCQMgwky+YJ1FC4POg8lVAg7U1Q5drtnHSw4H3t8HDdnZy2Dd4N4Nzc/QeqLW1bnM7rXuV9tEBhQQ5UoCbJDmWKBAQcMDZNhwRVNCYANBChZYEbkVCZOwASEcCDFQ4SEDIq6WTVqQIMECBx06iCACQQPBiSabHDqzRUTKARMhSFCDrc+WNQIcOoRw5+ZIHj8ADqSEQBQAwKKLhIzowEEeGKQ0owIYkPKjHihZoBKi0KFE01b4zg7h4y4IACH5BAkKAAAALAAAAAAgACAAAAb/QIBwSCwqFIuicklMEgVHQVHKVCYUmWeUWFAkqtOtEKqgAsgFcDFyHJLNmbZa6x2Lyd8595h8C48RagJmQgtHaX5XZUUJeQCGChGEin1SkGlubEhDcYdOAAWEhRlOC12HYUd1eqeRokOKCphgrY5MpotqhgWfunqPt4PCg71gpgXIyWSqqq9MBQPR0tHMzM5L0NPSC8PCxVUCyeLX38+/AFfXRA4HA+pjmoFqCAcHDQa3rbxzBRD1BwgcMFIlidMrAxYICHHA4N8DIqpsUWJ3wAEBChQaEBnQoB6RRr0uARjQocMAAA0w4nMz4IOaU0lImkSngYKFc3ZWyTwJAALGK4fnNA3ZOaQCBQ22wPgRQlSIAYwSfkHJMrQkTyEbKFzFydQq15ccOAjUEwQAIfkECQoAAAAsAAAAACAAIAAABv9AgHBILCoUi6JySUwSBUdBUcpUJhSZZ5RYUCSq060QqqACyAVwMXIcks2ZtlrrHYvJ3zn3mHwLjxFqAmZCC0dpfldlRQl5AIYKEYSKfVKQaW5sSENxh04ABYSFGU4LXYdhR3V6p5GiQ4oKmGCtjkymi2qGBZ+6eo+3g8KDvYLDxKrJuXNkys6qr0zNygvHxL/V1sVD29K/AFfRRQUDDt1PmoFqHgPtBLetvMwG7QMes0KxkkIFIQNKDhBgKvCh3gQiqmxt6NDBAAEIEAgUOHCgBBEH9Yg06uWAIQUABihQMACgBEUHTRwoUEOBIcqQI880OIDgm5ABDA8IgUkSwAAyij1/jejAARPPIQwONBCnBAJDCEOOCnFA8cOvEh1CEJEqBMIBEDaLcA3LJIEGDe/0BAEAIfkECQoAAAAsAAAAACAAIAAABv9AgHBILCoUi6JySUwSBUdBUcpUJhSZZ5RYUCSq060QqqACyAVwMXIcks2ZtlrrHYvJ3zn3mHwLjxFqAmZCC0dpfldlRQl5AIYKEYSKfVKQaW5sSENxh04ABYSFGU4LXYdhR3V6p5GiQ4oKmGCtjkymi2qGBZ+6eo+3g8KDvYLDxKrJuXNkys6qr0zNygvHxL/V1sVDDti/BQccA8yrYBAjHR0jc53LRQYU6R0UBnO4RxmiG/IjJUIJFuoVKeCBigBN5QCk43BgFgMKFCYUGDAgFEUQRGIRYbCh2xACEDcAcHDgQDcQFGf9s7VkA0QCI0t2W0DRw68h8ChAEELSJE8xijBvVqCgIU9PjwA+UNzG5AHEB9xkDpk4QMGvARQsEDlKxMCALDeLcA0rqEEDlWCCAAAh+QQJCgAAACwAAAAAIAAgAAAG/0CAcEgsKhSLonJJTBIFR0FRylQmFJlnlFhQJKrTrRCqoALIBXAxchySzZm2Wusdi8nfOfeYfAuPEWoCZkILR2l+V2VFCXkAhgoRhIp9UpBpbmxIQ3GHTgAFhIUZTgtdh2FHdXqnkaJDigqYYK2OTKaLaoYFn7p6j0wOA8PEAw6/Z4PKUhwdzs8dEL9kqqrN0M7SetTVCsLFw8d6C8vKvUQEv+dVCRAaBnNQtkwPFRQUFXOduUoTG/cUNkyYg+tIBlEMAFYYMAaBuCekxmhaJeSeBgiOHhw4QECAAwcCLhGJRUQCg3RDCmyUVmBYmlOiGqmBsPGlyz9YkAlxsJEhqCubABS9AsPgQAMqLQfM0oTMwEZ4QpLOwvMLxAEEXIBG5aczqtaut4YNXRIEACH5BAkKAAAALAAAAAAgACAAAAb/QIBwSCwqFIuicklMEgVHQVHKVCYUmWeUWFAkqtOtEKqgAsgFcDFyHJLNmbZa6x2Lyd8595h8C48RahAQRQtHaX5XZUUJeQAGHR0jA0SKfVKGCmlubEhCBSGRHSQOQwVmQwsZTgtdh0UQHKIHm2quChGophuiJHO3jkwOFB2UaoYFTnMGegDKRQQG0tMGBM1nAtnaABoU3t8UD81kR+UK3eDe4nrk5grR1NLWegva9s9czfhVAgMNpWqgBGNigMGBAwzmxBGjhACEgwcgzAPTqlwGXQ8gMgAhZIGHWm5WjelUZ8jBBgPMTBgwIMGCRgsygVSkgMiHByD7DWDmx5WuMkZqDLCU4gfAq2sACrAEWFSRLjUfWDopCqDTNQIsJ1LF0yzDAA90UHV5eo0qUjB8mgUBACH5BAkKAAAALAAAAAAgACAAAAb/QIBwSCwqFIuickk0FIiCo6A4ZSoZnRBUSiwoEtYipNOBDKOKKgD9DBNHHU4brc4c3cUBeSOk949geEQUZA5rXABHEW4PD0UOZBSHaQAJiEMJgQATFBQVBkQHZKACUwtHbX0RR0mVFp0UFwRCBSQDSgsZrQteqEUPGrAQmmG9ChFqRAkMsBd4xsRLBBsUoG6nBa14E4IA2kUFDuLjDql4peilAA0H7e4H1udH8/Ps7+3xbmj0qOTj5mEWpEP3DUq3glYWOBgAcEmUaNI+DBjwAY+dS0USGJg4wABEXMYyJNvE8UOGISKVCNClah4xjg60WUKyINOCUwrMzVRARMGENWQ4n/jpNTKTm15J/CTK2e0MoD+UKmHEs4onVDVVmyqdpAbNR4cKTjqNSots07EjzzJh1S0IADsAAAAAAAAAAAA=);
    position: relative;
    display: inline-block;
    background-size: contain;
}
</style>
@endsection
@section('javascript')
<script>
    $(document).ready(function(){

        //Show popup when validate errors
        @if($errors->any())
            $('#setting-modal').modal('show');
        @endif
        $('.check-storage').click(async function(){
            let data  = $(this).closest('.item').find('textarea')[0].value;
            $(this).prop('disabled', true);
            $(this).find('i').addClass('loading-icon');
            $(this).find('i').removeClass('icon-search');
            try{
                $.ajax({
                    method : 'POST',
                    url : "{{ route('settings.rcloneCheck') }}",
                    data : {
                        config : JSON.parse(data)
                    },
                    headers: {
                        "cache-control" : "no-cache"
                    },
                    success : (function(e){
                        $(this).prop('disabled', false);
                        $(this).find('i').removeClass('loading-icon');
                        $(this).find('i').addClass('icon-search');

                        console.log(e);
                        if(e['status'] == 'success'){
                            alert('Kết nối thành công');

                        }else{
                            alert('Kết nối không thành công');
                        }
                    }).bind(this),
                    error : (function(){
                        $(this).prop('disabled', false);
                        $(this).find('i').removeClass('loading-icon');
                        $(this).find('i').addClass('icon-search');
                        alert("Kết nối không thành công");
                    }).bind(this)
                })
            }catch(err){
                console.log(err);
                alert('Không thể đọc cấu trúc');
            }
        })
        $('#add-setting').click(function() {
            $('#m_form').trigger('reset');
            $('#setting-modal').modal('show');
        });

        $('.sortable').each(function(){
            Sortable.create($(this)[0], {
                handle: ".feather.icon-move",
                animation: 0,
                onEnd: function (evt) {
                    var data_sort = evt.from;
                    let order = [];

                    $(data_sort).find('.item').each(function(){
                        order.push({id: $(this).data('id'), order: $(this).index() + 1});
                    });

                    $.post('{{ route('settings.order') }}', {
                        order: order,
                        _token: '{{ csrf_token() }}'
                    }, function (data) {
                        
                    });
                }
            })
        });

        $('.sortable .item').each(function(index, value) {
            $(this).find('.sequence').val(index + 1);
        });
        
    });
</script>
@endsection