@extends('admin.body')
@php
    $pageName = 'Sản phẩm';
    $routeName = getCurrentSlug();
@endphp
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="row ">
            <div class="col-sm-12">
                @can('add_books')
                    <div class="text-right mb-3">
                        <a href="{{url($routeName.'/create')}}" class="btn btn-primary"><i
                                class="feather icon-plus"></i> Thêm mới</a>
                    </div>
                @endcan
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Tìm kiếm</h4>
                        <form id="ajax-search">
                        <div class="row">
                                <div class="col-sm-3">
                                    <input class="form-control" name="name" placeholder="Nhập tên hoặc mã sản phẩm"/>
                                </div>
                                <!-- <div class="col-sm-3">
                                    <select class="form-control populate select2" name="brand_id">
                                        <option value="0">Chọn thương hiệu</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control populate select2" name="origin_id">
                                        <option value="0">Chọn xuất xứ</option>
                                    </select>
                                </div> -->
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-success mt-1">
                                        <span><i class="feather icon-search"></i></span>
                                        Tìm kiếm
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">{{ $pageName }}</h4>
                        <div class="dt-responsive table-responsive">
                            <table id="datatable" class="table stableweb-table w100">
                                <thead>
                                    <tr>
                                        <th>Mã sản phẩm</th>
                                        <th>Tên sản phẩm</th>
                                        <th>Đơn giá</th>
                                        <th>Trạng thái</th>
                                        <th>Thao tác</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-body end -->
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){
        const ajax_url = "{!! route('book.view') !!}";
        var columns = [
            // { data: 'image',name: 'image',orderable: false, searchable: false},
            { data: 'sku',name: 'code'},
            { data: 'name',name: 'name',width: '40%'},
            { data: 'price',name: 'price', className: 'nowrap'},
            { data: 'status',name: 'status', searchable: false, className: 'nowrap'},
            { data: 'action',orderable: false, searchable: false, className: 'nowrap'}
        ];

        showDataTableServerSide($('#datatable'), ajax_url, columns);

        const URL_LIST = "{!! route('book.list') !!}";
        $(document).on('submit', '#ajax-search', function(e){
            e.preventDefault();

            let self = $(this);
            let url = `${URL_LIST}?${$(this).serialize()}`;

            showDataTableServerSide($('#datatable'), url, columns);
        });
    });
</script>
@endsection