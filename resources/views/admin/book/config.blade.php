@extends('admin.body')
@php
    $pageName = 'Sản phẩm';
    $routeName = getCurrentSlug();
@endphp
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets\icon\icofont\css\icofont.css')}}">
@endsection
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="panel-body">
            <form class="form-horizontal" action="{{url($routeName)}}" method="POST" role="form"
                enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Sản phẩm không tích điểm</h4>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Danh mục</label>
                                    <div class="col-sm-9">
                                        <select class="form-control populate select2" name="categories[]" multiple>
                                            @if($categories)
                                                @foreach($categories as $item)
                                                    <option value="{{$item->id}}" {{ isset($settingCategories) && in_array($item->id, $settingCategories) ? 'selected' : '' }}>{{$item->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if ($errors->has('categories'))
                                            <div class="text-danger mt-2">{{ $errors->first('categories') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Thương hiệu</label>
                                    <div class="col-sm-9">
                                        <select class="form-control populate select2" name="brands[]" multiple>
                                            @if($brands)
                                                @foreach($brands as $item)
                                                    <option value="{{$item->id}}" {{ isset($settingBrands) && in_array($item->id, $settingBrands) ? 'selected' : '' }}>{{$item->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        @if ($errors->has('brands'))
                                            <div class="text-danger mt-2">{{ $errors->first('brands') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Sản phẩm</label>
                                    <div class="col-sm-9">
                                        @include('admin.components.product-search-import', 
                                        [
                                            'inputName' => 'products[]',
                                            'products' => $settingProducts ?? null,
                                            'mode' => 'config'
                                        ])
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <x-submit-button :route="$routeName"/>
            </form>
        </div>
    </div>
<!-- Page-body end -->
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function(){

        @if(!isset($product))
            $('input[name="name"]').on('keyup', function(){
                convert_slug($(this).val());
            });
        @endif

        $(document).on('focusout', '.input-price', function(){
            $(this).val() == '' && $(this).val(0);
        });

        if($('#sortable').length > 0){
            Sortable.create($('#sortable')[0], {
                animation: 0,
                onEnd: function (evt) {
                    var data_sort = evt.from;
                    let order = [];
        
                    $(data_sort).find('li').each(function(){
                        order.push({id: $(this).data('id'), order: $(this).index() + 1});
                    });
        
                    $.post('{{ route('images.order') }}', {
                        order: order,
                        _token: '{{ csrf_token() }}'
                    }, function (data) {
                        data.status && pushNotify('Cập nhật thành công!', text = '', type = 'success');
                    });
                }
            })
        }
    });
</script>
@endsection