@extends('admin.body')
@php
    $pageName = 'Sản phẩm';
    $routeName = getCurrentSlug();
@endphp
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets\icon\icofont\css\icofont.css') }}">
@endsection
@section('title', $pageName)
@section('content')
    @include('admin.components.page-header')
    <!-- Page-body start -->
    <div class="page-body">
        <div class="panel-body">
            <form class="form-horizontal" action="{{ url($routeName) }}" method="POST" role="form"
                enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Thông tin {{ $pageName }}</h4>
                                <x-input type="text" title="Tên sản phẩm" name="name"
                                    value="{{ $book->name ?? '' }}" />
                                <x-input type="text" title="Mã sản phẩm (SKU)" name="sku"
                                    value="{{ $book->sku ?? '' }}" />
                                @if((isset($book) && ($book->status == 'new' || $book->status == 'reviewing')) || !isset($book))
                                <x-upload-attachment type="short" title="Tệp đọc"  name="file_link" value="{{ old('file_link', isset($file_link) ? $file_link : '' ) }}" accept="application/epub+zip"/>
                                @endif
                                @if(isset($book))
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Trạng thái</label>
                                    <div class="col-sm-9">
                                        @switch($book->status)
                                            @case("new")
                                                <label class="label label-info">Khởi tạo</label>
                                             @break
                                            @case("reviewing")
                                                <label class="label label-warning">Chờ xét duyệt</label>
                                             @break
                                            @case("approved")
                                                <label class="label label-primary">Đã duyệt</label>
                                            @break
                                            @case("published")
                                                <label class="label label-success">Đã phát hành</label>
                                            @break
                                            @case("removed")
                                                <label class="label label-danger">Đã xoá</label>
                                            @break
                                            @case("paused")
                                                <label class="label label-danger">Đã dừng phát hành</label>
                                            @break
                                        @endswitch
                                    </div>
                                </div>
                                @endif
                                <!-- <div class="form-group row">
                                                <label class="col-sm-3 col-form-label text-right">Danh mục</label>
                                                <div class="col-sm-9">
                                                    <select class="form-control populate select2" name="categories[]" multiple>
                                                    </select>
                                                </div>
                                            </div> -->

                                <!-- <div class="form-group row">
                                                <label class="col-sm-3 col-form-label text-right">Hạn sử dụng:</label>
                                                <div class="col-sm-3">
                                                    <div class='input-group date datetime-pick'>
                                                        <input type='text' class="form-control" name="product_date"
                                                            value="{{ isset($book) ? format_date($book->product_date) : format_date(\Carbon\Carbon::now()) }}"/>
                                                        <span class="input-group-addon bg-primary">
                                                        <span class="feather icon-calendar"></span>
                                                        </span>
                                                    </div>
                                                    @if ($errors->has('product_date'))
    <div class="text-danger mt-2">{{ $errors->first('product_date') }}</div>
    @endif
                                                </div>
                                            </div> -->
                                <!-- <div class="form-group row">
                                                <label class="col-sm-3 col-form-label text-right"></label>
                                                <div class="col-sm-4">
                                                    <div class="checkbox-zoom zoom-primary m-0">
                                                        <label>
                                                            <input type="checkbox" value="1" name="is_inventory_tracking" {{ isset($book) && $book->is_inventory_tracking ? 'checked' : '' }} class="check-all">
                                                            <span class="cr">
                                                                <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                            </span>
                                                            <span>Theo dõi tồn kho</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-5">
                                                    <div class="checkbox-zoom zoom-primary m-0">
                                                        <label>
                                                            <input type="checkbox" value="1" name="allow_sell_negative" {{ isset($book) && $book->allow_sell_negative ? 'checked' : '' }} class="check-all">
                                                            <span class="cr">
                                                                <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                            </span>
                                                            <span>Cho phép bán âm</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div> -->
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Giá sản phẩm</h4>
                                @if ($errors->has('price'))
                                    <div class="text-danger mt-2">{{ $errors->first('price') }}</div>
                                @endif
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Giá gốc (đ)</label>
                                    <div class="col-sm-9">
                                        <input type="text"
                                            value="{{ old('original_price', isset($book) ? format_price(intval($book->original_price)) : '0') }}"
                                            name="original_price" id="original_price" class="form-control input-price"
                                            onkeyup="this.value=formatMoney(this.value)" onclick="this.select()"
                                            autocomplete="off" required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-right">Giá bán (đ)</label>
                                    <div class="col-sm-9">
                                        <input type="text"
                                            value="{{ old('price', isset($book) ? format_price($book->price) : '0') }}"
                                            name="price" id="price" class="form-control input-price"
                                            onkeyup="this.value=formatMoney(this.value)" onclick="this.select()"
                                            autocomplete="off" required />
                                    </div>
                                </div>
                                <!-- @if (isLeader())
                                                @isset($book)
        <div class="form-group row">
                                                                    <label class="col-sm-3 col-form-label text-right">Giảm giá (%)</label>
                                                                    <div class="col-sm-9">
                                                                        <input type="text" value="{{ old('discount', $book->discount ?? '0') }}" name="discount" id="discount" class="form-control prevent-event"/>
                                                                    </div>
                                                                </div>
    @endisset -->
                                <!-- <div class="form-group row">
                                                <label class="col-sm-3 col-form-label text-right">Số lượng</label>
                                                <div class="col-sm-9">
                                                    <input type="number"
                                                    value="{{ old('qty', isset($book) ? $book->qty : '0') }}"
                                                    name="qty" class="form-control"
                                                    onclick="this.select()"
                                                    autocomplete="off" />
                                                    @if ($errors->has('qty'))
    <div class="text-danger mt-2">{{ $errors->first('qty') }}</div>
    @endif
                                                </div>
                                            </div> -->
                                <!-- <div class="form-group row">
                                                <label class="col-sm-3 col-form-label text-right">Số lượng showroom</label>
                                                <div class="col-sm-9">
                                                    <input type="number"
                                                    value="{{ old('showroom_qty', isset($book) ? $book->showroom_qty : '0') }}"
                                                    name="showroom_qty" class="form-control"
                                                    onclick="this.select()"
                                                    autocomplete="off" />
                                                    @if ($errors->has('showroom_qty'))
    <div class="text-danger mt-2">{{ $errors->first('showroom_qty') }}</div>
    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label text-right">Số lượng kho</label>
                                                <div class="col-sm-9">
                                                    <input type="number"
                                                    value="{{ old('inventory_qty', isset($book) ? $book->inventory_qty : '0') }}"
                                                    name="inventory_qty" class="form-control"
                                                    onclick="this.select()"
                                                    autocomplete="off" />
                                                    @if ($errors->has('inventory_qty'))
    <div class="text-danger mt-2">{{ $errors->first('inventory_qty') }}</div>
    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label text-right">Tiền thưởng (đ)</label>
                                                <div class="col-sm-9">
                                                    <input type="number"
                                                    value="{{ old('bonus', isset($book) ? format_price($book->bonus) : '0') }}"
                                                    name="bonus" class="form-control"
                                                    onclick="this.select()"
                                                    autocomplete="off"
                                                    required/>
                                                    @if ($errors->has('bonus'))
    <div class="text-danger mt-2">{{ $errors->first('bonus') }}</div>
    @endif
                                                </div>
                                            </div>
                                            @endif -->
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Thuộc tính sản phẩm</h4>
                                <!-- book_attrs -->
                                @foreach ($book_attrs as $attr)
                                    @switch($attr->type)
                                        @case('number')
                                            <x-input type="number" title="{{ $attr->describe }}" name="attr[{{ $attr->name }}]"
                                                value="{{ old('attr.'.$attr->name, isset($attr_values[$attr->name]) ? $attr_values[$attr->name] : '') }}" />
                                        @break

                                        @case('text')
                                            <x-input type="text" title="{{ $attr->describe }}" name="attr[{{ $attr->name }}]"
                                                value="{{ old('attr.'.$attr->name, isset($attr_values[$attr->name]) ? $attr_values[$attr->name] : '') }}" />
                                        @break
                                        @case('paragraph')
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label text-right">{{ $attr->describe }}:</label>
                                            <div class="col-sm-9"><x-textarea type="tinymce" title="{{ $attr->describe }}" name="attr[{{ $attr->name }}]"
                                                value="{{ old('attr.'.$attr->name, isset($attr_values[$attr->name]) ? $attr_values[$attr->name] : '') }}" />
                                                @if ($errors->has('attr.'.$attr->name))
                                                    <div class="text-danger mt-2">{{ $errors->first('attr.'.$attr->name) }}</div>
                                                @endif
                                        </div></div>
                                        @break
                                        @case('date')
                                            <x-datetime name="attr[{{$attr->name}}]" describe="{{$attr->describe}}" value="{{ old('attr.'.$attr->name, isset($attr_values[$attr->name]) ? $attr_values[$attr->name] : '') }}"/>
                                        @break

                                        @case('datetime')
                                            <x-datetime name="attr[{{$attr->name}}]" describe="{{$attr->describe}}" type="datetime" value="{{ old('attr.'.$attr->name, isset($attr_values[$attr->name]) ? $attr_values[$attr->name] : '') }}"/>
                                        @break

                                        @case('json')
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label text-right">{{ $attr->describe }}:</label>
                                            <div class="col-sm-9"><x-textarea type="tinymce" title="{{ $attr->describe }}" name="attr[{{ $attr->name }}]"
                                                value="{{ old('attr.'.$attr->name, isset($attr_values[$attr->name]) ? $attr_values[$attr->name] : '') }}" /></div></div>
                                        @break

                                        @case('attachment')
                                            <x-upload-attachment type="short" title="{{ $attr->describe }}"  name="attr[{{ $attr->name }}]" value="{{ old('attr.'.$attr->name, isset($attr_values[$attr->name]) ? $attr_values[$attr->name] : '') }}" accept="*/*" errorname="{{ 'attr.'.$attr->name }}"/>
                                        @break

                                        @case('list_attachment')
                                            <x-upload-attachment type="short" title="{{ $attr->describe }}"
                                                name="attr[{{ $attr->name }}]"
                                                note="{{ $attr->describe }}" />
                                        @break

                                        @case('image')
                                            <x-upload-attachment type="short" title="{{ $attr->describe }}"  name="attr[{{ $attr->name }}]" value="{{ old('attr.'.$attr->name, isset($attr_values[$attr->name]) ? $attr_values[$attr->name] : '') }}" accept="image/*" errorname="{{ 'attr.'.$attr->name }}"/>
                                        @break

                                        @case('image_list')
                                            <x-upload-file type="short" title="{{ $attr->describe }}"
                                                name="attr[{{ $attr->name }}]"  width="100%"
                                                note="{{ $attr->describe }}" />
                                        @break

                                        @default
                                            <x-input type="text" title="{{ $attr->describe }}" name="attr[{{ $attr->name }}]"
                                                value="{{ old('attr.'.$attr->name, isset($attr_values[$attr->name]) ? $attr_values[$attr->name] : '') }}" />
                                    @endswitch
                                @endforeach
                                <!-- <x-input type="number" title="Chiều dài (cm)" name="length" value="{{ $book->length ?? '' }}"/>
                                            <x-input type="number" title="Chiều rộng (cm)" name="width" value="{{ $book->width ?? '' }}"/>
                                            <x-input type="number" title="Chiều cao (cm)" name="height" value="{{ $book->height ?? '' }}"/>
                                            <x-input type="number" title="Cân nặng (kg)" name="weight" value="{{ $book->weight ?? '' }}"/> -->
                            </div>
                        </div>
                        @isset($book)
                            <div class="card hidden">
                                <div class="card-block">
                                    <h4 class="sub-title">Hình ảnh</h4>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="wrap-upload">
                                                <div class="upload-zone-icon"><i class="feather icon-upload-cloud"></i></div>
                                                <div class="upload-zone-button">
                                                    <a href="#" id="upload-multiple-images"
                                                        class="btn btn-primary">Upload ảnh</a>
                                                    <input type="file" data-id="{{ $book->id }}" data-type="product"
                                                        multiple="" accept="image/x-png, image/jpg, image/jpeg"
                                                        id="input-multiple-images" class="hidden" />
                                                </div>
                                                <ul id="sortable" class=" ">
                                                    {{-- @foreach ($book->images as $image)
                                                        <li data-id="{{ $image->id }}" data-path={{ $image->path }}>
                                                            <img src="{{ asset($image->path) }}" alt=""><span
                                                                class="remove-img">
                                                                <i class="feather icon-trash-2"></i></span>
                                                        </li>
                                                    @endforeach --}}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                    </div>
                                </div>
                            </div>
                        @endisset
                        {{-- <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Thông tin SEO</h4>
                                <x-input type="text" title="Đường dẫn" name="slug" value="{{ $book->slug ?? ''  }}"/>
                                <x-input type="text" title="Meta title" name="meta_title" value="{{ $book->meta_title ?? ''  }}"/>
                                <x-textarea type="" title="Meta description" name="meta_description" value="{{ $book->meta_description ?? ''  }}" />
                                <x-input type="text" title="Meta keyword" name="meta_keyword" value="{{ $book->meta_keyword ?? ''  }}"/>
                            </div>
                        </div> --}}
                        {{-- <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Nội dung {{ $pageName }}</h4>
                                <x-textarea type="tinymce" title="" name="description"
                                    value="{!! isset($book) ? $book->description : '' !!}" />
                            </div>
                        </div> --}}
                    </div>
                    <div class="col-sm-3">
                        @if((isset($book) && ($book->status == 'new' || $book->status == 'reviewing')) || !isset($book))
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Trạng thái</h4>
                                <x-switch-box type="short" title="Xét duyệt ngay" name="status"
                                    checked="{{ isset($book) ? ($book->status == 'reviewing' ? 'true' : '') : '' }}" />
                            </div>
                        </div>
                        @endif
                        @if(isset($book) && ($book->status == 'reviewing'))
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Duyệt</h4>
                                <div class="btn btn-success" id="approveBtn" onclick="approvePublisher('{{$book->id}}')">Chấp nhận phát hành</div>
                            </div>
                        </div>
                        @endif
                        @if((isset($book) && $book->status == 'reviewing'))
                        <div class="card">
                            <div class="card-block">
                                <h4 class="sub-title">Preview</h4>
                                <div id="qrcode"></div>
                                <script type="text/javascript">
                                </script>

                            </div>
                        </div>

                        @endif
                    </div>
                </div>
                <x-submit-button :route="$routeName" />
            </form>
        </div>
    </div>
    <!-- Page-body end -->
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function() {
            @if(isset($book))
            new QRCode(document.getElementById("qrcode"), "{{setting('deeplink_mobile').$book->id}}");
            window.approvePublisher = async function(bookId){
                if(confirm("Xác nhận duyệt cho phép phát hành sách này?")){
                    $("#approveBtn").prop( "disabled", true );
                    res = await $.ajax(
                        {
                            method: "POST",
                            url : "/admin/book/approve",
                            data : {
                                "book_id" : '{{$book->id}}',
                                _token: '{{ csrf_token() }}'
                            }
                        }
                    )
                    alert(res.message);
                    if(res.status == 200){
                        window.location.reload()
                    }
                    $("#approveBtn").prop( "disabled", false );
                }
            }
            @endif
            console.log('{{ $errors }}')
            @if (!isset($book))
                $('input[name="name"]').on('keyup', function() {
                    convert_slug($(this).val());
                });
            @endif

            $(document).on('focusout', '.input-price', function() {
                $(this).val() == '' && $(this).val(0);
            });

            if ($('#sortable').length > 0) {
                Sortable.create($('#sortable')[0], {
                    animation: 0,
                    onEnd: function(evt) {
                        var data_sort = evt.from;
                        let order = [];

                        $(data_sort).find('li').each(function() {
                            order.push({
                                id: $(this).data('id'),
                                order: $(this).index() + 1
                            });
                        });

                        $.post('{{ route('images.order') }}', {
                            order: order,
                            _token: '{{ csrf_token() }}'
                        }, function(data) {
                            data.status && pushNotify('Cập nhật thành công!', text = '', type =
                                'success');
                        });
                    }
                })
            }
        });
    </script>
@endsection
