<div class="form-group row">
        <span class="col-sm-3 col-form-label text-right">{{ $title }}</span>
        <div class="col-sm-9 box-image">
            <span class="filename-input">{{ !is_null($value) ? $value : '<<Rỗng>>'  }}</span>
            <input type="file" 
                name="{{ $name }}" 
                accept="{{ $accept }}"
                data-original-title="Thêm {{ $title }}" 
                class="form-control input-file input-attachment"/>
        <div class="text-left">
            <a href="#" class="btn btn-primary btn-upload-file">
                <i class="feather icon-upload-cloud"></i>
            </a>
            <a href="#" class="btn btn-danger btn-remove-file">
                <i class="feather icon-trash"></i>
            </a>
            @if ($errors->has($errorname))
                <div class="text-danger mt-2">{{ $errors->first($errorname) }}</div>
            @endif
        </div>
    </div>
</div>