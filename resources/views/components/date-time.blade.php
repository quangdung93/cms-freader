<div class="form-group row">
    <label class="col-sm-3 col-form-label text-right">{{ $describe }}:</label>
    <div class='col-sm-9 input-group date {{ $type == 'datetime' ? 'real-datetime-pick' : 'datetime-pick'}}'>
        <input type='text' class="form-control" name="{{ $name }}"
            value="{{ $value }}" />
        <span class="input-group-addon bg-primary">
            <span class="feather icon-calendar"></span>
        </span>
        @if ($errors->has($name))
            <div class="text-danger mt-2">{{ $errors->first($name) }}</div>
        @endif
    </div>
</div>
